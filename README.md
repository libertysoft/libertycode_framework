LibertyCode_Framework
=====================



Description
-----------

Library contains framework components, and implementation, 
to create and run application.

_Usage_

- Framework components

    Allows define framework engine components.
    Default behavior are set, but it can be instantiate and overwritten, 
    to customize framework engine.

- Framework implementation
 
    Factories, builders and utilities allows to define default framework implementation.
    Can be replaced to customize other framework implementation.
    
---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/framework ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/framework": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    - Include framework components only
    
        ```php
        require_once('<repository_root_path>/include/Include.php');
        ```

    - Include framework implementation (with framework components)
    
        ```php
        require_once('<repository_root_path>/include/framework/Include.php');
        ```

---



Usage
-----

#### Application mode

Application mode system allows to set macro configuration,
to impact behavior of application.

_Elements_

- Config

    Extends simple array data and configuration features. 
    Allows to store a set of specific parameters.

- Mode
    
    Allows to design a set of specific parameters, 
    associated to a specific key.
    
- ModeCollection

    Allows to design collection of modes.
    Uses list of modes,
    to set and retrieve specified mode.
    
- ModeFactory

    Allows to design a mode factory, 
    to provide new mode instance.
    
- Builder
    
    Allows to hydrate mode collection, with modes.
    
- Selector
    
    Allows to design mode selector.
    Uses mode collection to select a specified mode.
    Selection is action to mark as active, 
    a specified mode from mode collection.
    
#### Module

Modules system allows to separate application parts, 
by logical parts.
Application is a set of modules.

_Elements_

- Module

    Allows to design a module, 
    who is an item containing information and configuration, 
    about a specified part of application.
    
- ModuleCollection

    Allows to design collection of modules.
    Uses list of modules,
    to get merged configuration for whole application.

- ModuleFactory

    Allows to design a module factory, 
    to provide new or specified module instance,
    from specified configuration.
    
- StandardModuleFactory

    Extends module factory features. 
    Provides default module instance.
    
- Builder
    
    Allows to hydrate module collection, with modules.
    
#### Bootstrap

Bootstrap allows to boot module.

_Elements_

- Bootstrap

    Allows to design a bootstrap, 
    who is an item containing information, 
    to boot a specified module.
    
#### Route

Route allows to design HTTP route, used in application.

_Elements_

- HttpPatternRoute, HttpParamRoute, HttpFixRoute, HttpSeparatorRoute
    
    Extends HTTP route features. 
    Adapt all HTTP routes, 
    to use in application.
    
- HttpRouteFactory

    Extends HTTP route factory features. 
    Provides HTTP route instance, used in application.
    
#### Request

Request allows to design the request part, 
from the request flow process, 
used in application.

_Elements_
 
- HttpRequest
    
    Extends HTTP request features. 
    Allows to get route source from argument "route", 
    found in header or in GET.

#### Error

Error allows to design handlers and warning handlers, 
used in application.

_Elements_

- ExecThrowableHandler

    Extends throwable handler features. 
    Allows to handle throwable errors,
    during application execution.
    
- SysThrowableHandler

    Extends execution throwable handler features. 
    Allows to handle throwable errors,
    during application system running.
    
- FixExecThrowableHandler

    Extends execution throwable handler features. 
    Allows to handle throwable errors,
    during application execution, 
    from a specified fixed configuration.
    
- FixSysThrowableHandler

    Extends system throwable handler features. 
    Allows to handle throwable errors,
    during application system running, 
    from a specified fixed configuration.
    
- ExecThrowableWarnHandler

    Extends throwable warning handler features. 
    Allows to handle throwable warnings,
    during application execution.
    
- FixExecThrowableHandler

    Extends execution throwable warning handler features. 
    Allows to handle throwable warnings,
    during application execution,
    from a specified fixed configuration.
    
#### Application

Application allows to design the project application,
to access main framework features, 
and run application.

_Elements_
 
- App
    
    Allows to design project application, 
    and run application.
    Application contains:
    
    - Parser builder for configuration
        
        Uses parser builder features.
        Allows to get specific parser for application configuration.
    
    - Cache repository
    
        Uses cache repository features.
        Allows to manage application cache.
        
    - Application mode selector
    
        Uses mode selector features.
        Allows to select specific mode for application.
        
    - Module collection
    
        Uses module collection.
        Allows to store and manage all application modules.
    
    - Configuration
    
        Uses configuration features.
        Allows to retrieve application configuration.
        
    - Provider
    
        Uses dependency provider features.
        Allows to manage dependency injection.
    
    - Register
    
        Uses register features.
        Allows to store and manage global key-value pairs. 
        
    - Observer
        
        Uses event observer features.
        Allows to manage events. 
    
    - Front controller
    
        Uses request flow front controller features.
        Allows to manage and run request flow process. 

#### Framework

Framework allows to instantiate and hydrate application and dependent components,
to define framework implementation.

_Elements_
 
- Application

    Application implementation and features.

    ```php
    use liberty_code\framework\framework\application\library\ToolBoxApp;
    $app = ToolBoxApp::getObjApp();
    ...
    ```
    
- Library

    HTTP URL features.
    Path features.
    
    ```php
    // Get string web route, from specified route key and parameters
    use liberty_code\framework\framework\library\http\url\library\ToolBoxRoute;
    $route = ToolBoxRoute::getStrRoute(...);
    ...
    // Get string full path, from specified relative application file or directory path
    use liberty_code\framework\framework\library\path\library\ToolBoxPath;
    ToolBoxPath::getStrPathFull(...);
    ...
    ```
    
- Application mode

    Application mode selector implementation.

- Cache repository

    Application cache repository features.
    
    ```php
    use liberty_code\framework\framework\cache\repository\library\ToolBoxRepository;
    ...
    // Set specified item with specified key, and set it, if required
    ToolBoxRepository::getSetItem(...);
    ...
    ```
    
- Module

    Module collection implementation.

- Bootstrap

    Bootstrap features.
    
    ```php
    // Put application configuration
    use liberty_code\framework\framework\bootstrap\library\ToolBoxBootstrap;
    ToolBoxBootstrap::putAppConfigFile(...);
    ...
    // Put module configuration
    ToolBoxBootstrap::putModuleConfigFile(...);
    ...
    ```
    
- Auto-loading

    Auto-loading implementation.

- Configuration

    Configuration implementation and features.
    
    ```php
    // Get configuration path key, from specified keys
    use liberty_code\framework\framework\config\library\ToolBoxConfig;
    ToolBoxConfig::getStrPathKeyFromList(...);
    ...
    ```
      
- Provider

    Provider implementation.
    
- Register
    
    Register implementation.
    
- Observer
    
    Observer implementation.

- Router

    Router implementation.
    
- Request flow

    Response features.
    
    ```php
    // Get forwarded response, from specified route key and parameters
    use liberty_code\framework\framework\request_flow\response\http\library\ToolBoxResponse;
    ToolBoxResponse::getObjForwardResponse(...);
    ...
    // Get HTTP redirected response, from specified route key and parameters
    liberty_code\framework\framework\request_flow\response\http\library\ToolBoxHttpResponse;
    ToolBoxHttpResponse::getObjRedirectRouteResponse(...);
    ...
    ```
   
---


