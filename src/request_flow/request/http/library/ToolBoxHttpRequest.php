<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\request_flow\request\http\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxHttpRequest extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************
	
	/**
     * Get formatted route.
     *
     * @param string $strRoute
     * @return null|string
     */
    public static function getStrFormatRoute($strRoute)
    {
        // Init var
        $result = null;

		if(is_string($strRoute))
		{
			$result = $strRoute;
			
			// Case '/' not found at the start
			if(preg_match('#^\/.*$#', $strRoute) != 1)
			{
				// Add '/' in route
				$result = '/' . $strRoute;
			}
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get formatted route root.
     *
     * @param string $strRouteRoot
     * @return null|string
     */
    public static function getStrFormatRouteRoot($strRouteRoot)
    {
        // Init var
        $result = null;

		if(is_string($strRouteRoot))
		{
			$result = $strRouteRoot;
			
			// Case '/' found at the end
			if((preg_match('#^(.*)\/$#', $strRouteRoot, $tabMatch) == 1) && isset($tabMatch[1]))
			{
				// Take route without '/'
				$result = $tabMatch[1];
			}
		}
		
		// Return result
		return $result;
    }



}