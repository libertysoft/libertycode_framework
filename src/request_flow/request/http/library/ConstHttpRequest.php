<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\request_flow\request\http\library;



class ConstHttpRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Configuration schema
    const CONF_KEY_ROUTE = 'route';
	const CONF_KEY_ROUTE_ROOT = 'route-root';
	
	
	
	// Exception message constants
	const EXCEPT_MSG_ARG_ROUTE_NOT_FOUND = 'No route argument found from request!';
	const EXCEPT_MSG_ROUTE_ROOT_NOT_FOUND = 'No route root found from request!';
}