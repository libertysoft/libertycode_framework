<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\request_flow\request\http\exception;

use liberty_code\framework\request_flow\request\http\library\ConstHttpRequest;



class RouteRootNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct()
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstHttpRequest::EXCEPT_MSG_ROUTE_ROOT_NOT_FOUND;
	}
}