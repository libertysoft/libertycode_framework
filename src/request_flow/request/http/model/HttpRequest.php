<?php
/**
 * Description :
 * This class allows to define http request class for framework applications.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\request_flow\request\http\model;

use liberty_code\http\request_flow\request\model\HttpRequest as BaseHttpRequest;

use liberty_code\framework\request_flow\request\http\library\ConstHttpRequest;
use liberty_code\framework\request_flow\request\http\library\ToolBoxHttpRequest;
use liberty_code\framework\request_flow\request\http\exception\ArgRouteNotFoundException;
use liberty_code\framework\request_flow\request\http\exception\RouteRootNotFoundException;



class HttpRequest extends BaseHttpRequest
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
    // ******************************************************************************
	
	/**
	 * Get string route, from request headers.
	 * 
	 * @return null|string
	 */
	protected function getStrRouteFromHeader()
	{
		// Return result
		return $this->getHeader(ConstHttpRequest::CONF_KEY_ROUTE, null);
	}
	
	
	
	/**
	 * Get string route, from request GET parameters.
	 * 
	 * @return null|string
	 */
	protected function getStrRouteFromGet()
	{
		// Return result
		return $this->getGet(ConstHttpRequest::CONF_KEY_ROUTE, null);
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws ArgRouteNotFoundException
	 */
	public function getStrRoute()
	{
		// Try to get route from header
		$result = $this->getStrRouteFromHeader();
		if(is_null($result))
		{
			// Try to get route from GET
			$result = $this->getStrRouteFromGet();
			if(is_null($result))
			{
				throw new ArgRouteNotFoundException();
			}
		}
		
		// Return result
		$result = ToolBoxHttpRequest::getStrFormatRoute($result);
		return $result;
	}
	
	
	
	/**
	 * Get string route root, from request headers.
	 * 
	 * @return null|string
	 */
	protected function getStrRouteRootFromHeader()
	{
		// Return result
		return $this->getHeader(ConstHttpRequest::CONF_KEY_ROUTE_ROOT, null);
	}
	
	
	
	/**
	 * Get string route root, from request GET parameters.
	 * 
	 * @return null|string
	 */
	protected function getStrRouteRootFromGet()
	{
		// Return result
		return $this->getGet(ConstHttpRequest::CONF_KEY_ROUTE_ROOT, null);
	}
	
	
	
	/**
	 * Get string route root, calculated from route.
	 * 
	 * @return null|string
	 */
	protected function getStrRouteRootCalcul()
	{
		// Init var
		$strRoute = $this->getStrRoute();
		$strUrl = $_SERVER['REQUEST_URI'];
		
		// Re-treatment: skip GET part
		if(strpos($strUrl, '?') !== false)
		{
			$tabStr = explode('?', $strUrl);
			$strUrl = $tabStr[0];
		}
		
		// Set result
		$result = trim(substr($strUrl, 0, (strlen($strUrl)-strlen($strRoute))));
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * Get string route root.
	 * 
	 * @return string
	 * @throws RouteRootNotFoundException
	 */
	public function getStrRouteRoot()
	{
		// Try to get route root from header
		$result = $this->getStrRouteRootFromHeader();
		if(is_null($result))
		{
			// Try to get route root from GET
			$result = $this->getStrRouteRootFromGet();
			if(is_null($result))
			{
				// Try to get calculated route root from route
				$result = $this->getStrRouteRootCalcul();
				if(is_null($result))
				{
					throw new RouteRootNotFoundException();
				}
			}
		}
		
		// Return result
		$result = ToolBoxHttpRequest::getStrFormatRouteRoot($result);
		return $result;
	}
	
	
	
}