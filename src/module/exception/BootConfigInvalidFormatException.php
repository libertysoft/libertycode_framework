<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\exception;

use liberty_code\framework\module\library\ConstModule;



class BootConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstModule::EXCEPT_MSG_BOOT_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}





	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $tabConfig
     * @return boolean
     */
    protected static function checkConfigIsValid($tabConfig)
    {
        // Init var
        $result = is_array($tabConfig);

        // Run each config, if required
        if($result)
        {
            $tabConfig = array_values($tabConfig);
            for($intCpt = 0; ($intCpt < count($tabConfig)) && $result; $intCpt++)
            {
                $config = $tabConfig[$intCpt];

                // Check config valid
                $result =
                    // Check valid destination configuration
                    isset($config[ConstModule::TAB_BOOT_CONFIG_KEY_CALL]) &&
                    is_array($config[ConstModule::TAB_BOOT_CONFIG_KEY_CALL]) &&
                    (count($config[ConstModule::TAB_BOOT_CONFIG_KEY_CALL]) > 0) &&

                    // Check valid element
                    (
                        (!isset($config[ConstModule::TAB_BOOT_CONFIG_KEY_ELEMENT])) ||
                        is_array($config[ConstModule::TAB_BOOT_CONFIG_KEY_ELEMENT])
                    ) &&

                    // Check valid argument
                    (
                        (!isset($config[ConstModule::TAB_BOOT_CONFIG_KEY_ARGUMENT])) ||
                        is_array($config[ConstModule::TAB_BOOT_CONFIG_KEY_ARGUMENT])
                    );
            }
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}