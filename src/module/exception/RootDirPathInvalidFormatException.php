<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\exception;

use liberty_code\framework\module\library\ConstModule;



class RootDirPathInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $rootPath
     */
	public function __construct($rootPath)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstModule::EXCEPT_MSG_ROOT_DIR_PATH_INVALID_FORMAT, strval($rootPath));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified root folder path has valid format.
	 * 
     * @param mixed $rootPath
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($rootPath)
    {
		// Init var
		$result =
            // Check is valid string folder path
            (file_exists($rootPath) && is_dir($rootPath));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($rootPath);
		}
		
		// Return result
		return $result;
    }
	
	
	
}