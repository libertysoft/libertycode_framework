<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\exception;

use liberty_code\framework\module\library\ConstModule;



class CallableUnableGetException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstModule::EXCEPT_MSG_CALLABLE_UNABLE_GET,
            mb_strimwidth(strval($strKey), 0, 50, "...")
        );
	}
}