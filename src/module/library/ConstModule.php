<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\library;



class ConstModule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER = 'objConfigParserBuilder';
    const DATA_KEY_DEFAULT_MODULE_COLLECTION = 'objModuleCollection';
    const DATA_KEY_DEFAULT_ROOT_DIR_PATH = 'strRootDirPath';
    const DATA_KEY_DEFAULT_KEY = 'strKey';
    const DATA_KEY_DEFAULT_CONFIG = 'objConfig';
    const DATA_KEY_DEFAULT_BOOT_CONFIG = 'tabBootConfig';



    // Path configuration
	const CONF_PATH_DIR_CONFIG = '/config';
	const CONF_PATH_FILE_CONFIG_MODULE = '/Module.';
	const CONF_PATH_FILE_CONFIG_PARAM = self::CONF_PATH_DIR_CONFIG . '/Param.';
    const CONF_PATH_FILE_CONFIG_BOOT = self::CONF_PATH_DIR_CONFIG . '/Boot.';
    const CONF_PATH_FILE_CONFIG_PARAM_APP = self::CONF_PATH_DIR_CONFIG . '/ParamApp.';
	const CONF_PATH_FILE_CONFIG_AUTOLOAD = self::CONF_PATH_DIR_CONFIG . '/Autoload.';
	const CONF_PATH_FILE_CONFIG_DI = self::CONF_PATH_DIR_CONFIG . '/Di.';
	const CONF_PATH_FILE_CONFIG_REGISTER = self::CONF_PATH_DIR_CONFIG . '/Register.';
    const CONF_PATH_FILE_CONFIG_EVENT = self::CONF_PATH_DIR_CONFIG . '/Event.';
	const CONF_PATH_FILE_CONFIG_ROUTE_WEB = self::CONF_PATH_DIR_CONFIG . '/RouteWeb.';
	const CONF_PATH_FILE_CONFIG_ROUTE_COMMAND = self::CONF_PATH_DIR_CONFIG . '/RouteCommand.';

    // Merge configuration
    const TAB_MERGE_CONFIG_KEY_MERGE_OPTION = 'merge_option';

    // Module configuration
    const TAB_CONFIG_MODULE_KEY_KEY = 'key';

    // Boot configuration
    const TAB_BOOT_CONFIG_KEY_CALL = 'call';
    const TAB_BOOT_CONFIG_KEY_ELEMENT = 'element';
    const TAB_BOOT_CONFIG_KEY_ARGUMENT = 'argument';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_PARSER_BUILDER_INVALID_FORMAT = 'Following configuration parser builder "%1$s" invalid! It must be a parser builder object.';
    const EXCEPT_MSG_MODULE_COLLECTION_INVALID_FORMAT = 'Following module collection "%1$s" invalid! It must be a module collection object.';
    const EXCEPT_MSG_ROOT_DIR_PATH_INVALID_FORMAT = 'Following module root directory path "%1$s" invalid! The root path must be a valid string folder full path.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a valid string, not empty.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following configuration "%1$s" invalid! It must be a configuration object.';
    const EXCEPT_MSG_BOOT_CONFIG_INVALID_FORMAT =
        'Following boot config "%1$s" invalid! 
        The config must be an array and following the default module boot configuration standard.';
    const EXCEPT_MSG_CALLABLE_UNABLE_GET = 'Impossible to get callable from following module "%1$s"! Some module configuration elements are unsolved.';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a valid string, not empty, in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Following value "%1$s" invalid! The value must be a module object in collection.';
	const EXCEPT_MSG_MERGE_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default module collection merge configuration standard.';
}