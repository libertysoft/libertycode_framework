<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\file\library\ToolBoxFile;
use liberty_code\parser\file\api\FileParserInterface;
use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;



class ToolBoxModule extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

	/**
     * Get string configuration file full path, 
	 * from specified module root directory full path and
	 * specified configuration file relative path.
     *
     * @param string $strRootDirPath
	 * @param string $strFilePath
     * @param FileParserInterface $objFileParser
     * @return null|string
     */
    public static function getStrConfigFilePath($strRootDirPath, $strFilePath, FileParserInterface $objFileParser)
    {
        // Init var
        $strFileFullPath = $strRootDirPath . $strFilePath . $objFileParser->getStrFileExt();
        $result = ToolBoxFile::getStrFilePath($strFileFullPath);
		
		// Return result
		return $result;
    }



    /**
     * Get module array configuration,
     * from specified module root directory full path
     * and specified configuration file relative path.
     *
     * @param string $strRootDirPath
     * @param string $strFilePath
     * @param ParserBuilderInterface $objParserBuilder = null
     * @return null|array
     */
    public static function getTabConfig($strRootDirPath, $strFilePath, ParserBuilderInterface $objParserBuilder = null)
    {
        // Init var
        $result = null;

        if(!is_null($objParserBuilder))
        {
            $objFileParser = $objParserBuilder->getObjFileParser();

            // Check file path found
            $strConfigFilePath = static::getStrConfigFilePath($strRootDirPath, $strFilePath, $objFileParser);
            if(!is_null($strConfigFilePath))
            {
                // Get data
                $tabData = $objFileParser->getData($strConfigFilePath);

                // Register data in result, if valid
                if((!is_null($tabData)) && is_array($tabData))
                {
                    $result = $tabData;
                }
            }
        }

        // Return result
        return $result;
    }



}