<?php
/**
 * Description :
 * This class allows to define default module factory class.
 * Can be consider is base of all module factory type.
 *
 * Default module factory uses the following specified configuration, to get and hydrate module:
 * [
 *     type(optional): "string constant to determine module type",
 *
 *     ... specific factory module configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\framework\module\factory\api\ModuleFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\module\exception\ModuleCollectionInvalidFormatException;
use liberty_code\framework\module\factory\library\ConstModuleFactory;
use liberty_code\framework\module\factory\exception\FactoryInvalidFormatException;
use liberty_code\framework\module\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|ModuleCollectionInterface getObjModuleCollection() Get module collection object.
 * @method void setObjModuleCollection(null|ModuleCollectionInterface $objModuleCollection) Set module collection object.
 * @method null|ModuleFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|ModuleFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultModuleFactory extends DefaultFactory implements ModuleFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ModuleCollectionInterface $objModuleCollection = null
     * @param ModuleFactoryInterface $objFactory = null
     */
    public function __construct(
        ModuleCollectionInterface $objModuleCollection = null,
        ModuleFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init module collection
        $this->setObjModuleCollection($objModuleCollection);

        // Init module factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstModuleFactory::DATA_KEY_DEFAULT_MODULE_COLLECTION))
        {
            $this->__beanTabData[ConstModuleFactory::DATA_KEY_DEFAULT_MODULE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstModuleFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstModuleFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified module.
     * Overwrite it to set specific module hydration.
     *
     * @param ModuleInterface $objModule
     * @param array &$tabConfigFormat
     * @param boolean $boolIsNew = true
     */
    protected function hydrateModule(
        ModuleInterface $objModule,
        array &$tabConfigFormat,
        $boolIsNew = true
    )
    {
        // Init formatted configuration, if required
        if(array_key_exists(ConstModuleFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstModuleFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate route, if required
        $objModuleCollection = $this->getObjModuleCollection();
        if(!is_null($objModuleCollection))
        {
            $objModule->setModuleCollection($objModuleCollection);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstModuleFactory::DATA_KEY_DEFAULT_MODULE_COLLECTION,
            ConstModuleFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstModuleFactory::DATA_KEY_DEFAULT_MODULE_COLLECTION:
                    ModuleCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstModuleFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified module object.
     *
     * @param ModuleInterface $objModule
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(ModuleInterface $objModule, array $tabConfigFormat)
    {
        // Init var
        $strModuleClassPath = $this->getStrModuleClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strModuleClassPath)) &&
            ($strModuleClassPath == get_class($objModule))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Return result
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstModuleFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstModuleFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of module,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrModuleClassPathFromType($strConfigType);



    /**
     * Get string class path of module engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrModuleClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrModuleClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrModuleClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrModuleClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrModuleClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance module,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @param array $tabConfigFormat
     * @return null|ModuleInterface
     */
    protected function getObjModuleNew($strConfigType, array $tabConfigFormat)
    {
        // Init var
        $strClassPath = $this->getStrModuleClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance module engine.
     *
     * @param array $tabConfigFormat
     * @param ModuleInterface $objModule = null
     * @return null|ModuleInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjModuleEngine(
        array $tabConfigFormat,
        ModuleInterface $objModule = null
    )
    {
        // Init var
        $result = null;
        $boolIsNew = is_null($objModule);
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objModule = (
            $boolIsNew ?
                $this->getObjModuleNew($strConfigType, $tabConfigFormat) :
                $objModule
        );

        // Get and hydrate module, if required
        if(
            (!is_null($objModule)) &&
            $this->checkConfigIsValid($objModule, $tabConfigFormat)
        )
        {
            $this->hydrateModule($objModule, $tabConfigFormat, $boolIsNew);
            $result = $objModule;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjModule(
        array $tabConfig,
        $strConfigKey = null,
        ModuleInterface $objModule = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjModuleEngine($tabConfigFormat, $objModule);

        // Get module from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjModule($tabConfig, $strConfigKey, $objModule);
        }

        // Return result
        return $result;
    }



}