<?php
/**
 * Description :
 * This class allows to define standard module factory class.
 * Standard module factory allows to provide and hydrate default module instance.
 *
 * Standard module factory uses the following specified main configuration:
 * [
 *     file_path_format_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by file path destination"
 * ]
 *
 * Standard module factory uses the following specified configuration, to get and hydrate module:
 * [
 *     -> Configuration key(optional): "module root directory full path"
 *     type(optional): "default",
 *     path(required: optional if provided as configuration key): "string module root directory full path",
 *     config_parser(optional):
 *         Config parser builder object OR
 *         Config parser builder configuration array (@see ParserBuilderInterface )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\factory\standard\model;

use liberty_code\framework\module\factory\model\DefaultModuleFactory;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
use liberty_code\parser\file\factory\api\FileParserFactoryInterface;
use liberty_code\parser\file\factory\string_table\model\StrTableFileParserFactory;
use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\parser\build\factory\model\FactoryBuilder as ParserBuilder;
use liberty_code\data\data\table\path\library\ConstPathTableData;
use liberty_code\data\data\table\path\model\PathTableData;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\config\library\ConstConfig;
use liberty_code\framework\module\library\ConstModule;
use liberty_code\framework\module\library\ToolBoxModule;
use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\module\model\DefaultModule;
use liberty_code\framework\module\factory\api\ModuleFactoryInterface;
use liberty_code\framework\module\factory\standard\library\ConstStandardModuleFactory;
use liberty_code\framework\module\factory\standard\exception\ConfigInvalidFormatException;
use liberty_code\framework\module\factory\standard\exception\GetConfigInvalidFormatException;



/**
 * @method ModuleCollectionInterface getObjModuleCollection() Get module collection object.
 * @method void setObjModuleCollection(ModuleCollectionInterface $objModuleCollection) Set module collection object.
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
class StandardModuleFactory extends DefaultModuleFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ModuleCollectionInterface $objModuleCollection
     * @param array $tabConfig = null,
     */
    public function __construct(
        ModuleCollectionInterface $objModuleCollection,
        array $tabConfig = null,
        ModuleFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objModuleCollection, $objFactory, $objProvider);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardModuleFactory::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstStandardModuleFactory::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateModule(
        ModuleInterface $objModule,
        array &$tabConfigFormat,
        $boolIsNew = true
    )
    {
        // Check arguments
        GetConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Call parent method
        parent::hydrateModule($objModule, $tabConfigFormat, $boolIsNew);

        // Init var
        $boolIsNew = (is_bool($boolIsNew) ? $boolIsNew : true);
        $objDefaultConfigParserBuilder = $this->getObjModuleDefaultConfigParserBuilder();
        $objConfigParserBuilder = $objModule->getObjConfigParserBuilder();
        $strRootDirPath = $this->getStrModuleRootDirPath($tabConfigFormat);

        // Hydrate module, if required
        if((!$boolIsNew))
        {
            $objConfigParserBuilder = $this->getObjModuleConfigParserBuilder($tabConfigFormat);
            $strKey = $this->getStrModuleKey($strRootDirPath, $objDefaultConfigParserBuilder, $objConfigParserBuilder);
            $objConfig = $this->getObjModuleConfig($strRootDirPath, $objDefaultConfigParserBuilder, $objConfigParserBuilder);

            // Hydrate module
            $objModule->setConfigParserBuilder($objConfigParserBuilder);
            $objModule->setRootDirPath($strRootDirPath);
            $objModule->setKey($strKey);
            $objModule->setConfig($objConfig);
        }

        // Hydrate default module, if required
        if($objModule instanceof DefaultModule)
        {
            $tabBootConfig = $this->getTabModuleBootConfig($strRootDirPath, $objDefaultConfigParserBuilder, $objConfigParserBuilder);
            $objModule->setTabBootConfig($tabBootConfig);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardModuleFactory::DATA_KEY_DEFAULT_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardModuleFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as module root directory full path, if required
            if(!array_key_exists(ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH, $result))
            {
                $result[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrModuleClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of module, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardModuleFactory::GET_CONFIG_TYPE_DEFAULT:
                $result = DefaultModule::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get configuration parser builder object, for module.
     *
     * @param array $tabConfigFormat
     * @return null|ParserBuilderInterface
     */
    protected function getObjModuleConfigParserBuilder(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Init parser builder
        if(array_key_exists(ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER, $tabConfigFormat))
        {
            // Get parser builder object
            $result = $tabConfigFormat[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER];

            // Get parser builder, from configuration, if required
            if(is_array($tabConfigFormat[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER]))
            {
                /** @var ParserBuilderInterface $result */
                $result = $this->getObjInstance(ParserBuilderInterface::class);

                // Create parser builder, if required
                if(is_null($result))
                {
                    // Init parser factory
                    $objParserFactory = $this->getObjInstance(ParserFactoryInterface::class);
                    $objParserFactory = (
                        is_null($objParserFactory) ?
                            new StrTableParserFactory(
                                null,
                                null,
                                null,
                                null,
                                null,
                                $this->getObjProvider()
                            ) :
                            $objParserFactory
                    );

                    // Init file parser factory
                    $objFileParserFactory = $this->getObjInstance(FileParserFactoryInterface::class);
                    $objFileParserFactory = (
                        is_null($objFileParserFactory) ?
                            new StrTableFileParserFactory(
                                $objParserFactory,
                                null,
                                $this->getObjProvider()
                            ) :
                            $objFileParserFactory
                    );

                    // Create parser builder
                    $result = new ParserBuilder(
                        $objParserFactory,
                        $objFileParserFactory
                    );
                }

                // Hydrate parser builder
                $tabConfig = $tabConfigFormat[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER];
                $result->setConfig($tabConfig);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get default configuration parser builder object, for module.
     *
     * @return ParserBuilderInterface
     */
    protected function getObjModuleDefaultConfigParserBuilder()
    {
        // Init var
        $objModuleCollection = $this->getObjModuleCollection();
        $result = $objModuleCollection->getObjConfigParserBuilder();

        // Return result
        return $result;
    }



    /**
     * Get string module root directory full path.
     *
     * @param array $tabConfigFormat
     * @return string
     */
    protected function getStrModuleRootDirPath(array $tabConfigFormat)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfigFormat[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH];
        $result = (
            isset($tabConfig[ConstStandardModuleFactory::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN]) ?
                sprintf($tabConfig[ConstStandardModuleFactory::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN], $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get string module key.
     *
     * @param string $strRootDirPath
     * @param ParserBuilderInterface $objDefaultConfigParserBuilder
     * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @return string
     */
    protected function getStrModuleKey(
        $strRootDirPath,
        ParserBuilderInterface $objDefaultConfigParserBuilder,
        ParserBuilderInterface $objConfigParserBuilder = null
    )
    {
        // Init var
        $tabModuleConfig = ToolBoxModule::getTabConfig(
            $strRootDirPath,
            ConstModule::CONF_PATH_FILE_CONFIG_MODULE,
            is_null($objConfigParserBuilder) ? $objDefaultConfigParserBuilder : $objConfigParserBuilder
        );
        $result = (
            isset($tabModuleConfig[ConstModule::TAB_CONFIG_MODULE_KEY_KEY]) ?
                $tabModuleConfig[ConstModule::TAB_CONFIG_MODULE_KEY_KEY] :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get module configuration object.
     *
     * @param string $strRootDirPath
     * @param ParserBuilderInterface $objDefaultConfigParserBuilder
     * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @return DataConfig
     */
    protected function getObjModuleConfig(
        $strRootDirPath,
        ParserBuilderInterface $objDefaultConfigParserBuilder,
        ParserBuilderInterface $objConfigParserBuilder = null
    )
    {
        // Init var
        $result = new DataConfig(new PathTableData());
        $objData = $result->getObjData();

        // Hydrate path table data configuration
        $tabConfig = array(
            ConstPathTableData::TAB_CONFIG_KEY_PATH_SEPARATOR => ConstConfig::CONFIG_PATH_SEPARATOR
        );
        if($objData instanceof PathTableData)
        {
            $objData->setTabConfig($tabConfig);
        }

        // Hydrate configuration
        $tabData = ToolBoxModule::getTabConfig(
            $strRootDirPath,
            ConstModule::CONF_PATH_FILE_CONFIG_PARAM,
            is_null($objConfigParserBuilder) ? $objDefaultConfigParserBuilder : $objConfigParserBuilder
        );
        if(is_array($tabData))
        {
            $objData->setDataSrc($tabData);
        }

        // Return result
        return $result;
    }



    /**
     * Get module boot configuration array.
     *
     * @param string $strRootDirPath
     * @param ParserBuilderInterface $objDefaultConfigParserBuilder
     * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @return array
     */
    protected function getTabModuleBootConfig(
        $strRootDirPath,
        ParserBuilderInterface $objDefaultConfigParserBuilder,
        ParserBuilderInterface $objConfigParserBuilder = null
    )
    {
        // Init var
        $result = array();

        // Get boot configuration
        $tabData = ToolBoxModule::getTabConfig(
            $strRootDirPath,
            ConstModule::CONF_PATH_FILE_CONFIG_BOOT,
            is_null($objConfigParserBuilder) ? $objDefaultConfigParserBuilder : $objConfigParserBuilder
        );

        // Register boot configuration in result, if required
        if(is_array($tabData))
        {
            $result = $tabData;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws
     */
    protected function getObjModuleNew($strConfigType, array $tabConfigFormat)
    {
        // Init var
        $result = parent::getObjModuleNew($strConfigType, $tabConfigFormat);

        if(is_null($result))
        {
            // Check arguments
            GetConfigInvalidFormatException::setCheck($tabConfigFormat);

            // Get info
            $objDefaultConfigParserBuilder = $this->getObjModuleDefaultConfigParserBuilder();
            $objConfigParserBuilder = $this->getObjModuleConfigParserBuilder($tabConfigFormat);
            $strRootDirPath = $this->getStrModuleRootDirPath($tabConfigFormat);

            $strKey = $this->getStrModuleKey($strRootDirPath, $objDefaultConfigParserBuilder, $objConfigParserBuilder);
            $objConfig = $this->getObjModuleConfig($strRootDirPath, $objDefaultConfigParserBuilder, $objConfigParserBuilder);
            $tabBootConfig = $this->getTabModuleBootConfig($strRootDirPath, $objDefaultConfigParserBuilder, $objConfigParserBuilder);

            // Get class path of module, from type
            switch($strConfigType)
            {
                case null:
                case ConstStandardModuleFactory::GET_CONFIG_TYPE_DEFAULT:
                    $result = new DefaultModule(
                        $strKey,
                        $strRootDirPath,
                        $objConfig,
                        $tabBootConfig,
                        $objConfigParserBuilder
                    );
                    break;
            }
        }

        // Return result
        return $result;
    }



}