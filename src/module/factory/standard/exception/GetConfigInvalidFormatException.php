<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\factory\standard\exception;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\framework\module\factory\standard\library\ConstStandardModuleFactory;



class GetConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardModuleFactory::EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid root dir path
            isset($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH]) &&
            is_string($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH]) &&
            (trim($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH]) != '') &&

            // Check valid config parser builder
            (
                (!isset($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER])) ||
                (
                    (
                        is_array($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER])
                    ) ||
                    (
                        is_object($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER]) &&
                        ($config[ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER] instanceof ParserBuilderInterface)
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}