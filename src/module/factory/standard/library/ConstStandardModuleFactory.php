<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\factory\standard\library;



class ConstStandardModuleFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN = 'file_path_format_pattern';

    // Get configuration
    const TAB_GET_CONFIG_KEY_ROOT_DIR_PATH = 'path';
    const TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER = 'config_parser';

    // Type configuration
    const GET_CONFIG_TYPE_DEFAULT = 'default';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the standard module factory configuration standard.';
    const EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the standard get module factory configuration standard.';
}