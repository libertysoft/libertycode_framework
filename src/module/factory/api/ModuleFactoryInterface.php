<?php
/**
 * Description :
 * This class allows to describe behavior of module factory class.
 * Module factory allows to provide new or specified module instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined module types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\factory\api;

use liberty_code\framework\module\api\ModuleInterface;



interface ModuleFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of module,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrModuleClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance module,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param ModuleInterface $objModule = null
     * @return null|ModuleInterface
     */
    public function getObjModule(
        array $tabConfig,
        $strConfigKey = null,
        ModuleInterface $objModule = null
    );
}