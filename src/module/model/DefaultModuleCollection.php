<?php
/**
 * Description :
 * This class allows to define default module collection class.
 * key: module key => value: module object.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\framework\module\api\ModuleCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\library\table\library\ConstTable;
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\framework\module\library\ConstModule;
use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\module\exception\CollectionKeyInvalidFormatException;
use liberty_code\framework\module\exception\CollectionValueInvalidFormatException;
use liberty_code\framework\module\exception\MergeConfigInvalidFormatException;



class DefaultModuleCollection extends DefaultBean implements ModuleCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var ParserBuilderInterface */
    protected $objConfigParserBuilder;



    /** @var CallFactoryInterface */
    protected $objCallFactory;
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserBuilderInterface $objConfigParserBuilder
     * @param CallFactoryInterface $objCallFactory = null
     */
    public function __construct(
        ParserBuilderInterface $objConfigParserBuilder,
        CallFactoryInterface $objCallFactory = null,
        $tabData = array()
    )
    {
        // Call parent constructor
        parent::__construct($tabData);

        // Init default configuration parser builder
        $this->setConfigParserBuilder($objConfigParserBuilder);

        // Init call factory
        $this->setCallFactory($objCallFactory);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			// Check value argument
			CollectionValueInvalidFormatException::setCheck($value);

			// Check key argument
			/** @var ModuleInterface $value */
			if(
				(!is_string($key)) ||
				($key != $value->getStrKey())
			)
			{
				throw new CollectionKeyInvalidFormatException($key);
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





	// Methods check
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkExists($strKey)
	{
		// Return result
        return (!is_null($this->getObjModule($strKey)));
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjConfigParserBuilder()
    {
        // Return result
        return $this->objConfigParserBuilder;
    }



    /**
     * @inheritdoc
     */
    public function getObjCallFactory()
    {
        // Return result
        return $this->objCallFactory;
    }



	/**
	 * @inheritdoc
	 */
	public function getTabKey()
	{
		// return result
		return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getObjModule($strKey)
	{
		// Init var
		$result = null;
		
		// Try to get module object if found
		try
		{
			if($this->beanDataExists($strKey))
			{
				$result = $this->beanGetData($strKey);
			}
		}
		catch(\Exception $e)
		{
		}
		
		// Return result
		return $result;
	}



    /**
     * Get array of merged module configuration,
     * from specified get module configuration callable.
     *
     * Callable format: null|array function(ModuleInterface $objModule):
     * Allows to get configuration.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
     *
     * @param callable $callableGetTabConfig
     * @param array $tabConfig = null
     * @return null|array
     * @throws MergeConfigInvalidFormatException
     */
    protected function getTabMergeConfig(
        $callableGetTabConfig,
        array $tabConfig = null
    )
    {
        // Check arguments
        MergeConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;
        $strMergeOption = (
            (isset($tabConfig[ConstModule::TAB_MERGE_CONFIG_KEY_MERGE_OPTION])) ?
                $tabConfig[ConstModule::TAB_MERGE_CONFIG_KEY_MERGE_OPTION] :
                ConstTable::OPTION_MERGE_OVERWRITE
        );

        if(is_callable($callableGetTabConfig))
        {
            // Run all modules
            foreach($this->getTabKey() as $strKey)
            {
                // Get info
                $objModule = $this->getObjModule($strKey);
                $tabConfig = $callableGetTabConfig($objModule);

                // Merge data in result if required
                if((!is_null($tabConfig)) && is_array($tabConfig))
                {
                    // Init result if required
                    if(is_null($result))
                    {
                        $result = array();
                    }

                    $result = ToolBoxTable::getTabMerge($result, $tabConfig, $strMergeOption);
                }
                /*
                // Else: break loop and register empty result
                else
                {
                    $result = null;
                    break;
                }
                */
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws MergeConfigInvalidFormatException
     */
    public function getTabMergeConfigParamApp(array $tabConfig = null)
    {
        // Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigParamApp();
            },
            $tabConfig
        );
    }


	
	/**
	 * @inheritdoc
	 * @throws MergeConfigInvalidFormatException
     */
	public function getTabMergeConfigAutoload(array $tabConfig = null)
	{
		// Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigAutoload();
            },
            $tabConfig
        );
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws MergeConfigInvalidFormatException
     */
	public function getTabMergeConfigDi(array $tabConfig = null)
	{
		// Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigDi();
            },
            $tabConfig
        );
	}



	/**
	 * @inheritdoc
	 * @throws MergeConfigInvalidFormatException
	 */
	public function getTabMergeConfigRegister(array $tabConfig = null)
	{
		// Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigRegister();
            },
            $tabConfig
        );
	}



    /**
     * @inheritdoc
     * @throws MergeConfigInvalidFormatException
     */
    public function getTabMergeConfigEvent(array $tabConfig = null)
    {
        // Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigEvent();
            },
            $tabConfig
        );
    }



	/**
	 * @inheritdoc
	 * @throws MergeConfigInvalidFormatException
     */
	public function getTabMergeConfigRouteWeb(array $tabConfig = null)
	{
		// Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigRouteWeb();
            },
            $tabConfig
        );
	}
	
	
	
	/**
	 * @inheritdoc
	 * @throws MergeConfigInvalidFormatException
     */
	public function getTabMergeConfigRouteCmd(array $tabConfig = null)
	{
		// Return result
        return $this->getTabMergeConfig(
            function(ModuleInterface $objModule)
            {
                return $objModule->getTabConfigRouteCmd();
            },
            $tabConfig
        );
	}
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfigParserBuilder(ParserBuilderInterface $objParserBuilder)
    {
        $this->objConfigParserBuilder = $objParserBuilder;
    }



    /**
     * @inheritdoc
     */
    public function setCallFactory(CallFactoryInterface $objCallFactory = null)
    {
        $this->objCallFactory = $objCallFactory;
    }



	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setModule(ModuleInterface $objModule)
	{
		// Init var
		$strKey = $objModule->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objModule);
		
		// return result
		return $strKey;
	}
	
	
	
	/**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

		// Register module collection on module object
		/** @var ModuleInterface $val */
		$val->setModuleCollection($this);
    }



    /**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    public function setTabModule($tabModule)
    {
        // Init var
        $result = array();

        // Case index array of modules
        if(is_array($tabModule))
        {
            // Run all modules and for each, try to set
            foreach($tabModule as $module)
            {
                $strKey = $this->setModule($module);
                $result[] = $strKey;
            }
        }
        // Case collection of modules
        else if($tabModule instanceof ModuleCollectionInterface)
        {
            // Run all modules and for each, try to set
            foreach($tabModule->getTabKey() as $strKey)
            {
                $objModule = $tabModule->getObjModule($strKey);
                $strKey = $this->setModule($objModule);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeModule($strKey)
    {
        // Init var
        $result = $this->getObjModule($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeModuleAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeModule($strKey);
        }
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function boot()
    {
        // Run all modules
        foreach($this->getTabKey() as $strKey)
        {
            // Get info
            $objModule = $this->getObjModule($strKey);

            // Boot
            $objModule->boot();
        }
    }



}