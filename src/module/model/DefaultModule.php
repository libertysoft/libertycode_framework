<?php
/**
 * Description :
 * This class allows to define default module class.
 * Can be consider is base of all module types.
 *
 * Default module uses the following specified boot configuration:
 * [
 *     // Boot configuration 1
 *     [
 *         call(required):[
 *             Call builder destination configuration (@see CallFactoryInterface::getObjCall() )
 *         ],
 *
 *         element(optional): [
 *             'string call element 1',
 *             ...,
 *             'string call element N'
 *         ],
 *
 *         argument(optional): [...call arguments]
 *     ],
 *     ...,
 *     // Boot configuration N
 *     [...]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\framework\module\api\ModuleInterface;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\framework\module\library\ConstModule;
use liberty_code\framework\module\library\ToolBoxModule;
use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\module\exception\ConfigParserBuilderInvalidFormatException;
use liberty_code\framework\module\exception\ModuleCollectionInvalidFormatException;
use liberty_code\framework\module\exception\RootDirPathInvalidFormatException;
use liberty_code\framework\module\exception\KeyInvalidFormatException;
use liberty_code\framework\module\exception\ConfigInvalidFormatException;
use liberty_code\framework\module\exception\BootConfigInvalidFormatException;
use liberty_code\framework\module\exception\CallableUnableGetException;



/**
 * @method array getTabBootConfig() Get boot configuration array.
 * @method void setTabBootConfig(array $tabBootConfig) Set boot configuration array.
 */
class DefaultModule extends FixBean implements ModuleInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
	 * @param string $strKey
	 * @param string $strRootDirPath
	 * @param ConfigInterface $objConfig
     * @param array $tabBootConfig
     * @param ParserBuilderInterface $objConfigParserBuilder = null
     * @param ModuleCollectionInterface $objModuleCollection = null
     */
    public function __construct(
        $strKey,
        $strRootDirPath,
        ConfigInterface $objConfig,
        array $tabBootConfig,
        ParserBuilderInterface $objConfigParserBuilder = null,
        ModuleCollectionInterface $objModuleCollection = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration parser builder, if required
        if(!is_null($objConfigParserBuilder))
        {
            $this->setConfigParserBuilder($objConfigParserBuilder);
        }

        // Init module collection, if required
        if(!is_null($objModuleCollection))
        {
            $this->setModuleCollection($objModuleCollection);
        }

		// Init properties
        $this->setRootDirPath($strRootDirPath);
		$this->setKey($strKey);
		$this->setConfig($objConfig);
        $this->setTabBootConfig($tabBootConfig);
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstModule::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER))
        {
            $this->__beanTabData[ConstModule::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER] = null;
        }

        if(!$this->beanExists(ConstModule::DATA_KEY_DEFAULT_MODULE_COLLECTION))
        {
            $this->__beanTabData[ConstModule::DATA_KEY_DEFAULT_MODULE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstModule::DATA_KEY_DEFAULT_ROOT_DIR_PATH))
        {
            $this->__beanTabData[ConstModule::DATA_KEY_DEFAULT_ROOT_DIR_PATH] = __DIR__ . '/../..';
        }

        if(!$this->beanExists(ConstModule::DATA_KEY_DEFAULT_KEY))
        {
            $this->__beanTabData[ConstModule::DATA_KEY_DEFAULT_KEY] = '';
        }

        if(!$this->beanExists(ConstModule::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstModule::DATA_KEY_DEFAULT_CONFIG] = null;
        }

        if(!$this->beanExists(ConstModule::DATA_KEY_DEFAULT_BOOT_CONFIG))
        {
            $this->__beanTabData[ConstModule::DATA_KEY_DEFAULT_BOOT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstModule::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER,
            ConstModule::DATA_KEY_DEFAULT_MODULE_COLLECTION,
            ConstModule::DATA_KEY_DEFAULT_ROOT_DIR_PATH,
            ConstModule::DATA_KEY_DEFAULT_KEY,
            ConstModule::DATA_KEY_DEFAULT_CONFIG,
            ConstModule::DATA_KEY_DEFAULT_BOOT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstModule::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER:
                    ConfigParserBuilderInvalidFormatException::setCheck($value);
                    break;

                case ConstModule::DATA_KEY_DEFAULT_MODULE_COLLECTION:
                    ModuleCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstModule::DATA_KEY_DEFAULT_ROOT_DIR_PATH:
                    RootDirPathInvalidFormatException::setCheck($value);
                    break;

                case ConstModule::DATA_KEY_DEFAULT_KEY:
                    KeyInvalidFormatException::setCheck($value);
                    break;

                case ConstModule::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstModule::DATA_KEY_DEFAULT_BOOT_CONFIG:
                    BootConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkConfigExists()
    {
        // Return result
        return (count($this->getObjConfig()->getTabKey()) > 0);
    }



    /**
     * @inheritdoc
     */
    public function checkBootExists()
    {
        // Return result
        return (count($this->getTabBootConfig()) > 0);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjConfigParserBuilder()
    {
        // Return result
        return $this->beanGet(ConstModule::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER);
    }



    /**
     * Get default parser builder object, for configuration.
     *
     * @return null|ParserBuilderInterface
     */
    protected function getObjDefaultConfigParserBuilder()
    {
        // Init var
        $result = null;
        $objModuleCollection = $this->getObjModuleCollection();

        // Get default parser builder object, if required
        if(!is_null($objModuleCollection))
        {
            $result = $objModuleCollection->getObjConfigParserBuilder();
        }

        // Return result
        return $result;
    }



    /**
     * Get parser builder object, for configuration.
     *
     * @return null|ParserBuilderInterface
     */
    protected function getObjConfigParserBuilderEngine()
    {
        // Init var
        $result = $this->getObjConfigParserBuilder();
        $result = (
        is_null($result) ?
            $this->getObjDefaultConfigParserBuilder() :
            $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjModuleCollection()
    {
        // Return result
        return $this->beanGet(ConstModule::DATA_KEY_DEFAULT_MODULE_COLLECTION);
    }



    /**
     * Get call factory object.
     *
     * @return null|CallFactoryInterface
     */
    protected function getObjCallFactory()
    {
        // Init var
        $result = null;
        $objModuleCollection = $this->getObjModuleCollection();

        // Get call factory if found
        if(!is_null($objModuleCollection))
        {
            $result = $objModuleCollection->getObjCallFactory();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRootDirPath()
    {
        // Return result
        return $this->beanGet(ConstModule::DATA_KEY_DEFAULT_ROOT_DIR_PATH);
    }



	/**
	 * @inheritdoc
	 */
	public function getStrKey()
	{
        // Return result
        return $this->beanGet(ConstModule::DATA_KEY_DEFAULT_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjConfig()
	{
        // Return result
        return $this->beanGet(ConstModule::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigParamApp()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_PARAM_APP,
            $this->getObjConfigParserBuilderEngine()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigAutoload()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_AUTOLOAD,
            $this->getObjConfigParserBuilderEngine()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigDi()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_DI,
            $this->getObjConfigParserBuilderEngine()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigRegister()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_REGISTER,
            $this->getObjConfigParserBuilderEngine()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigEvent()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_EVENT,
            $this->getObjConfigParserBuilderEngine()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigRouteWeb()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_ROUTE_WEB,
            $this->getObjConfigParserBuilderEngine()
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabConfigRouteCmd()
    {
        // Return result
        return ToolBoxModule::getTabConfig(
            $this->getStrRootDirPath(),
            ConstModule::CONF_PATH_FILE_CONFIG_ROUTE_COMMAND,
            $this->getObjConfigParserBuilderEngine()
        );
    }



	
	
    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfigParserBuilder(ParserBuilderInterface $objParserBuilder = null)
    {
        $this->beanSet(ConstModule::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER, $objParserBuilder);
    }



	/**
	 * @inheritdoc
	 */
	public function setModuleCollection(ModuleCollectionInterface $objModuleCollection = null)
	{
		$this->beanSet(ConstModule::DATA_KEY_DEFAULT_MODULE_COLLECTION, $objModuleCollection);
	}



    /**
     * @inheritdoc
     */
    public function setRootDirPath($strRootDirPath)
    {
        $this->beanSet(ConstModule::DATA_KEY_DEFAULT_ROOT_DIR_PATH, $strRootDirPath);
    }



	/**
	 * @inheritdoc
	 */
    public function setKey($strKey)
	{
        $this->beanSet(ConstModule::DATA_KEY_DEFAULT_KEY, $strKey);
    }



	/**
	 * @inheritdoc
     */
    public function setConfig(ConfigInterface $objConfig)
	{
        $this->beanSet(ConstModule::DATA_KEY_DEFAULT_CONFIG, $objConfig);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function boot()
    {
        // Init var
        $objCallFactory = $this->getObjCallFactory();
        $tabBootConfig = $this->getTabBootConfig();

        if(!is_null($objCallFactory))
        {
            // Run each boot configuration
            foreach($tabBootConfig as $bootConfig)
            {
                // Get callable info
                $tabCallConfig = $bootConfig[ConstModule::TAB_BOOT_CONFIG_KEY_CALL];
                $tabStrCallElm = (
                    array_key_exists(ConstModule::TAB_BOOT_CONFIG_KEY_ELEMENT, $bootConfig) ?
                        $bootConfig[ConstModule::TAB_BOOT_CONFIG_KEY_ELEMENT] :
                        array()
                );
                $tabCallArg = (
                    array_key_exists(ConstModule::TAB_BOOT_CONFIG_KEY_ARGUMENT, $bootConfig) ?
                        $bootConfig[ConstModule::TAB_BOOT_CONFIG_KEY_ARGUMENT] :
                        array()
                );
                $objCall = $objCallFactory->getObjCall($tabCallConfig);
                $callable = (
                    (!is_null($objCall)) ?
                        $objCall->getCallable($tabCallArg, $tabStrCallElm) :
                        null
                );

                // Execute callable, if required (callable found)
                if(!is_null($callable))
                {
                    $callable();
                }
                else
                {
                    throw new CallableUnableGetException($this->getStrKey());
                }
            }
        }
    }



}