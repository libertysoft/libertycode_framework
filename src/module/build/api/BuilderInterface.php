<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified module collection instance, with modules.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\build\api;

use liberty_code\framework\module\api\ModuleCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************


    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified module collection.
     *
     * @param ModuleCollectionInterface $objModuleCollection
     * @param boolean $boolClear = true
     */
    public function hydrateModuleCollection(ModuleCollectionInterface $objModuleCollection, $boolClear = true);
}