<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate module collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate module collection:
 * [
 *     module key 1 => Array of configuration values (@see ModuleFactoryInterface::getObjModule() ),
 *
 *     ...,
 *
 *     module key N => ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\framework\module\build\api\BuilderInterface;

use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\module\factory\api\ModuleFactoryInterface;
use liberty_code\framework\module\build\library\ConstBuilder;
use liberty_code\framework\module\build\exception\FactoryInvalidFormatException;
use liberty_code\framework\module\build\exception\DataSrcInvalidFormatException;



/**
 * @method ModuleFactoryInterface getObjFactory() Get module factory object.
 * @method void setObjFactory(ModuleFactoryInterface $objFactory) Set module factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ModuleFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        ModuleFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init mode factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateModuleCollection(ModuleCollectionInterface $objModuleCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabModule = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new route
            $key = (is_string($key) ? $key : null);
            $objModule = $objFactory->getObjModule($tabConfig, $key);

            // Register module, if found
            if(!is_null($objModule))
            {
                $tabModule[] = $objModule;
            }
            // Throw exception if route not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear modules from collection, if required
        if($boolClear)
        {
            $objModuleCollection->removeModuleAll();
        }

        // Register modules on collection
        $objModuleCollection->setTabModule($tabModule);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
			ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
					DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}

	
	
}