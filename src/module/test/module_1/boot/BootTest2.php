<?php

namespace liberty_code\framework\module\test\module_1\boot;



class BootTest2
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods execute
    // ******************************************************************************

    public function boot()
    {
        $strText = 'Boot: ' . get_class($this);

        echo(sprintf('<br />%1$s<br />', $strText));
    }
}