<?php

namespace liberty_code\framework\module\test\module_1\boot;



class BootTest1
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * @param string $strAdd
     * @param string $strAdd2
     */
    public function boot($strAdd = '', $strAdd2 = '')
    {
        $strText =
            'Boot: ' . get_class($this) . ': ' .
            ((trim($strAdd) != '') ? ': Arg 1 "' . $strAdd . '"' : '') .
            ((trim($strAdd2) != '') ? ': Arg 2 "' . $strAdd2 . '"' : '');

        echo(sprintf('<br />%1$s<br />', $strText));
    }
}