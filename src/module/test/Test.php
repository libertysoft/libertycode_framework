<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/module/test/CallFactoryTest.php');

// Use
use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
use liberty_code\parser\file\factory\string_table\model\StrTableFileParserFactory;
use liberty_code\parser\build\factory\model\FactoryBuilder as ParserBuilder;
use liberty_code\framework\module\model\DefaultModuleCollection;
use liberty_code\framework\module\factory\library\ConstModuleFactory;
use liberty_code\framework\module\factory\standard\library\ConstStandardModuleFactory;
use liberty_code\framework\module\factory\standard\model\StandardModuleFactory;
use liberty_code\framework\module\build\model\DefaultBuilder;



// Init var
$objParserFactory = new StrTableParserFactory();
$objFileParserFactory = new StrTableFileParserFactory($objParserFactory);
$objConfigParserBuilder = new ParserBuilder(
    $objParserFactory,
    $objFileParserFactory,
    array(
        'type' => 'string_table_json_yml'
    )
);
$objModuleCollection = new DefaultModuleCollection($objConfigParserBuilder, $objCallFactory);
$objModuleFactory = new StandardModuleFactory(
    $objModuleCollection,
    array(
        ConstStandardModuleFactory::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '%1$s'
    )
);
$objBuilder = new DefaultBuilder($objModuleFactory);

$tabDataSrc = array(
    '/src/module/test/module_1 not care' => [
        ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH => '/src/module/test/module_1',
        // ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER => [
        //     ConstStandardModuleFactory::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootAppPath . '%1$s'
        // ]
    ],
    '/src/module/test/module_2' => [
        ConstModuleFactory::TAB_CONFIG_KEY_TYPE => ConstStandardModuleFactory::GET_CONFIG_TYPE_DEFAULT,
        ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER => [
            'type' => 'string_table_json'
        ]
    ],
    [
        ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_ROOT_DIR_PATH => '/src/module/test/module_3',
        ConstStandardModuleFactory::TAB_GET_CONFIG_KEY_CONFIG_PARSER_BUILDER => [
            'type' => 'string_table_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
            'root_node_name' => 'root'
        ]
    ]
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');




// Test module
$objBuilder->hydrateModuleCollection($objModuleCollection);
foreach($objModuleCollection->getTabKey() as $strKey)
{
    $objModule = $objModuleCollection->getObjModule($strKey);

	echo('Test module "' . $strKey . '": <br />');
	try{
		echo('Collection check: <pre>');var_dump($objModuleCollection->checkExists($strKey));echo('</pre>');
		
		echo('Module collection count: <pre>');var_dump(count($objModule->getObjModuleCollection()->getTabKey()));echo('</pre>');
        echo('Module config: <pre>');
        var_dump(
            (!is_null($objModule->getObjConfigParserBuilder())) ?
                $objModule->getObjConfigParserBuilder()->getTabConfig() :
                null
        );
        echo('</pre>');
		echo('Module root directory path: <pre>');var_dump($objModule->getStrRootDirPath());echo('</pre>');
        echo('Module key: <pre>');var_dump($objModule->getStrKey());echo('</pre>');
        echo('Module config: <pre>');var_dump($objModule->getObjConfig());echo('</pre>');

        echo('Module check config found: <pre>');var_dump($objModule->checkConfigExists());echo('</pre>');
        echo('Module check boot found: <pre>');var_dump($objModule->checkBootExists());echo('</pre>');

        echo('Module config array param app: <pre>');var_dump($objModule->getTabConfigParamApp());echo('</pre>');
		echo('Module config array autoload: <pre>');var_dump($objModule->getTabConfigAutoload());echo('</pre>');
		echo('Module config array di: <pre>');var_dump($objModule->getTabConfigDi());echo('</pre>');
		echo('Module config array register: <pre>');var_dump($objModule->getTabConfigRegister());echo('</pre>');
        echo('Module config array event: <pre>');var_dump($objModule->getTabConfigEvent());echo('</pre>');
		echo('Module config array route web: <pre>');var_dump($objModule->getTabConfigRouteWeb());echo('</pre>');
		echo('Module config array route command <pre>');var_dump($objModule->getTabConfigRouteCmd());echo('</pre>');
		
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test merge configuration
echo('Merge configuration parameters app: <pre>');var_dump($objModuleCollection->getTabMergeConfigParamApp());echo('</pre>');
echo('<br />');

echo('Merge configuration auto-loading: <pre>');var_dump($objModuleCollection->getTabMergeConfigAutoload());echo('</pre>');
echo('<br />');

echo('Merge configuration dependency injection: <pre>');var_dump($objModuleCollection->getTabMergeConfigDi());echo('</pre>');
echo('<br />');

echo('Merge configuration register: <pre>');var_dump($objModuleCollection->getTabMergeConfigRegister());echo('</pre>');
echo('<br />');

echo('Merge configuration event: <pre>');var_dump($objModuleCollection->getTabMergeConfigEvent());echo('</pre>');
echo('<br />');

echo('Merge configuration web routes: <pre>');var_dump($objModuleCollection->getTabMergeConfigRouteWeb());echo('</pre>');
echo('<br />');

echo('Merge configuration command line routes: <pre>');var_dump($objModuleCollection->getTabMergeConfigRouteCmd());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test boot
echo('Boot: <br />');
$objModuleCollection->boot();

echo('<br /><br /><br />');


