<?php

namespace liberty_code\framework\module\test\module_2\boot;



class BootTest3
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * @param string $strAdd
     * @param string $strAdd2
     */
    public function boot1($strAdd = '', $strAdd2 = '')
    {
        $strText =
            'Boot 1: ' . get_class($this) . ': ' .
            ((trim($strAdd) != '') ? ': Arg 1 "' . $strAdd . '"' : '') .
            ((trim($strAdd2) != '') ? ': Arg 2 "' . $strAdd2 . '"' : '');

        echo(sprintf('<br />%1$s<br />', $strText));
    }



    public function boot2()
    {
        $strText = 'Boot 2: ' . get_class($this);

        echo(sprintf('<br />%1$s<br />', $strText));
    }
}