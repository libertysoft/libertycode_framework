<?php
/**
 * Description :
 * This class allows to describe behavior of modules collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\api;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\framework\module\api\ModuleInterface;



interface ModuleCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************
	
	/**
	 * Check if specified module key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get default parser builder object, for configuration.
     *
     * @return ParserBuilderInterface
     */
    public function getObjConfigParserBuilder();



    /**
     * Get call factory object.
     *
     * @return null|CallFactoryInterface
     */
    public function getObjCallFactory();



	/**
	 * Get index array of string module keys.
	 * 
	 * @return array
	 */
	public function getTabKey();
	
	
	
	/**
	 * Get module from specified key.
	 * 
	 * @param string $strKey
	 * @return null|ModuleInterface
	 */
	public function getObjModule($strKey);



    /**
     * Get array of merged configuration application parameters.
     *
     * Configuration format:
     * Merge configuration can be provided.
     * Null means no specific merge required.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    public function getTabMergeConfigParamApp(array $tabConfig = null);



	/**
     * Get array of merged configuration auto-loading.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
     *
	 * @param array $tabConfig = null
     * @return null|array
     */
	public function getTabMergeConfigAutoload(array $tabConfig = null);
	
	
	
	/**
     * Get array of merged configuration dependency injection.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
     *
	 * @param array $tabConfig = null
     * @return null|array
     */
	public function getTabMergeConfigDi(array $tabConfig = null);



	/**
	 * Get array of merged configuration register.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
	 *
	 * @param array $tabConfig = null
	 * @return null|array
	 */
	public function getTabMergeConfigRegister(array $tabConfig = null);



    /**
     * Get array of merged configuration event.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    public function getTabMergeConfigEvent(array $tabConfig = null);



	/**
     * Get array of merged configuration web routes.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
     *
	 * @param array $tabConfig = null
     * @return null|array
     */
	public function getTabMergeConfigRouteWeb(array $tabConfig = null);
	
	
	
	/**
     * Get array of merged configuration command line routes.
     *
     * Configuration format: @see ModuleCollectionInterface::getTabMergeConfigParamApp()
     *
	 * @param array $tabConfig = null
     * @return null|array
     */
	public function getTabMergeConfigRouteCmd(array $tabConfig = null);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set default parser builder object, for configuration.
     *
     * @param ParserBuilderInterface $objParserBuilder
     */
    public function setConfigParserBuilder(ParserBuilderInterface $objParserBuilder);



    /**
     * Set call factory object.
     *
     * @param CallFactoryInterface $objCallFactory
     */
    public function setCallFactory(CallFactoryInterface $objCallFactory);



	/**
	 * Set module and return its key.
	 * 
	 * @param ModuleInterface $objModule
	 * @return string
     */
	public function setModule(ModuleInterface $objModule);



    /**
     * Set list of modules (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|ModuleInterface $tabModule
     * @return array
     */
    public function setTabModule($tabModule);



    /**
     * Remove module and return its instance.
     *
     * @param string $strKey
     * @return ModuleInterface
     */
    public function removeModule($strKey);



    /**
     * Remove all modules.
     */
    public function removeModuleAll();





    // Methods execute
    // ******************************************************************************

    /**
     * Boot all modules.
     */
    public function boot();
}