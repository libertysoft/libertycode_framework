<?php
/**
 * Description :
 * This class allows to describe behavior of module class.
 * Module is item, with all information about a specified part of application.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\module\api;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\module\api\ModuleCollectionInterface;



interface ModuleInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if configuration found.
     *
     * @return boolean
     */
    public function checkConfigExists();



    /**
     * Check if boot found.
     *
     * @return boolean
     */
    public function checkBootExists();





	// Methods getters
	// ******************************************************************************

    /**
     * Get parser builder object, for configuration.
     *
     * @return null|ParserBuilderInterface
     */
    public function getObjConfigParserBuilder();



	/**
	 * Get module collection object.
	 *
	 * @return null|ModuleCollectionInterface
	 */
	public function getObjModuleCollection();



    /**
     * Get string module root directory full path.
     *
     * @return string
     */
    public function getStrRootDirPath();



	/**
	 * Get string key (considered as module id).
	 *
	 * @return string
	 */
	public function getStrKey();



    /**
     * Get configuration object.
     *
     * @return ConfigInterface
     */
    public function getObjConfig();



    /**
     * Get array of configuration application parameters.
     *
     * @return null|array
     */
    public function getTabConfigParamApp();



    /**
     * Get array of configuration auto-loading.
     *
     * @return null|array
     */
    public function getTabConfigAutoload();



    /**
     * Get array of configuration dependency injection.
     *
     * @return null|array
     */
    public function getTabConfigDi();



    /**
     * Get array of configuration register.
     *
     * @return null|array
     */
    public function getTabConfigRegister();



    /**
     * Get array of configuration event.
     *
     * @return null|array
     */
    public function getTabConfigEvent();



    /**
     * Get array of configuration web routes.
     *
     * @return null|array
     */
    public function getTabConfigRouteWeb();



    /**
     * Get array of command line routes.
     *
     * @return null|array
     */
    public function getTabConfigRouteCmd();



	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set parser builder object, for configuration.
     *
     * @param ParserBuilderInterface $objParserBuilder = null
     */
    public function setConfigParserBuilder(ParserBuilderInterface $objParserBuilder = null);



	/**
	 * Set module collection object.
	 * 
	 * @param ModuleCollectionInterface $objModuleCollection = null
	 */
	public function setModuleCollection(ModuleCollectionInterface $objModuleCollection = null);



    /**
     * Set string module root directory full path.
     *
     * @param string $strRootDirPath
     */
    public function setRootDirPath($strRootDirPath);



    /**
     * Set string key (considered as module id).
     *
     * @param string $strKey
     */
    public function setKey($strKey);



    /**
     * Set configuration object.
     *
     * @param ConfigInterface $objConfig
     */
    public function setConfig(ConfigInterface $objConfig);





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot();
}