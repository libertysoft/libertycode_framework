<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\autoload\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\autoload\rule\factory\standard\model\StandardRuleFactory;
use liberty_code\autoload\build\model\DefaultBuilder;
use liberty_code\autoload\load\model\Loader;
use liberty_code\framework\framework\cache\repository\library\ToolBoxRepository;
use liberty_code\framework\framework\application\library\ToolBoxApp;
use liberty_code\framework\framework\autoload\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate rule collection,
     * from specified auto-loader object.
     *
     * @param Loader $objLoader $objLoader
     */
    protected static function hydrateRuleCollection(Loader $objLoader)
    {
        // Init var
        $objRuleCollection = $objLoader->getObjRuleCollection();
        $objApp = ToolBoxApp::getObjApp();
        $tabDataSrc = ToolBoxRepository::getSetItem(
            ConstBuilder::CACHE_KEY_CONFIG_LOADER,
            function() use ($objApp) {
                return (
                    (!is_null($tabConfig = $objApp
                        ->getObjModuleCollection()
                        ->getTabMergeConfigAutoload())) ?
                        $tabConfig :
                        array()
                );
            }
        );

        // Hydrate rule collection object, if required
        if(is_array($tabDataSrc))
        {
            $objFactory = new StandardRuleFactory();
            $objBuilder = new DefaultBuilder($objFactory);
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateRuleCollection($objRuleCollection);
        }
    }



    /**
     * Hydrate specified auto-loader object.
     *
     * @param Loader $objLoader
     */
    public static function hydrateLoader(Loader $objLoader)
    {
        static::hydrateRuleCollection($objLoader);
    }



}