<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\autoload\factory\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\autoload\rule\library\ConstRule;
use liberty_code\autoload\rule\model\DefaultRuleCollection;
use liberty_code\autoload\load\model\Loader;



class ToolBoxLoaderFactory extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

	/**
     * Get auto-loader object,
	 * from specified application folder root full path.
     *
     * @param string $strAppRootPath
     * @return Loader
     */
    public static function getObjLoader($strAppRootPath)
    {
        // Init var
        $tabConfig = array(
            ConstRule::TAB_COLLECTION_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strAppRootPath . '%1$s'
        );
        $objRuleCollection = new DefaultRuleCollection($tabConfig);
        $objRuleCollection->setMultiMatch(false);
        $result = new Loader($objRuleCollection);
		
		// Return result
		return $result;
    }



}