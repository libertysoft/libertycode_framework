<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\application\factory\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
use liberty_code\parser\file\factory\model\DefaultFileParserFactory;
use liberty_code\parser\file\factory\string_table\model\StrTableFileParserFactory;
use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\parser\build\factory\model\FactoryBuilder as ParserBuilder;
use liberty_code\data\data\table\path\library\ConstPathTableData;
use liberty_code\data\data\table\path\model\PathTableData;
use liberty_code\register\item\model\ItemCollection;
// use liberty_code\register\item\instance\model\InstanceCollection;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\cache\repository\format\model\FormatRepository;
// use liberty_code\cache\repository\format\build\library\ToolBoxFormatBuilder;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\di\dependency\library\ConstDependency;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\call\call\file\library\ConstFileCall;
use liberty_code\call\call\di\clss\library\ConstClassCall;
use liberty_code\call\call\di\dependency\library\ConstDependencyCall;
use liberty_code\call\call\di\func\library\ConstFunctionCall;
use liberty_code\call\call\factory\file\model\FileCallFactory;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\call\call\factory\di\model\DiCallFactory;
use liberty_code\route\route\model\DefaultRouteCollection;
use liberty_code\route\router\api\RouterInterface;
use liberty_code\route\router\model\DefaultRouter;
use liberty_code\route\call\call\factory\model\RouteCallFactory;
use liberty_code\event\event\model\DefaultEventCollection;
use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\event\observer\model\DefaultObserver;
use liberty_code\event\call\call\factory\model\EventCallFactory;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\request_flow\front\library\ConstFrontController;
use liberty_code\controller\controller\api\ControllerInterface;
use liberty_code\controller\controller\exception\AccessDeniedException;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\http\request_flow\front\model\HttpFrontController;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\request_flow\front\info\library\ConstInfoFrontController;
use liberty_code\command\request_flow\front\info\model\InfoFrontController;
use liberty_code\file\register\directory\library\ConstDirRegister;
use liberty_code\file\register\directory\model\DirRegister;
use liberty_code\error\handler\library\ConstHandler;
use liberty_code\error\handler\model\DefaultHandlerCollection;
use liberty_code\error\tryer\library\ConstTryer;
use liberty_code\error\tryer\api\TryerInterface;
use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\model\DefaultWarnHandlerCollection;
use liberty_code\error\warning\tryer\library\ConstWarnTryer;
use liberty_code\error\warning\tryer\model\DefaultWarnTryer;
use liberty_code\framework\config\library\ConstConfig;
use liberty_code\framework\event\library\ConstEvent;
use liberty_code\framework\request_flow\request\http\model\HttpRequest;
use liberty_code\framework\app_mode\mode\model\DefaultModeCollection;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code\framework\app_mode\select\model\DefaultSelector;
use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\module\model\DefaultModuleCollection;
use liberty_code\framework\application\library\ConstApp;
use liberty_code\framework\application\exception\TabConfigInvalidFormatException;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\application\model\DefaultApp;
use liberty_code\framework\framework\library\ConstFramework;



class ToolBoxAppFactory extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if controller right access found,
     * from specified array of call parameters.
     *
     * @param array $tabCallParam = array()
     * @throws AccessDeniedException
     */
    protected static function setCheckControllerAccess(array $tabCallParam)
    {
        // Init var
        $objInstance = null;
        $strMethodNm = null;
        $tabMethodArg = null;

        // Get arguments
        if(
            isset($tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE]) &&
            isset($tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME]) &&
            isset($tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT])
        )
        {
            $objInstance = $tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE];
            $strMethodNm = $tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME];
            $tabMethodArg = $tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT];
        }
        else if(
            isset($tabCallParam[ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE]) &&
            isset($tabCallParam[ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME]) &&
            isset($tabCallParam[ConstDependencyCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT])
        )
        {
            $objInstance = $tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_INSTANCE];
            $strMethodNm = $tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_METHOD_NAME];
            $tabMethodArg = $tabCallParam[ConstClassCall::TAB_CALLABLE_PARAM_ARG_KEY_ARGUMENT];
        }

        // Check controller right access, if required
        if(
            (!is_null($objInstance)) &&
            ($objInstance instanceof ControllerInterface) &&
            (!is_null($strMethodNm)) &&
            (!is_null($tabMethodArg))
        )
        {
            // Check right access controller
            $boolAccess =
                $objInstance->checkAccess() &&
                $objInstance->checkAccessMethod($strMethodNm, $tabMethodArg);

            // Throw access exception if required
            if(!$boolAccess)
            {
                throw new AccessDeniedException(get_class($objInstance));
            }
        }
    }





	// Methods getters
	// ******************************************************************************

    /**
     * Get application root directory path,
     * from specified application configuration.
     *
     * @param array $tabAppConfig
     * @return string
     * @throws TabConfigInvalidFormatException
     */
    protected static function getStrAppRootPath(array $tabAppConfig)
    {
        // Set check arguments
        TabConfigInvalidFormatException::setCheck($tabAppConfig);

        // Init var
        $result = $tabAppConfig[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH];

        // Return result
        return $result;
    }



    /**
     * Get call factory object.
     *
     * @param string $strAppRootPath
     * @param ProviderInterface $objProvider
     * @param RouterInterface $objRouter
     * @param ObserverInterface $objObserver
     * @param callable $callableBefore = null
     * @param callable $callableAfter = null
     * @return CallFactoryInterface
     */
    protected static function getObjCallFactory(
        $strAppRootPath,
        ProviderInterface $objProvider,
        RouterInterface $objRouter,
        ObserverInterface $objObserver,
        $callableBefore = null,
        $callableAfter = null
    )
    {
        $tabConfig = array(
            ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strAppRootPath . '%1$s',
            ConstFileCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true,
            ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strAppRootPath . '%1$s',
            ConstFunctionCall::TAB_CONFIG_KEY_FILE_PATH_FORMAT_REQUIRE => true
        );

        // Init result
        $result = new EventCallFactory(
            $objObserver,
            $tabConfig,
            $callableBefore,
            $callableAfter,
            null,
            $objProvider
        );

        $result = new RouteCallFactory(
            $objRouter,
            $result->getTabConfig(),
            $result->getCallableBefore(),
            $result->getCallableAfter(),
            $result,
            $objProvider
        );

        $result = new FileCallFactory(
            $result->getTabConfig(),
            $result->getCallableBefore(),
            $result->getCallableAfter(),
            $result,
            $objProvider
        );

        $result = new DiCallFactory(
            $objProvider,
            $result->getTabConfig(),
            $result->getCallableBefore(),
            $result->getCallableAfter(),
            $result,
            $objProvider
        );

        // Return result
        return $result;
    }
	
	
	
	/**
     * Get http default application object, 
	 * from specified application configuration.
     *
     * @param array $tabAppConfig
	 * @param array $tabAppConfigParserType
     * @return AppInterface
     */
    public static function getObjHttpDefaultApp(array $tabAppConfig, array $tabAppConfigParserType)
    {
        // Init application root directory
        $strAppRootPath = static::getStrAppRootPath($tabAppConfig);

        // Init application objects
        /** @var ParserBuilderInterface $objAppConfigParserBuilder */
        /** @var SelectorInterface $objAppAppModeSelector */
        /** @var RepositoryInterface $objAppCacheRepo */
        /** @var ModuleCollectionInterface $objAppModuleCollection */
        /** @var ConfigInterface $objAppConfig */
        /** @var ProviderInterface $objAppProvider */
        /** @var RegisterInterface $objAppRegister */
        /** @var RouterInterface $objAppRouter */
        /** @var ObserverInterface $objAppObserver */
        $objAppConfigParserBuilder = null;
        $objAppAppModeSelector = null;
        $objAppCacheRepo = null;
        $objAppModuleCollection = null;
        $objAppConfig = null;
        $objAppProvider = null;
        $objAppRegister = null;
        $objAppRouter = null;
        $objAppObserver = null;
        $objAppExecTryer = null;
        $objAppSysTryer = null;
        static::setAppObj(
            $strAppRootPath,
            $tabAppConfigParserType,
            $objAppConfigParserBuilder,
            $objAppAppModeSelector,
            $objAppCacheRepo,
            $objAppModuleCollection,
            $objAppConfig,
            $objAppProvider,
            $objAppRegister,
            $objAppRouter,
            $objAppObserver,
            $objAppExecTryer,
            $objAppSysTryer
        );

		// Get front controller
        /** @var HttpRequest $objActiveRequest */
        $objActiveRequest = HttpRequest::instanceGetDefault();
        $objDefaultResponse = new HttpResponse();
        $objDefaultResponse->setIntStatusCode(200);
        $objFrontController = new HttpFrontController(
            $objAppRouter,
            $objActiveRequest,
            $objDefaultResponse,
            true,
            ConstFrontController::OPTION_SELECT_RESPONSE_VALUE_LAST
        );

        // Set application
        $result = new DefaultApp(
            $tabAppConfig,
            $objAppConfigParserBuilder,
            $objAppAppModeSelector,
            $objAppCacheRepo,
            $objAppModuleCollection,
            $objAppConfig,
            $objAppProvider,
            $objAppRegister,
            $objAppObserver,
            $objFrontController,
            $objAppExecTryer,
            $objAppSysTryer
        );
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get command line default application object,
     * from specified application configuration.
     *
	 * @param array $tabAppConfig
     * @param array $tabAppConfigParserType
     * @return AppInterface
     */
    public static function getObjCommandDefaultApp(array $tabAppConfig, array $tabAppConfigParserType)
    {
        // Init application root directory
        $strAppRootPath = static::getStrAppRootPath($tabAppConfig);

		/// Init application objects
        /** @var ParserBuilderInterface $objAppConfigParserBuilder */
        /** @var SelectorInterface $objAppAppModeSelector */
        /** @var RepositoryInterface $objAppCacheRepo */
        /** @var ModuleCollectionInterface $objAppModuleCollection */
        /** @var ConfigInterface $objAppConfig */
        /** @var ProviderInterface $objAppProvider */
        /** @var RegisterInterface $objAppRegister */
        /** @var RouterInterface $objAppRouter */
        /** @var ObserverInterface $objAppObserver */
        $objAppConfigParserBuilder = null;
        $objAppAppModeSelector = null;
        $objAppCacheRepo = null;
        $objAppModuleCollection = null;
        $objAppConfig = null;
        $objAppProvider = null;
        $objAppRegister = null;
        $objAppRouter = null;
        $objAppObserver = null;
        $objAppExecTryer = null;
        $objAppSysTryer = null;
        static::setAppObj(
            $strAppRootPath,
            $tabAppConfigParserType,
            $objAppConfigParserBuilder,
            $objAppAppModeSelector,
            $objAppCacheRepo,
            $objAppModuleCollection,
            $objAppConfig,
            $objAppProvider,
            $objAppRegister,
            $objAppRouter,
            $objAppObserver,
            $objAppExecTryer,
            $objAppSysTryer
        );

        // Get front controller
        /** @var CommandRequest $objActiveRequest */
        $objActiveRequest = CommandRequest::instanceGetDefault();
        $objDefaultResponse = new DefaultResponse();
        $objFrontController = new InfoFrontController(
            $objAppRouter,
            $objActiveRequest,
            $objDefaultResponse,
            true,
            ConstFrontController::OPTION_SELECT_RESPONSE_VALUE_LAST
        );

        // Hydrate front controller info response option
        $objFrontController->setBoolInfoCommandOpt(true);
        $objFrontController->setBoolInfoSummaryOpt(true);
        $objFrontController->setTabInfoOptionName(ConstInfoFrontController::DATA_DEFAULT_VALUE_INFO_OPTION_NAME);

        // Set application
        $result = new DefaultApp(
            $tabAppConfig,
            $objAppConfigParserBuilder,
            $objAppAppModeSelector,
            $objAppCacheRepo,
            $objAppModuleCollection,
            $objAppConfig,
            $objAppProvider,
            $objAppRegister,
            $objAppObserver,
            $objFrontController,
            $objAppExecTryer,
            $objAppSysTryer
        );

		// Return result
		return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set application objects,
     * from specified application configuration.
     *
     * @param string $strAppRootPath
     * @param array $tabAppConfigParserType
     * @param ParserBuilderInterface &$objAppConfigParserBuilder = null,
     * @param SelectorInterface &$objAppAppModeSelector = null,
     * @param RepositoryInterface &$objAppCacheRepo = null,
     * @param ModuleCollectionInterface &$objAppModuleCollection = null,
     * @param ConfigInterface &$objAppConfig = null,
     * @param ProviderInterface &$objAppProvider = null,
     * @param RegisterInterface &$objAppRegister = null,
     * @param RouterInterface &$objAppRouter = null,
     * @param ObserverInterface &$objAppObserver = null
     * @param TryerInterface &$objAppExecTryer = null
     * @param TryerInterface &$objAppSysTryer = null
     */
    protected static function setAppObj(
        $strAppRootPath,
        array $tabAppConfigParserType,
        ParserBuilderInterface &$objAppConfigParserBuilder = null,
        SelectorInterface &$objAppAppModeSelector = null,
        RepositoryInterface &$objAppCacheRepo = null,
        ModuleCollectionInterface &$objAppModuleCollection = null,
        ConfigInterface &$objAppConfig = null,
        ProviderInterface &$objAppProvider = null,
        RegisterInterface &$objAppRegister = null,
        RouterInterface &$objAppRouter = null,
        ObserverInterface &$objAppObserver = null,
        TryerInterface &$objAppExecTryer = null,
        TryerInterface &$objAppSysTryer = null
    )
    {
        // Get application mode selector, for application
        $objAppModeCollection = new DefaultModeCollection();
        $objAppAppModeSelector = new DefaultSelector($objAppModeCollection);

        // Get cache repository, for application
        $strFmkCachePath = $strAppRootPath . ConstFramework::CONF_PATH_DIR_VAR_FRAMEWORK_CACHE;
        $objRegister = new DirRegister(array(
            ConstDirRegister::TAB_CONFIG_KEY_STORE_DIR_PATH => $strFmkCachePath
        ));
        $objFormatDataGet = new FormatData();
        $objFormatDataSet = new FormatData();
        $objAppCacheRepo = new FormatRepository(
            null,
            $objRegister,
            $objFormatDataGet,
            $objFormatDataSet
        );
        // Not necessary to put serial formatting, DirRegister do it
        //ToolBoxFormatBuilder::hydrateSerialFormat($objAppCacheRepo);

        // Get config, for application
        $tabConfig = array(
            ConstPathTableData::TAB_CONFIG_KEY_PATH_SEPARATOR => ConstConfig::CONFIG_PATH_SEPARATOR
        );
        $objData = new PathTableData($tabConfig);
        $objAppConfig = new DataConfig($objData);

        // Get DI provider, for application
        //$objInstanceCollection = new InstanceCollection();
        //$objRegister = new MemoryRegister($objInstanceCollection);
        $objRegister = new DefaultTableRegister();
        $objDepCollection = new DefaultDependencyCollection(
            $objRegister,
            $objAppConfig,
            array(
                ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE => true,
                ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH => ConstDependency::CONFIG_SELECT_CLASS_PATH_LAST,
                ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE => false,
                ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE => true,
                ConstDependency::TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION => 0
            )
        );
        $objAppProvider = new DefaultProvider($objDepCollection);

        // Get configuration parser builder, for application
        $objParserFactory = new StrTableParserFactory(
            null,
            null,
            null,
            null,
            null,
            $objAppProvider
        );
        $objFileParserFactory = new DefaultFileParserFactory(
            $objParserFactory,
            null,
            $objAppProvider
        );
        $objFileParserFactory = new StrTableFileParserFactory(
            $objParserFactory,
            $objFileParserFactory,
            $objAppProvider
        );
        $objAppConfigParserBuilder = new ParserBuilder($objParserFactory, $objFileParserFactory);
        $objAppConfigParserBuilder->setConfig($tabAppConfigParserType);

        // Get module collection, for application
        $objAppModuleCollection = new DefaultModuleCollection($objAppConfigParserBuilder);

        // Get register, for application
        $objItemCollection = new ItemCollection();
        $objAppRegister = new MemoryRegister($objItemCollection);

        // Get router, for application
        $objRouteCollection = new DefaultRouteCollection();
        $objAppRouter = new DefaultRouter($objRouteCollection);
        $objRouteCollection->setMultiMatch(false);

        // Get event observer, for application
        $objEventCollection = new DefaultEventCollection();
        $objAppObserver = new DefaultObserver($objEventCollection);

        // Get execution tryer, for application
        $objWarnRegister = new DefaultTableRegister();
        $objHandlerCollection = new DefaultHandlerCollection(
            array(
                ConstHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE => true
            )
        );
        $objWarnHandlerCollection = new DefaultWarnHandlerCollection(
            array(
                ConstWarnHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE => true
            )
        );
        $objAppExecTryer = new DefaultWarnTryer(
            $objHandlerCollection,
            $objWarnHandlerCollection,
            $objWarnRegister,
            array(
                ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN =>
                    ConstTryer::CONFIG_SELECT_THROW_EXECUTION_RETURN_LAST,
                ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE => true,
                ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN =>
                    ConstWarnTryer::CONFIG_SELECT_WARN_EXECUTION_RETURN_LAST,
                ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE => false
            )
        );

        // Get system tryer, for application
        $objHandlerCollection = new DefaultHandlerCollection(
            array(
                ConstHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE => true
            )
        );
        $objWarnHandlerCollection = new DefaultWarnHandlerCollection(
            array(
                ConstWarnHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE => true
            )
        );
        $objAppSysTryer = new DefaultWarnTryer(
            $objHandlerCollection,
            $objWarnHandlerCollection,
            $objWarnRegister,
            array(
                ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN =>
                    ConstTryer::CONFIG_SELECT_THROW_EXECUTION_RETURN_LAST,
                ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE => true,
                ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN =>
                    ConstWarnTryer::CONFIG_SELECT_WARN_EXECUTION_RETURN_LAST,
                ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE => true
            )
        );

        // Set call factory for module collection
        $objCallFactory = static::getObjCallFactory(
            $strAppRootPath,
            $objAppProvider,
            $objAppRouter,
            $objAppObserver
        );

        $objAppModuleCollection->setCallFactory($objCallFactory);

        // Set call factory for route collection
        $funcBeforeRouteExec = function (array $tabCallParam) use ($objAppObserver) {
            // Check controller, if required
            static::setCheckControllerAccess($tabCallParam);

            // Dispatch event before
            $objAppObserver->dispatch(ConstEvent::EVENT_NAME_REQUEST_FLOW_BEFORE_ROUTE_EXECUTION, array($tabCallParam));
        };

        $funcAfterRouteExec = function (array $tabCallParam) use ($objAppObserver) {
            // Dispatch event after
            $objAppObserver->dispatch(ConstEvent::EVENT_NAME_REQUEST_FLOW_AFTER_ROUTE_EXECUTION, array($tabCallParam));
        };

        $objCallFactory = static::getObjCallFactory(
            $strAppRootPath,
            $objAppProvider,
            $objAppRouter,
            $objAppObserver,
            $funcBeforeRouteExec,
            $funcAfterRouteExec
        );

        $objRouteCollection->setCallFactory($objCallFactory);

        // Set call factory for event collection
        $objCallFactory = static::getObjCallFactory(
            $strAppRootPath,
            $objAppProvider,
            $objAppRouter,
            $objAppObserver
        );

        $objEventCollection->setCallFactory($objCallFactory);
    }



}