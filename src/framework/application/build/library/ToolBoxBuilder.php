<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\application\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\parser\file\factory\api\FileParserFactoryInterface;
use liberty_code\parser\build\factory\model\FactoryBuilder as ParserBuilder;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\di\dependency\api\DependencyCollectionInterface;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\factory\api\DependencyFactoryInterface;
use liberty_code\di\dependency\factory\standard\model\StandardDependencyFactory;
use liberty_code\di\build\api\BuilderInterface as DiBuilderInterface;
use liberty_code\di\build\model\DefaultBuilder as DefaultDiBuilder;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\call\call\factory\api\CallFactoryInterface;
use liberty_code\route\route\api\RouteCollectionInterface;
use liberty_code\route\route\factory\api\RouteFactoryInterface;
use liberty_code\route\build\api\BuilderInterface as RouteBuilderInterface;
use liberty_code\route\build\model\DefaultBuilder as DefaultRouteBuilder;
use liberty_code\route\router\api\RouterInterface;
use liberty_code\event\event\api\EventCollectionInterface;
use liberty_code\event\event\factory\api\EventFactoryInterface;
use liberty_code\event\event\factory\standard\model\StandardEventFactory;
use liberty_code\event\build\api\BuilderInterface as EventBuilderInterface;
use liberty_code\event\build\model\DefaultBuilder as DefaultEventBuilder;
use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\request_flow\request\api\RequestInterface;
use liberty_code\request_flow\front\api\FrontControllerInterface;
use liberty_code\http\request_flow\request\model\HttpRequest as BaseHttpRequest;
use liberty_code\http\request_flow\front\model\HttpFrontController;
use liberty_code\command\route\factory\model\CommandRouteFactory;
use liberty_code\command\request_flow\request\model\CommandRequest;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code\command\request_flow\front\info\model\InfoFrontController;
use liberty_code\error\handler\api\HandlerCollectionInterface;
use liberty_code\error\handler\factory\api\HandlerFactoryInterface;
use liberty_code\error\handler\factory\model\DefaultHandlerFactory;
use liberty_code\error\handler\factory\standard\model\StandardHandlerFactory;
use liberty_code\error\build\api\BuilderInterface as ErrorBuilderInterface;
use liberty_code\error\build\model\DefaultBuilder as DefaultErrorBuilder;
use liberty_code\error\tryer\api\TryerInterface;
use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;
use liberty_code\error\warning\handler\factory\api\WarnHandlerFactoryInterface;
use liberty_code\error\warning\handler\factory\model\DefaultWarnHandlerFactory;
use liberty_code\error\warning\build\api\BuilderInterface as WarnBuilderInterface;
use liberty_code\error\warning\build\model\DefaultBuilder as DefaultWarnBuilder;
use liberty_code\error\warning\tryer\api\WarnTryerInterface;
use liberty_code\error\warning\tryer\model\DefaultWarnTryer;
use liberty_code\error\warning\error\handler\factory\model\WarnHandlerFactory;
use liberty_code\framework\route\route\http\factory\model\HttpRouteFactory;
use liberty_code\framework\request_flow\request\http\model\HttpRequest;
use liberty_code\framework\app_mode\mode\factory\api\ModeFactoryInterface;
use liberty_code\framework\app_mode\mode\factory\model\DefaultModeFactory;
use liberty_code\framework\app_mode\build\api\BuilderInterface as AppModeBuilderInterface;
use liberty_code\framework\app_mode\build\model\DefaultBuilder as DefaultAppModeBuilder;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code\framework\module\factory\api\ModuleFactoryInterface;
use liberty_code\framework\module\factory\standard\library\ConstStandardModuleFactory;
use liberty_code\framework\module\factory\standard\model\StandardModuleFactory;
use liberty_code\framework\module\build\api\BuilderInterface as ModuleBuilderInterface;
use liberty_code\framework\module\build\model\DefaultBuilder as DefaultModuleBuilder;
use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\application\api\AppInterface;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate dependency collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateDependencyCollection(AppInterface $objApp)
    {
        // Init var
        $strRootDirPath = $objApp->getStrRootDirPath();
        /** @var ParserBuilder $objConfigParserBuilder */
        $objConfigParserBuilder = $objApp->getObjConfigParserBuilder();
        $objProvider = $objApp->getObjProvider();
        $objDependencyCollection = $objProvider->getObjDependencyCollection();

        // Set default DI factory, on dependency
        $objDiFactory = $objProvider->getFromClassPath(StandardDependencyFactory::class);
        $objPref = new Preference(array(
            'source' => DependencyFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objDiFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default DI builder, on dependency
        $objDiBuilder = $objProvider->getFromClassPath(DefaultDiBuilder::class);
        $objPref = new Preference(array(
            'source' => DiBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objDiBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application provider, as default provider, on dependency
        $objPref = new Preference(array(
            'source' => ProviderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objProvider],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application provider dependency collection, as default dependency collection, on dependency
        $objPref = new Preference(array(
            'source' => DependencyCollectionInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objDependencyCollection],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set parser factory, as default parser factory, on dependency
        $objParserFactory = $objConfigParserBuilder->getObjParserFactory();
        $objPref = new Preference(array(
            'source' => ParserFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objParserFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set file parser factory, as default file parser factory, on dependency
        $objFileParserFactory = $objConfigParserBuilder->getObjFileParserFactory();
        $objPref = new Preference(array(
            'source' => FileParserFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objFileParserFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default application mode factory, on dependency
        /** @var DefaultModeFactory $objAppModeFactory */
        $objAppModeFactory = $objProvider->getFromClassPath(DefaultModeFactory::class);
        $objAppModeFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => ModeFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objAppModeFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default application mode builder, on dependency
        $objAppModeBuilder = $objProvider->getFromClassPath(DefaultAppModeBuilder::class);
        $objPref = new Preference(array(
            'source' => AppModeBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objAppModeBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application mode selector, as default mode selector, on dependency
        $objAppMode = $objApp->getObjAppMode();
        $objPref = new Preference(array(
            'source' => SelectorInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objAppMode],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application module collection, as default module collection, on dependency
        $objModuleCollection = $objApp->getObjModuleCollection();
        $objPref = new Preference(array(
            'source' => ModuleCollectionInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objModuleCollection],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default module factory, on dependency
        /** @var StandardModuleFactory $objModuleFactory */
        $objModuleFactory = $objProvider->getFromClassPath(StandardModuleFactory::class);
        $objModuleFactory->setTabConfig(
            array(
                ConstStandardModuleFactory::TAB_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN => $strRootDirPath . '%1$s'
            )
        );
        $objModuleFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => ModuleFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objModuleFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default module builder, on dependency
        $objModuleBuilder = $objProvider->getFromClassPath(DefaultModuleBuilder::class);
        $objPref = new Preference(array(
            'source' => ModuleBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objModuleBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application configuration object, as default configuration object, on dependency
        $objConfig = $objApp->getObjConfig();
        $objPref = new Preference(array(
            'source' => ConfigInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objConfig],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default call factory, on dependency
        $objCallFactory = $objApp->getObjObserver()->getObjEventCollection()->getObjCallFactory();
        $objPref = new Preference(array(
            'source' => CallFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objCallFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default route builder, on dependency
        $objRouteBuilder = $objProvider->getFromClassPath(DefaultRouteBuilder::class);
        $objPref = new Preference(array(
            'source' => RouteBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objRouteBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application router, as default router, on dependency
        $objRouter = $objApp->getObjFrontController()->getObjRouter();
        $objPref = new Preference(array(
            'source' => RouterInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objRouter],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application router route collection , as default route collection, on dependency
        $objRouteCollection = $objRouter->getObjRouteCollection();
        $objPref = new Preference(array(
            'source' => RouteCollectionInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objRouteCollection],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default event factory, on dependency
        /** @var StandardEventFactory $objEventFactory */
        $objEventFactory = $objProvider->getFromClassPath(StandardEventFactory::class);
        $objEventFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => EventFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objEventFactory],
            'option' => [
                'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default event builder, on dependency
        $objEventBuilder = $objProvider->getFromClassPath(DefaultEventBuilder::class);
        $objPref = new Preference(array(
            'source' => EventBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objEventBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application event observer, as default observer, on dependency
        $objObserver = $objApp->getObjObserver();
        $objPref = new Preference(array(
            'source' => ObserverInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objObserver],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application event observer event collection, as default event collection, on dependency
        $objEventCollection = $objObserver->getObjEventCollection();
        $objPref = new Preference(array(
            'source' => EventCollectionInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objEventCollection],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application front controller, as default front controller, on dependency
        $objFrontController = $objApp->getObjFrontController();
        $objPref = new Preference(array(
            'source' => FrontControllerInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objFrontController],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application front controller request, as default request, on dependency
        $objRequest = $objApp->getObjFrontController()->getObjActiveRequest();
        $objPref = new Preference(array(
            'source' => RequestInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set handler factory, on dependency
        /** @var DefaultWarnTryer $objTryer */
        $objTryer = $objApp->getObjExecTryer();
        /** @var DefaultHandlerFactory $objDefaultHandlerFactory */
        $objDefaultHandlerFactory = $objProvider->getFromClassPath(DefaultHandlerFactory::class);
        $objDefaultHandlerFactory->setObjProvider($objProvider);
        /** @var StandardHandlerFactory $objStandardHandlerFactory */
        $objStandardHandlerFactory = $objProvider->getFromClassPath(StandardHandlerFactory::class);
        $objStandardHandlerFactory->setObjFactory($objDefaultHandlerFactory);
        $objStandardHandlerFactory->setObjProvider($objProvider);
        /** @var WarnHandlerFactory $objHandlerFactory */
        $objHandlerFactory = new WarnHandlerFactory($objTryer->getObjWarnRegister());
        $objHandlerFactory->setObjFactory($objStandardHandlerFactory);
        $objHandlerFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => HandlerFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objHandlerFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default handler builder, on dependency
        $objErrorBuilder = $objProvider->getFromClassPath(DefaultErrorBuilder::class);
        $objPref = new Preference(array(
            'source' => ErrorBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objErrorBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application execution tryer, as default tryer, on dependency
        $objPref = new Preference(array(
            'source' => TryerInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objTryer],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application execution tryer handler collection, as default handler collection, on dependency
        $objHandlerCollection = $objTryer->getObjHandlerCollection();
        $objPref = new Preference(array(
            'source' => HandlerCollectionInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objHandlerCollection],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set warning handler factory, on dependency
        /** @var DefaultWarnHandlerFactory $objWarnHandlerFactory */
        $objWarnHandlerFactory = $objProvider->getFromClassPath(DefaultWarnHandlerFactory::class);
        $objWarnHandlerFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => WarnHandlerFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objWarnHandlerFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default warning handler builder, on dependency
        $objWarnBuilder = $objProvider->getFromClassPath(DefaultWarnBuilder::class);
        $objPref = new Preference(array(
            'source' => WarnBuilderInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objWarnBuilder],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application execution tryer, as default warning tryer, on dependency
        $objPref = new Preference(array(
            'source' => WarnTryerInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objTryer],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application execution tryer warning handler collection, as default warning handler collection, on dependency
        $objWarnHandlerCollection = $objTryer->getObjWarnHandlerCollection();
        $objPref = new Preference(array(
            'source' => WarnHandlerCollectionInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objWarnHandlerCollection],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application, on dependency
        $objPref = new Preference(array(
            'source' => AppInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objApp],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);
    }



    /**
     * Hydrate HTTP dependency collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateHttpDependencyCollection(AppInterface $objApp)
    {
        // Init var
        $objProvider = $objApp->getObjProvider();
        $objDependencyCollection = $objProvider->getObjDependencyCollection();

        // Set default route factory, on dependency
        $objRouteFactory = $objProvider->getFromClassPath(HttpRouteFactory::class);
        $objRouteFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => RouteFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objRouteFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application front controller, as default HTTP front controller, on dependency
        $objFrontController = $objApp->getObjFrontController();
        $objPref = new Preference(array(
            'source' => HttpFrontController::class,
            'set' =>  ['type' => 'instance', 'value' => $objFrontController],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application front controller HTTP request, as default HTTP request, on dependency
        $objRequest = $objApp->getObjFrontController()->getObjActiveRequest();
        $objPref = new Preference(array(
            'source' => HttpRequest::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        $objPref = new Preference(array(
            'source' => BaseHttpRequest::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default command request, on dependency
        $objRequest = new CommandRequest();
        $objPref = new Preference(array(
            'source' => CommandRequest::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Hydrate application object
        static::hydrateDependencyCollection($objApp);
    }



    /**
     * Hydrate specified HTTP application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateHttpApp(AppInterface $objApp)
    {
        static::hydrateHttpDependencyCollection($objApp);
    }



    /**
     * Hydrate command line dependency collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateCommandDependencyCollection(AppInterface $objApp)
    {
        // Init var
        $objProvider = $objApp->getObjProvider();
        $objDependencyCollection = $objProvider->getObjDependencyCollection();

        // Set default route factory, on dependency
        $objRouteFactory = $objProvider->getFromClassPath(CommandRouteFactory::class);
        $objRouteFactory->setObjProvider($objProvider);
        $objPref = new Preference(array(
            'source' => RouteFactoryInterface::class,
            'set' =>  ['type' => 'instance', 'value' => $objRouteFactory],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application front controller, as default command front controller, on dependency
        $objFrontController = $objApp->getObjFrontController();
        $objPref = new Preference(array(
            'source' => CommandFrontController::class,
            'set' =>  ['type' => 'instance', 'value' => $objFrontController],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        $objFrontController = $objApp->getObjFrontController();
        $objPref = new Preference(array(
            'source' => InfoFrontController::class,
            'set' =>  ['type' => 'instance', 'value' => $objFrontController],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set default HTTP request, on dependency
        $objRequest = new HttpRequest();
        $objPref = new Preference(array(
            'source' => HttpRequest::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        $objPref = new Preference(array(
            'source' => BaseHttpRequest::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Set application front controller command request, as default command request, on dependency
        $objRequest = $objApp->getObjFrontController()->getObjActiveRequest();
        $objPref = new Preference(array(
            'source' => CommandRequest::class,
            'set' =>  ['type' => 'instance', 'value' => $objRequest],
            'option' => [
                //'shared' => true
            ]
        ));
        $objDependencyCollection->setDependency($objPref);

        // Hydrate application object
        static::hydrateDependencyCollection($objApp);
    }



    /**
     * Hydrate specified command line application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateCommandApp(AppInterface $objApp)
    {
        static::hydrateCommandDependencyCollection($objApp);
    }
	
	
	
}