<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\application\library;



class ConstApp
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
	const EXCEPT_MSG_APPLICATION_NOT_FOUND = 'No application found!';
}