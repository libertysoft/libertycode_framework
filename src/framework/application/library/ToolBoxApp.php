<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\application\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\application\model\DefaultApp;
use liberty_code\framework\framework\application\exception\ApplicationNotFoundException;



class ToolBoxApp extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     * Get application object.
	 * 
     * @return AppInterface
	 * @throws ApplicationNotFoundException
     */
    public static function getObjApp()
    {
		// Check if application already instantiated
		if(DefaultApp::instanceCheckNew())
		{
			throw new ApplicationNotFoundException();
		}
		
		// Return result
		return DefaultApp::instanceGetDefault();
    }
	
	
	
}