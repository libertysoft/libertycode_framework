<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\route\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\route\build\api\BuilderInterface;
use liberty_code\route\build\model\DefaultBuilder;
use liberty_code\command\request_flow\response\info\library\ToolBoxInfoResponse;
use liberty_code\command\request_flow\front\info\model\InfoFrontController;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\cache\repository\library\ToolBoxRepository;
use liberty_code\framework\framework\route\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate HTTP route collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateHttpRouteCollection(AppInterface $objApp)
    {
        // Init var
        $objProvider = $objApp->getObjProvider();
        $objRouteCollection = $objApp->getObjFrontController()->getObjRouter()->getObjRouteCollection();
        $tabDataSrc = ToolBoxRepository::getSetItem(
            ConstBuilder::CACHE_KEY_CONFIG_ROUTER_WEB,
            function() use ($objApp) {
                return (
                    (!is_null($tabConfig = $objApp
                        ->getObjModuleCollection()
                        ->getTabMergeConfigRouteWeb())) ?
                        $tabConfig :
                        array()
                );
            }
        );

        // Hydrate route collection object, if required
        $objBuilder = $objProvider->getFromClassPath(BuilderInterface::class);
        if(
            is_array($tabDataSrc) &&
            ($objBuilder instanceof DefaultBuilder)
        )
        {
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateRouteCollection($objRouteCollection);
        }
    }



    /**
     * Hydrate specified HTTP application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateHttpApp(AppInterface $objApp)
    {
        static::hydrateHttpRouteCollection($objApp);
    }



    /**
     * Hydrate command line route collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateCommandRouteCollection(AppInterface $objApp)
    {
        // Init var
        $objProvider = $objApp->getObjProvider();
        $objRouteCollection = $objApp->getObjFrontController()->getObjRouter()->getObjRouteCollection();
        $tabDataSrc = ToolBoxRepository::getSetItem(
            ConstBuilder::CACHE_KEY_CONFIG_ROUTER_COMMAND,
            function() use ($objApp) {
                return (
                    (!is_null($tabConfig = $objApp
                        ->getObjModuleCollection()
                        ->getTabMergeConfigRouteCmd())) ?
                        $tabConfig :
                        array()
                );
            }
        );

        // Hydrate route collection object, if required
        $objBuilder = $objProvider->getFromClassPath(BuilderInterface::class);
        if(
            is_array($tabDataSrc) &&
            ($objBuilder instanceof DefaultBuilder)
        )
        {
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateRouteCollection($objRouteCollection);
        }
    }



    /**
     * Hydrate command information responses,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateInfoResponse(AppInterface $objApp)
    {
        // Init var
        $objFrontController = $objApp->getObjFrontController();

        // Hydrate front controller info response, if required
        if($objFrontController instanceof InfoFrontController)
        {
            $objInfoCommandResponse = ToolBoxInfoResponse::getObjCommandResponse($objFrontController);
            $objInfoSummaryResponse = ToolBoxInfoResponse::getObjSummaryResponse($objFrontController);
            $objFrontController->setObjInfoCommandResponse($objInfoCommandResponse);
            $objFrontController->setObjInfoSummaryResponse($objInfoSummaryResponse);
        }
    }



    /**
     * Hydrate specified command line application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateCommandApp(AppInterface $objApp)
    {
        static::hydrateCommandRouteCollection($objApp);
        static::hydrateInfoResponse($objApp);
    }



}