<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\route\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Cache configuration
    const CACHE_KEY_CONFIG_ROUTER_WEB = 'config.router_web';
    const CACHE_KEY_CONFIG_ROUTER_COMMAND = 'config.router_command';
}