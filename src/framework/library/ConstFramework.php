<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\library;



class ConstFramework
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Path configuration
    const CONF_PATH_DIR_CONFIG = '/config';

    const CONF_PATH_DIR_VAR = '/var';
    const CONF_PATH_DIR_VAR_FRAMEWORK = self::CONF_PATH_DIR_VAR . '/framework';
    const CONF_PATH_DIR_VAR_FRAMEWORK_CACHE = self::CONF_PATH_DIR_VAR_FRAMEWORK . '/cache';
}