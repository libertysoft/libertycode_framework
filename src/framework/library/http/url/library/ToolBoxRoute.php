<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\library\http\url\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\request_flow\request\http\model\HttpRequest;
use liberty_code\framework\framework\application\library\ToolBoxApp;



class ToolBoxRoute extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Get formatted route full (with route root) or not.
     *
	 * @param string $strRoute
     * @param boolean $boolFull = true
     * @return null|string
     */
    protected static function getStrFormatRouteFull($strRoute, $boolFull = true)
    {
        // Init var
        $result = null;
        $strRouteRoot = static::getStrRouteRoot();

		if(is_string($strRoute) && (!is_null($strRouteRoot)))
		{
			$result = $strRoute;
			
			// Complete URL if required
			if($boolFull)
			{
				$result = $strRouteRoot . $strRoute;
			}
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get formatted route with query arguments (GET) or not.
     *
	 * @param string $strRoute
     * @param array $tabArg = array()
     * @return null|string
     */
    protected static function getStrFormatRouteArg($strRoute, array $tabArg = array())
    {
        // Init var
        $result = null;

		if(is_string($strRoute))
		{
			$result = $strRoute;
			$strArg = http_build_query($tabArg);
			
			// Complete URL if required
			if(trim($strArg) != '')
			{
				$result = $strRoute . '?' . $strArg;
			}
		}
		
		// Return result
		return $result;
    }
	
	
	
	/**
	 * Get string route root.
	 * 
	 * @return null|string
	 */
    public static function getStrRouteRoot()
    {
        // Init var
        $result = null;
        $objRequest = ToolBoxApp::getObjApp()->getObjFrontController()->getObjActiveRequest();

        // Get route, if required
        if($objRequest instanceof HttpRequest)
        {
            $result = $objRequest->getStrRouteRoot();
        }
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get string active route (from request).
     *
	 * @param array $tabArg = array()
     * @param boolean $boolFull = true
     * @return null|string
     */
    public static function getStrRouteActive(array $tabArg = array(), $boolFull = true)
    {
        // Init var
        $result = null;
		$objRequest = ToolBoxApp::getObjApp()->getObjFrontController()->getObjActiveRequest();

        // Get route, if required
		if($objRequest instanceof HttpRequest)
        {
            $result = $objRequest->getStrRoute();

            // Format URL if required
            $result = static::getStrFormatRouteFull($result, $boolFull);
            $result = static::getStrFormatRouteArg($result, $tabArg);
        }
		
		// Return result
		return $result;
    }
	
	
	
	/**
     * Get string route.
     *
	 * @param string $strKey
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
	 * @param array $tabArg = array()
     * @param boolean $boolFull = true
     * @return null|string
     */
    public static function getStrRoute(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array(),
        array $tabArg = array(),
        $boolFull = true
    )
    {
		// Init var
		$result = null;
		$objRouter = ToolBoxApp::getObjApp()->getObjFrontController()->getObjRouter();

        // Get route, if required
        $result = $objRouter->getStrRouteHandleSource($strKey, $tabCallArg, $tabStrCallElm);

        // Format URL if required
        $result = static::getStrFormatRouteFull($result, $boolFull);
        $result = static::getStrFormatRouteArg($result, $tabArg);
        
		// Return result
		return $result;
    }



    /**
     * Get route full (with route root) or not.
     *
     * @param string $strRoute
     * @param boolean $boolFull = true
     * @return null|string
     */
    public static function getStrRouteFull($strRoute, $boolFull = true)
    {
        // Return result
        return static::getStrFormatRouteFull($strRoute, $boolFull);
    }



}