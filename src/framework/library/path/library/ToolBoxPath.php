<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\library\path\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\framework\application\library\ToolBoxApp;



class ToolBoxPath extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Get application root directory full path.
     *
     * @return string
     */
    public static function getStrAppRootDirPath()
    {
        // Init var
        $objApp = ToolBoxApp::getObjApp();

		// Return result
		return $objApp->getStrRootDirPath();
    }



    /**
     * Get formatted full path,
     * with application root directory full path, or not.
     *
     * @param string $strPath
     * @param boolean $boolFull = true
     * @return null|string
     */
    public static function getStrPathFull($strPath, $boolFull = true)
    {
        // Init var
        $result = null;

        if(is_string($strPath))
        {
            $result = $strPath;

            // Complete path, if required
            if($boolFull)
            {
                $result = static::getStrAppRootDirPath() . $strPath;
            }
        }

        // Return result
        return $result;
    }



}