<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\cache\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\cache\repository\library\ToolBoxRepository as BaseToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\framework\app_mode\config\library\ConstConfig as ConstAppModeConfig;
use liberty_code\framework\framework\application\library\ToolBoxApp;



class ToolBoxRepository extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified item,
     * from application cache repository, if required.
     * Get from specified item,
     * and set on application cache repository after, if required, else.
     *
     * Configuration array format:
     * @see RepositoryInterface::setItem() configuration array format.
     *
     * @param string $strKey
     * @param mixed|callable $item
     * @param array $tabConfig = null
     * @return mixed
     */
    public static function getSetItem(
        $strKey,
        $item,
        array $tabConfig = null
    )
    {
        // Init var
        $objApp = ToolBoxApp::getObjApp();
        $result = (
            $objApp->getObjAppMode()->getActiveModeValue(ConstAppModeConfig::TAB_CONFIG_KEY_CACHE) ?
                BaseToolBoxRepository::getSetItem(
                    $objApp->getObjCacheRepo(),
                    $strKey,
                    $item,
                    $tabConfig
                ) :
                (is_callable($item) ? $item() : $item)
        );

        // Return result
        return $result;
    }



}