<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\di\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\di\build\api\BuilderInterface;
use liberty_code\di\build\model\DefaultBuilder;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\cache\repository\library\ToolBoxRepository;
use liberty_code\framework\framework\di\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate dependency collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateDependencyCollection(AppInterface $objApp)
    {
        // Init var
        $objDependencyCollection = $objApp->getObjProvider()->getObjDependencyCollection();
        $tabDataSrc = ToolBoxRepository::getSetItem(
            ConstBuilder::CACHE_KEY_CONFIG_PROVIDER,
            function() use ($objApp) {
                return (
                    (!is_null($tabConfig = $objApp
                        ->getObjModuleCollection()
                        ->getTabMergeConfigDi())) ?
                        $tabConfig :
                        array()
                );
            }
        );

        // Hydrate configuration object, if required
        $objBuilder = $objApp->getObjProvider()->getFromClassPath(BuilderInterface::class);
        if(
            is_array($tabDataSrc) &&
            ($objBuilder instanceof DefaultBuilder)
        )
        {
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateDependencyCollection($objDependencyCollection, false);
        }
    }



    /**
     * Hydrate specified application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateApp(AppInterface $objApp)
    {
        static::hydrateDependencyCollection($objApp);
    }



}