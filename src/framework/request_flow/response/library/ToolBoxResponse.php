<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\request_flow\response\http\library;

use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\framework\framework\application\library\ToolBoxApp;



class ToolBoxResponse
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     *  Get forwarded response object,
     * from specified route.
     *
	 * @param string $strKey
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return null|ResponseInterface
     */
    public static function getObjForwardResponse(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
		// Init var
		$result = null;

        $objFrontController = ToolBoxApp::getObjApp()->getObjFrontController();
		if(!is_null($objFrontController))
		{
			$result = $objFrontController->executeRoute($strKey, $tabCallArg, $tabStrCallElm);
		}
		
		// Return result
		return $result;
    }
	
	
	
}