<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/framework/Include.php');

// Use
use liberty_code\framework\framework\request_flow\response\http\library\ToolBoxHttpResponse;



// Boot application
$strAppRootDirPath = $strRootAppPath . '/src/framework/test/application_test';
$tabAppConfig = array(
    'root_dir_path' => $strAppRootDirPath
);
$tabAppConfigParserType = array(
    'type' => 'string_table_json_yml'
);
$strActiveAppModeKey = 'mode_2';
require_once($strRootAppPath . '/include/framework/BootHttpApp.php');



// Init var
$objResponse = ToolBoxHttpResponse::getObjRedirectRouteResponse(
	'route_5',
	array(
		'Serge',
		'BOUZID'
	),
    array(),
	array(
		'test_1' => 'Value 1', 
		'test_2' => 'Value 2'
	)
);



// Test send
$objResponse->send();


