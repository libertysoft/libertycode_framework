<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\request_flow\response\http\library;

use liberty_code\http\request_flow\response\redirection\model\RedirectResponse;
use liberty_code\framework\framework\library\http\url\library\ToolBoxRoute;



class ToolBoxHttpResponse
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     * Get redirection response object from specified route.
     *
	 * @param string $strKey
	 * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
	 * @param array $tabArg = array()
	 * @param RedirectResponse $objResponse = null
     * @return RedirectResponse
     */
    public static function getObjRedirectRouteResponse(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array(),
        array $tabArg = array(),
        RedirectResponse $objResponse = null
    )
    {
		// Init var
		$result = $objResponse;
		if(is_null($result))
		{
			$result = new RedirectResponse();
		}

		// Hydrate response
		$strUrl = ToolBoxRoute::getStrRoute($strKey, $tabCallArg, $tabStrCallElm);
		$result->setStrUrl($strUrl);
		$result->setTabGetArg($tabArg);
		
		// Return result
		return $result;
    }
	
	
	
}