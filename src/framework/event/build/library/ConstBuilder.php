<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\event\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Cache configuration
    const CACHE_KEY_CONFIG_OBSERVER = 'config.observer';
}