<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\event\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\event\build\api\BuilderInterface;
use liberty_code\event\build\model\DefaultBuilder;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\cache\repository\library\ToolBoxRepository;
use liberty_code\framework\framework\event\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate event collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateEventCollection(AppInterface $objApp)
    {
        // Init var
        $objProvider = $objApp->getObjProvider();
        $objEventCollection = $objApp->getObjObserver()->getObjEventCollection();
        $tabDataSrc = ToolBoxRepository::getSetItem(
            ConstBuilder::CACHE_KEY_CONFIG_OBSERVER,
            function() use ($objApp) {
                return (
                    (!is_null($tabConfig = $objApp
                        ->getObjModuleCollection()
                        ->getTabMergeConfigEvent())) ?
                        $tabConfig :
                        array()
                );
            }
        );

        // Hydrate event object, if required
        $objBuilder = $objProvider->getFromClassPath(BuilderInterface::class);
        if(
            is_array($tabDataSrc) &&
            ($objBuilder instanceof DefaultBuilder)
        )
        {
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateEventCollection($objEventCollection);
        }
    }



    /**
     * Hydrate specified application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateApp(AppInterface $objApp)
    {
        static::hydrateEventCollection($objApp);
    }



}