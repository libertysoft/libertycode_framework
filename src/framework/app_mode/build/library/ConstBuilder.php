<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\app_mode\build\library;

use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\framework\library\ConstFramework;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Configuration
	const DEFAULT_ACTIVE_KEY = 'prod';
	
	const DEFAULT_DATA_SRC = array(
		'dev' => [
			ConstConfig::TAB_CONFIG_KEY_CACHE => false,
			ConstConfig::TAB_CONFIG_KEY_DEBUG => true,
			ConstConfig::TAB_CONFIG_KEY_LOG_INFO => false,
			ConstConfig::TAB_CONFIG_KEY_LOG_WARNING => false,
			ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => false,
			ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => false
		],
		'prod' => [
			ConstConfig::TAB_CONFIG_KEY_CACHE => true,
			ConstConfig::TAB_CONFIG_KEY_DEBUG => false,
			ConstConfig::TAB_CONFIG_KEY_LOG_INFO => true,
			ConstConfig::TAB_CONFIG_KEY_LOG_WARNING => true,
			ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => true,
			ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => true
		]
	);

    // Path configuration
    const CONF_PATH_FILE_CONFIG_APP_MODE = ConstFramework::CONF_PATH_DIR_CONFIG . '/AppMode.';
}