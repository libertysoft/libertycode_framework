<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\app_mode\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\app_mode\build\api\BuilderInterface;
use liberty_code\framework\app_mode\build\model\DefaultBuilder;
use liberty_code\framework\module\library\ToolBoxModule;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\app_mode\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate mode collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateModeCollection(AppInterface $objApp)
    {
        // Init var
        $objModeCollection = $objApp->getObjAppMode()->getObjModeCollection();
        $tabDataSrc = static::getTabDataSrc($objApp);

        // Hydrate mode collection, if required
        $objBuilder = $objApp->getObjProvider()->getFromClassPath(BuilderInterface::class);
        if(
            is_array($tabDataSrc) &&
            ($objBuilder instanceof DefaultBuilder)
        )
        {
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateModeCollection($objModeCollection);
        }
    }



    /**
     * Hydrate mode selector,
     * from specified application object.
     *
     * @param AppInterface $objApp
     * @param string $strActiveKey = null
     */
    protected static function hydrateSelector(AppInterface $objApp, $strActiveKey = null)
    {
        // Init key
        $objSelector = $objApp->getObjAppMode();
        $objModeCollection = $objSelector->getObjModeCollection();
        $tabKey = $objModeCollection->getTabKey();
        $strActiveKey = (
            is_string($strActiveKey) ?
                $strActiveKey :
                ConstBuilder::DEFAULT_ACTIVE_KEY
        );

        // Set active key, if found
        if($objModeCollection->checkExists($strActiveKey))
        {
            $objSelector->setActiveKey($strActiveKey);
        }
        // Else: set first key as active key, if found
        else if(count($tabKey) >= 1)
        {
            $objSelector->setActiveKey($tabKey[0]);
        }
    }



    /**
     * Hydrate specified application object.
     *
     * @param AppInterface $objApp
     * @param string $strActiveKey = null
     */
    public static function hydrateApp(AppInterface $objApp, $strActiveKey = null)
    {
        static::hydrateModeCollection($objApp);
        static::hydrateSelector($objApp, $strActiveKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get data source array,
     * from specified application object.
     *
     * @param AppInterface $objApp
     * @return null|array
     */
    protected static function getTabDataSrc(AppInterface $objApp)
    {
        // Init var
        $result = ConstBuilder::DEFAULT_DATA_SRC;
        $strRootDirPath = $objApp->getStrRootDirPath();
        $objConfigParserBuilder = $objApp->getObjConfigParserBuilder();
        $objFileParser = $objConfigParserBuilder->getObjFileParser();
        $strFilePath = ToolBoxModule::getStrConfigFilePath(
            $strRootDirPath,
            ConstBuilder::CONF_PATH_FILE_CONFIG_APP_MODE,
            $objFileParser
        );

        // Check file path valid
        if(!is_null($strFilePath))
        {
            // Register data source, if required
            $tabDataSrc = $objFileParser->getData($strFilePath);
            if(is_array($tabDataSrc))
            {
                $result = $tabDataSrc;
            }
        }

        // Return result
        return $result;
    }



}