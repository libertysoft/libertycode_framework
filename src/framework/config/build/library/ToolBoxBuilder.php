<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\config\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\config\config\data\model\DataConfig;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\cache\repository\library\ToolBoxRepository;
use liberty_code\framework\framework\config\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate configuration,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateConfig(AppInterface $objApp)
    {
        // Init var
        $objConfig = $objApp->getObjConfig();
        $tabDataSrc = ToolBoxRepository::getSetItem(
            ConstBuilder::CACHE_KEY_CONFIG_CONFIG,
            function() use ($objApp) {
                return (
                    (!is_null($tabConfig = $objApp
                        ->getObjModuleCollection()
                        ->getTabMergeConfigParamApp())) ?
                        $tabConfig :
                        array()
                );
            }
        );

        // Hydrate configuration object, if required
        if(is_array($tabDataSrc))
        {
            /** @var DataConfig $objConfig */
            $objConfig->getObjData()->setDataSrc($tabDataSrc);
        }
    }



    /**
     * Hydrate specified application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateApp(AppInterface $objApp)
    {
        static::hydrateConfig($objApp);
    }



}