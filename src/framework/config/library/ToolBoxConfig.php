<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\config\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\config\library\ConstConfig;



class ToolBoxConfig extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration path key,
     * from specified index array of keys.
     *
     * @param array $tabKey
     * @return null|mixed
     */
    public static function getStrPathKey(array $tabKey)
    {
        // Init var
        $result = '';

        // Run each key
        foreach($tabKey as $strKey)
        {
            // Build path with key, if required
            $result = (
            (is_string($strKey) && (trim($strKey) != '')) ?
                (
                (is_string($result) && (trim($result) != '')) ?
                    sprintf(
                        '%1$s%2$s%3$s',
                        $result,
                        ConstConfig::CONFIG_PATH_SEPARATOR,
                        $strKey
                    ) :
                    $strKey
                ) :
                $result
            );
        }

        // Return result
        return $result;
    }



	/**
	 * Get configuration path key,
     * from specified list of keys.
	 * 
	 * @param string ...$key
	 * @return null|mixed
	 */
	public static function getStrPathKeyFromList(...$key)
	{
		// Return result
		return static::getStrPathKey($key);
	}
	
	
	
}