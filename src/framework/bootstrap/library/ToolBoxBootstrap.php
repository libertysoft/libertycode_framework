<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\bootstrap\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\data\data\table\model\TableData;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\config\config\register\model\RegisterConfig;
use liberty_code\config\config\cache\model\CacheConfig;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\module\api\ModuleInterface;



class ToolBoxBootstrap extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

    /**
     * Put specified configuration,
     * to specified configuration object.
     *
     * @param ConfigInterface $objConfig
     * @param string $strKey
     * @param string $value
     * @return boolean
     */
    protected static function putConfigValue(
        ConfigInterface $objConfig,
        $strKey,
        $value
    )
    {
        // Init var
        $result = false;

        // Put value on data configuration, if required
        if($objConfig instanceof DataConfig)
        {
            $objData = $objConfig->getObjData();
            $result = $objData->putValue($strKey, $value);
        }
        // Put value on register configuration, if required
        else if($objConfig instanceof RegisterConfig)
        {
            $objRegister = $objConfig->getObjRegister();
            $result = $objRegister->putItem($strKey, $value);
        }
        else if($objConfig instanceof CacheConfig)
        {
            $objCacheRepo = $objConfig->getObjRepository();
            $result = $objCacheRepo->setItem($strKey, $value);
        }

        // Return result
        return $result;
    }



    /**
     * Put specified application configuration.
     *
     * @param AppInterface $objApp
     * @param string $strKey
     * @param string $value
     * @return boolean
     */
    public static function putAppConfigConfigValue(
        AppInterface $objApp,
        $strKey,
        $value
    )
    {
        // Init var
        $objConfig = $objApp->getObjConfig();
        $result = (
            (!is_null($objConfig)) &&
            static::putConfigValue(
                $objConfig,
                $strKey,
                $value
            )
        );

        // Return result
        return $result;
    }



    /**
     * Put specified module configuration.
     *
     * @param ModuleInterface $objModule
     * @param string $strKey
     * @param string $value
     * @return boolean
     */
    public static function putModuleConfigValue(
        ModuleInterface $objModule,
        $strKey,
        $value
    )
    {
        // Init var
        $objConfig = $objModule->getObjConfig();
        $result = static::putConfigValue(
            $objConfig,
            $strKey,
            $value
        );

        // Return result
        return $result;
    }



    /**
     * Put configuration,
     * from specified configuration full file path,
     * to specified configuration object.
     *
     * @param AppInterface $objApp
     * @param ModuleInterface $objModule
     * @param ConfigInterface $objConfig
     * @param string $strFilePath
     * @param string $strKey = null
     * @return boolean
     */
    protected static function putConfigFile(
        AppInterface $objApp,
        ModuleInterface $objModule,
        ConfigInterface $objConfig,
        $strFilePath,
        $strKey = null
    )
    {
        // Init var
        $result = false;
        $objParserBuilder = $objModule->getObjConfigParserBuilder();
        $objParserBuilder = (
        is_null($objParserBuilder) ?
            $objApp->getObjConfigParserBuilder() :
            $objParserBuilder
        );

        // Check parser found
        if(!is_null($objParserBuilder))
        {
            // Get parsed data
            $objFileParser = $objParserBuilder->getObjFileParser();
            $tabData = $objFileParser->getData($strFilePath);

            // Check parsed data found
            if(is_array($tabData))
            {
                // Format parsed data
                $tabData = (
                    (is_string($strKey) && (trim($strKey) != '')) ?
                        array($strKey => $tabData) :
                        $tabData
                );

                // Put parsed data on data configuration, if required
                if($objConfig instanceof DataConfig)
                {
                    $result = true;
                    $objData = $objConfig->getObjData();

                    if($objData instanceof TableData)
                    {
                        $tabData = ToolBoxTable::getTabMerge($objData->getDataSrc(), $tabData);
                        $objData->setDataSrc($tabData);
                    }
                    else
                    {
                        foreach($tabData as $key => $value)
                        {
                            $result = $objData->putValue($key, $value) && $result;
                        }
                    }
                }
                // Put parsed data on register configuration, if required
                else if($objConfig instanceof RegisterConfig)
                {
                    $objRegister = $objConfig->getObjRegister();
                    $result = $objRegister->hydrateItem($tabData, true, false);
                }
            }
        }

        // Return result
        return $result;
    }



	/**
     * Put application configuration,
     * from specified configuration full file path.
     *
     * @param AppInterface $objApp
     * @param ModuleInterface $objModule
     * @param string $strFilePath
     * @param string $strKey = null
     * @return boolean
     */
    public static function putAppConfigFile(
        AppInterface $objApp,
        ModuleInterface $objModule,
        $strFilePath,
        $strKey = null
    )
    {
        // Init var
        $objConfig = $objApp->getObjConfig();
        $result = (
            (!is_null($objConfig)) &&
            static::putConfigFile(
                $objApp,
                $objModule,
                $objConfig,
                $strFilePath,
                $strKey
            )
        );

        // Return result
        return $result;
    }



    /**
     * Put module configuration,
     * from specified configuration full file path.
     *
     * @param AppInterface $objApp
     * @param ModuleInterface $objModule
     * @param string $strFilePath
     * @param string $strKey = null
     * @return boolean
     */
    public static function putModuleConfigFile(
        AppInterface $objApp,
        ModuleInterface $objModule,
        $strFilePath,
        $strKey = null
    )
    {
        // Init var
        $result = static::putConfigFile(
            $objApp,
            $objModule,
            $objModule->getObjConfig(),
            $strFilePath,
            $strKey
        );

        // Return result
        return $result;
    }



}