<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\module\build\library;

use liberty_code\framework\framework\library\ConstFramework;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Path configuration
    const CONF_PATH_FILE_CONFIG_MODULE = ConstFramework::CONF_PATH_DIR_CONFIG . '/Module.';

    // Module configuration
    const TAB_CONFIG_MODULE_KEY_LIST = 'list';
}