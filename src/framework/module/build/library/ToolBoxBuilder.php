<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\module\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\module\library\ToolBoxModule;
use liberty_code\framework\module\build\api\BuilderInterface;
use liberty_code\framework\module\build\model\DefaultBuilder;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\module\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate module collection,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateModuleCollection(AppInterface $objApp)
    {
        // Init var
        $objModuleCollection = $objApp->getObjModuleCollection();
        $tabDataSrc = static::getTabDataSrc($objApp);

        // Hydrate module collection, if required
        $objBuilder = $objApp->getObjProvider()->getFromClassPath(BuilderInterface::class);
        if(
            is_array($tabDataSrc) &&
            ($objBuilder instanceof DefaultBuilder)
        )
        {
            $objBuilder->setTabDataSrc($tabDataSrc);
            $objBuilder->hydrateModuleCollection($objModuleCollection);
        }
    }



    /**
     * Hydrate specified application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateApp(AppInterface $objApp)
    {
        static::hydrateModuleCollection($objApp);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get data source array,
     * from specified application object.
     *
     * @param AppInterface $objApp
     * @return null|array
     */
    protected static function getTabDataSrc(AppInterface $objApp)
    {
        // Init var
        $result = null;
        $strRootDirPath = $objApp->getStrRootDirPath();
        $objConfigParserBuilder = $objApp->getObjConfigParserBuilder();
        $objFileParser = $objConfigParserBuilder->getObjFileParser();
        $strFilePath = ToolBoxModule::getStrConfigFilePath(
            $strRootDirPath,
            ConstBuilder::CONF_PATH_FILE_CONFIG_MODULE,
            $objFileParser
        );

        // Check file path valid
        if(!is_null($strFilePath))
        {
            // Register data source, if required
            $tabDataSrc = $objFileParser->getData($strFilePath);
            if(
                is_array($tabDataSrc) &&
                array_key_exists(ConstBuilder::TAB_CONFIG_MODULE_KEY_LIST, $tabDataSrc) &&
                is_array($tabDataSrc[ConstBuilder::TAB_CONFIG_MODULE_KEY_LIST])
            )
            {
                $result = $tabDataSrc[ConstBuilder::TAB_CONFIG_MODULE_KEY_LIST];
            }
        }

        // Return result
        return $result;
    }



}