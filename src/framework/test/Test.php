<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/framework/Include.php');

// Use
use liberty_code\framework\framework\application\library\ToolBoxApp;



// Boot application
$strAppRootDirPath = $strRootAppPath . '/src/framework/test/application_test';
$tabAppConfig = array(
    'root_dir_path' => $strAppRootDirPath
);
$tabAppConfigParserType = array(
    'type' => 'string_table_json_yml'
);
$strActiveAppModeKey = 'mode_2';
require_once($strRootAppPath . '/include/framework/BootHttpApp.php');



// Init var
$objApp = ToolBoxApp::getObjApp();



// Test selector
echo('Selector active key: <pre>');var_dump($objApp->getObjAppMode()->getStrActiveKey());echo('</pre>');
echo('Selector active mode configuration: <pre>');var_dump($objApp->getObjAppMode()->getObjActiveMode()->getObjConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test application parameters
echo('Test application parameters<br /><br />');

$tabKey = array(
    't1', // Found
    'test', // Not found
    't2', // Found
    't3', // Found
    't4', // Found
    't5', // Found
    't6', // Not found
    'param_app_add' // Found
);
foreach($tabKey as $strKey)
{
    echo('Key: <pre>');var_dump($strKey);echo('</pre>');
    echo('Application config: <pre>');var_dump($objApp->getObjConfig()->getValue($strKey));echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test module
echo('Test module<br /><br />');

$objModuleCollection = $objApp->getObjModuleCollection();
foreach($objModuleCollection->getTabKey() as $strKey)
{

    echo('Key: <pre>');var_dump($strKey);echo('</pre>');
    echo('Module config: <pre>');var_dump($objModuleCollection->getObjModule($strKey)->getObjConfig()->getTabValue());echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test dependency injection
echo('Test dependency injection<br /><br />');

$objDependencyCollection = $objApp->getObjProvider()->getObjDependencyCollection();
foreach($objDependencyCollection->getTabKey() as $strKey)
{
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('Dependency config: <pre>');var_dump($objDependencyCollection->getObjDependency($strKey)->getTabConfig());echo('</pre>');
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test register
echo('Test register<br /><br />');

$objRegister = $objApp->getObjRegister();
foreach($objRegister->getTabKey() as $strKey)
{
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('Item: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test event
echo('Test event<br /><br />');

$objObserver = $objApp->getObjObserver();
foreach($objObserver->getObjEventCollection()->getTabKey() as $strKey)
{
    echo('Event: key: <pre>');var_dump($strKey);echo('</pre>');
    echo('Event: check: <pre>');print_r($objObserver->checkExists($strKey));echo('</pre>');

    try{
        $callResult = $objObserver->dispatch($strKey, array('Value 1 - Event test', 'Value 2 - Event test'));

        echo('Event: callable result: <pre>');var_dump($callResult);echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ' - ' . $e->getMessage()));
        echo('<br />');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test route
echo('Test route<br /><br />');

$objRouteCollection = $objApp->getObjFrontController()->getObjRouter()->getObjRouteCollection();
foreach($objRouteCollection as $strKey => $objRoute)
{
	echo('Key: <pre>');var_dump($strKey);echo('</pre>');
	echo('Route config: <pre>');var_dump($objRouteCollection->getObjRoute($strKey)->getTabConfig());echo('</pre>');
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test run
echo('Test run<br /><br />');

try{
    $objApp->run();
    echo('Run done<br />');
} catch(\Exception $e) {
    echo(htmlentities(get_class($e) . ' - ' . $e->getMessage()));
    echo('<br />');
}

echo('<br /><br /><br />');


