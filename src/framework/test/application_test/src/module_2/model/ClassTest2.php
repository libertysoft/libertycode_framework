<?php

namespace liberty_code\framework\framework\test\application_test\src\module_2\model;

use liberty_code\framework\framework\test\application_test\src\module_1\model\ClassTest1;



class ClassTest2
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param ClassTest1 $objArg1
     * @param string $objDtArg2
     * @param bool $boolArg3
     * @param array $tabArg4
     */
    public function __construct(ClassTest1 $objArg1, $objDtArg2 = '', $boolArg3 = true, array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $objArg1,
            'arg_2' => $objDtArg2,
            'arg_3' => $boolArg3,
            'arg_4' => $tabArg4
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }



    public function action($strAdd = '', $strAdd2 = '')
    {
        echo('<br />Action class test 2<br />');

        // Return result
        return 'Result action class test 2' .
            (is_string($strAdd) && (trim($strAdd) != '') ? ': ' . $strAdd : '').
            (is_string($strAdd2) && (trim($strAdd2) != '') ? ': ' . $strAdd2 : '');
    }



}