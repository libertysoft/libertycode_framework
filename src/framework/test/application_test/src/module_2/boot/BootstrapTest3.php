<?php

namespace liberty_code\framework\framework\test\application_test\src\module_2\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\bootstrap\library\ToolBoxBootstrap;
use liberty_code\framework\framework\config\library\ToolBoxConfig;



class BootstrapTest3 extends DefaultBootstrap
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(AppInterface $objApp)
    {
        // Call parent constructor
        parent::__construct($objApp, 'module_2');
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @param string $strAdd
     * @param string $strAdd2
     */
    public function boot1($strAdd = 'Default value 1', $strAdd2 = 'Default value 2')
    {
        $strText =
            'Boot 1 module "' . $this->getObjModule()->getStrKey() . '": ' .
            get_class($this) . ': ' .
            ((trim($strAdd) != '') ? ': Arg 1 "' . $strAdd . '"' : '') .
            ((trim($strAdd2) != '') ? ': Arg 2 "' . $strAdd2 . '"' : '');

        echo(sprintf('<br />%1$s<br />', $strText));

        ToolBoxBootstrap::putAppConfigFile(
            $this->getObjApp(),
            $this->getObjModule(),
            __DIR__ . '/../config/ParamAppAddTest.json'
        );

        ToolBoxBootstrap::putAppConfigFile(
            $this->getObjApp(),
            $this->getObjModule(),
            __DIR__ . '/../config/ParamAppAddTest.json',
            'param_app_add'
        );

        ToolBoxBootstrap::putAppConfigConfigValue(
            $this->getObjApp(),
            ToolBoxConfig::getStrPathKeyFromList('param_app_add', 't6'),
            'val 6'
        );
    }



    /**
     * @param ProviderInterface $objProvider
     */
    public function boot2(ProviderInterface $objProvider)
    {
        $strText =
            'Boot 2 module "' . $this->getObjModule()->getStrKey() . '": ' .
            get_class($this) . ': ' .
            'Provider count dependencies: "' . strval(count($objProvider->getObjDependencyCollection()->getTabKey())) . '"';

        echo(sprintf('<br />%1$s<br />', $strText));

        ToolBoxBootstrap::putModuleConfigFile(
            $this->getObjApp(),
            $this->getObjModule(),
            __DIR__ . '/../config/ParamAddTest.json'
        );

        ToolBoxBootstrap::putModuleConfigFile(
            $this->getObjApp(),
            $this->getObjModule(),
            __DIR__ . '/../config/ParamAddTest.json',
            'param_add'
        );

        ToolBoxBootstrap::putModuleConfigValue(
            $this->getObjModule(),
            ToolBoxConfig::getStrPathKeyFromList('param_add', 'module_2_t5'),
            'val 5'
        );
    }



}