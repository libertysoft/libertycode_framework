<?php

namespace liberty_code\framework\framework\test\application_test\src\module_1\model;



class ClassTest1
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************
    
    /**
     * Constructor
     *
     * @param $intArg1
     * @param $strArg2
     * @param $boolArg3
     * @param array $tabArg4
     */
    public function __construct($intArg1, $strArg2, $boolArg3, array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $intArg1,
            'arg_2' => $strArg2,
            'arg_3' => $boolArg3,
            'arg_4' => $tabArg4
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }





    // Methods action
    // ******************************************************************************

    public function action($strAdd = '', $strAdd2 = '')
    {
        echo('<br />Action class test 1<br />');

        // Return result
        return 'Result action class test 1' .
            (is_string($strAdd) && (trim($strAdd) != '') ? ': ' . $strAdd : '').
            (is_string($strAdd2) && (trim($strAdd2) != '') ? ': ' . $strAdd2 : '');
    }



}