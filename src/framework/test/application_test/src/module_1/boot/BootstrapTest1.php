<?php

namespace liberty_code\framework\framework\test\application_test\src\module_1\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;



class BootstrapTest1 extends DefaultBootstrap
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(AppInterface $objApp)
    {
        // Call parent constructor
        parent::__construct($objApp, 'module_1');
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @param string $strAdd
     * @param string $strAdd2
     */
    public function boot($strAdd = 'Default value 1', $strAdd2 = 'Default value 2')
    {
        $strText =
            'Boot module "' . $this->getObjModule()->getStrKey() . '": ' .
            get_class($this) . ': ' .
            ((trim($strAdd) != '') ? ': Arg 1 "' . $strAdd . '"' : '') .
            ((trim($strAdd2) != '') ? ': Arg 2 "' . $strAdd2 . '"' : '');

        echo(sprintf('<br />%1$s<br />', $strText));
    }



}