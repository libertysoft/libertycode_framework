<?php

namespace liberty_code\framework\framework\test\application_test\src\module_1\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;



class BootstrapTest2 extends DefaultBootstrap
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(AppInterface $objApp)
    {
        // Call parent constructor
        parent::__construct($objApp, 'module_1');
    }





    // Methods execute
    // ******************************************************************************

    public function boot()
    {
        $strText =
            'Boot module "' . $this->getObjModule()->getStrKey() . '": ' .
            get_class($this);

        echo(sprintf('<br />%1$s<br />', $strText));
    }



}