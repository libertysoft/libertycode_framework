<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\register\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\register\build\library\ConstBuilder;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate register,
     * from specified application object.
     *
     * @param AppInterface $objApp
     */
    protected static function hydrateRegister(AppInterface $objApp)
    {
        // Init var
        $objRegister = $objApp->getObjRegister();
        $objModuleCollection = $objApp->getObjModuleCollection();

        // Get data source
        $tabDataSrc = array();
        $tabDataSrcBase = $objModuleCollection->getTabMergeConfigRegister();
        if(is_array($tabDataSrcBase))
        {
            foreach ($tabDataSrcBase as $key => $value)
            {
                // Register formatted data
                $tabDataSrc[$key] = static::getValue($objApp, $value);
            }
        }

        // Hydrate register, if required
        $objRegister->hydrateItem($tabDataSrc ,false , true);
    }



    /**
     * Hydrate specified application object.
     *
     * @param AppInterface $objApp
     */
    public static function hydrateApp(AppInterface $objApp)
    {
        static::hydrateRegister($objApp);
    }





	// Methods getters
	// ******************************************************************************

	/**
	 * Get formatted value,
     * from specified value.
	 *
     * @param AppInterface $objApp
	 * @param mixed $value
	 * @return mixed
	 */
	protected static function getValue(AppInterface $objApp, $value)
	{
		// Init var
		$result = $value;

		// Check format required
		if(
			is_array($value) &&
			isset($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_TYPE]) &&
			is_string($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_TYPE])
		)
		{
			// Init var
			$objProvider = $objApp->getObjProvider();
			$valueFormat = null;

			// Check if class required
			if(
				($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_TYPE] == ConstBuilder::CONF_VALUE_KEY_CLASS_PATH) &&
				isset($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_PATH]) &&
				is_string($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_PATH])
			)
			{
				$strClassPath = $value[ConstBuilder::TAB_CONFIG_KEY_VALUE_PATH];
				$valueFormat = $objProvider->getFromClassPath($strClassPath);
			}
			// Check dependency required
			else if(
				($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_TYPE] == ConstBuilder::CONF_VALUE_KEY_DEPENDENCY_KEY) &&
				isset($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_KEY]) &&
				is_string($value[ConstBuilder::TAB_CONFIG_KEY_VALUE_KEY])
			)
			{
				$strKey = $value[ConstBuilder::TAB_CONFIG_KEY_VALUE_KEY];
				$valueFormat = $objProvider->getFromKey($strKey);
			}

			// Set formatted value, if required
			if(!is_null($valueFormat))
			{
				$result = $valueFormat;
			}
		}
		
		// Return result
		return $result;
	}
	
	
	
}