<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\framework\register\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Configuration
	const TAB_CONFIG_KEY_VALUE_TYPE = 'type';
	const TAB_CONFIG_KEY_VALUE_PATH = 'path';
	const TAB_CONFIG_KEY_VALUE_KEY = 'key';

	const CONF_VALUE_KEY_CLASS_PATH = 'class_path';
	const CONF_VALUE_KEY_DEPENDENCY_KEY = 'dependency_key';
}