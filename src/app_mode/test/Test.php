<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\app_mode\mode\model\DefaultModeCollection;
use liberty_code\framework\app_mode\mode\factory\model\DefaultModeFactory;
use liberty_code\framework\app_mode\build\model\DefaultBuilder;
use liberty_code\framework\app_mode\select\model\DefaultSelector;



// Init var
$objModeCollection = new DefaultModeCollection();
$objSelector = new DefaultSelector($objModeCollection);
$objModeFactory = new DefaultModeFactory();
$objBuilder = new DefaultBuilder($objModeFactory);

$tabDataSrc = array(
    'mode_1' => [
        ConstConfig::TAB_CONFIG_KEY_CACHE => true,
        ConstConfig::TAB_CONFIG_KEY_DEBUG => false,
        ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => true,
        ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => true
    ],
    'mode_2' => [
        ConstConfig::TAB_CONFIG_KEY_CACHE => true,
        ConstConfig::TAB_CONFIG_KEY_DEBUG => false,
        ConstConfig::TAB_CONFIG_KEY_LOG_INFO => false,
        ConstConfig::TAB_CONFIG_KEY_LOG_WARNING => false,
        ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => true,
        ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => true
    ],
    'mode_3' => [
    ]
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test mode
$objBuilder->hydrateModeCollection($objModeCollection);
foreach($objSelector->getObjModeCollection()->getTabKey() as $strKey)
{
	$objMode = $objSelector->getObjModeCollection()->getObjMode($strKey);
	
	echo('Test mode "' . $strKey . '": <br />');
	try{
		echo('Collection check: <pre>');var_dump($objModeCollection->checkExists($strKey));echo('</pre>');
		
		echo('Mode collection count: <pre>');var_dump(count($objMode->getObjModeCollection()->getTabKey()));echo('</pre>');
		echo('Mode key: <pre>');var_dump($objMode->getStrKey());echo('</pre>');
		echo('Mode configuration: <pre>');var_dump($objMode->getObjConfig()->getTabValue());echo('</pre>');
		
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . ' - ' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test selector default active values
$objSelector->setActiveKey('mode_2');
echo('Selector default active key: <pre>');var_dump($objSelector->getStrActiveKey());echo('</pre>');
echo('Selector default active mode configuration: <pre>');var_dump($objSelector->getObjActiveMode()->getObjConfig()->getTabValue());echo('</pre>');

echo('<br /><br /><br />');



// Test selector
$tabKey = array(
	'test', // Ko: not found
	7, // Ko: not found
	'dev', // Ko: not found
	'mode_2', // Ok
	'prod', // Ko: not found
	'mode_1' // Ok
);
foreach($tabKey as $strKey)
{
	echo('Selector: test set active key ("' . $strKey . '"): <br />');
	
	try{
		$objSelector->setActiveKey($strKey);
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . ' - ' . $e->getMessage()));
		echo('<br />');
	}
	
	try{
		echo('Selector active key ("' . $strKey . '"): <pre>');var_dump($objSelector->getStrActiveKey());echo('</pre>');
		echo('Selector active mode configuration ("' . $strKey . '"): <pre>');var_dump($objSelector->getObjActiveMode()->getObjConfig()->getTabValue());echo('</pre>');
		
		foreach($objSelector->getObjActiveMode()->getObjConfig()->getTabKey() as $strConfigKey)
		{
			$objSelector->setActiveModeValue($strConfigKey, $objSelector->getActiveModeValue($strConfigKey));
			echo('Selector active configuration ("' . $strConfigKey . '"): <pre>');var_dump($objSelector->getActiveModeValue($strConfigKey));echo('</pre>');
		}
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . ' - ' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


