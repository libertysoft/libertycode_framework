<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate mode collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate mode collection:
 * [
 *     mode key 1 => Array of configuration values (@see ModeFactoryInterface::getObjMode() ),
 *
 *     ...,
 *
 *     mode key N => ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\framework\app_mode\build\api\BuilderInterface;

use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;
use liberty_code\framework\app_mode\mode\factory\api\ModeFactoryInterface;
use liberty_code\framework\app_mode\build\library\ConstBuilder;
use liberty_code\framework\app_mode\build\exception\FactoryInvalidFormatException;
use liberty_code\framework\app_mode\build\exception\DataSrcInvalidFormatException;



/**
 * @method ModeFactoryInterface getObjFactory() Get mode factory object.
 * @method void setObjFactory(ModeFactoryInterface $objFactory) Set mode factory object.
 * @method array getTabDataSrc() Get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ModeFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        ModeFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init mode factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateModeCollection(ModeCollectionInterface $objModeCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabMode = array();
        foreach($tabDataSrc as $strKey => $tabValue)
        {
            // Get new mode
            $objMode = $objFactory->getObjMode($strKey, $tabValue);
            $tabMode[] = $objMode;
        }

        // Clear modes from collection, if required
        if($boolClear)
        {
            $objModeCollection->removeModeAll();
        }

        // Register modes on collection
        $objModeCollection->setTabMode($tabMode);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
			ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
					DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}

	
	
}