<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified mode collection instance, with modes.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\build\api;

use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified mode collection.
     *
     * @param ModeCollectionInterface $objModeCollection
     * @param boolean $boolClear = true
     */
    public function hydrateModeCollection(ModeCollectionInterface $objModeCollection, $boolClear = true);
}