<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\select\library;



class ConstSelector
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_MODE_COLLECTION = 'objModeCollection';
	const DATA_KEY_DEFAULT_ACTIVE_KEY = 'strActiveKey';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_MODE_COLLECTION_INVALID_FORMAT = 'Following mode collection "%1$s" invalid! It must be a mode collection object.';
	const EXCEPT_MSG_ACTIVE_KEY_NOT_FOUND = 'Following active key "%1$s" not found in mode collection!';
}