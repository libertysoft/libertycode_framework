<?php
/**
 * Description :
 * This class allows to describe behavior of selector class.
 * Selector allows to select a mode from its modes collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\select\api;

use liberty_code\framework\app_mode\mode\api\ModeInterface;
use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;



interface SelectorInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get mode collection object.
	 *
	 * @return null|ModeCollectionInterface
	 */
	public function getObjModeCollection();
	
	
	
	/**
	 * Get string active mode key (to select active mode object).
	 *
	 * @return null|string
	 */
	public function getStrActiveKey();
	
	
	
    /**
     * Get active mode object.
     *
     * @return ModeInterface
     */
    public function getObjActiveMode();
	
	
	
	/**
	 * Get value from specified active mode configuration key.
	 * 
	 * @param string $strKey
	 * @return null|mixed
	 */
	public function getActiveModeValue($strKey);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set mode collection object.
	 * 
	 * @param ModeCollectionInterface $objModeCollection
	 */
	public function setModeCollection(ModeCollectionInterface $objModeCollection);



	/**
	 * Set string active mode key (to select active mode object).
	 *
	 * @param string $strKey
	 * @param boolean $boolReset = true
	 */
	public function setActiveKey($strKey, $boolReset = true);
	
	
	
	/**
	 * Set specified value for specified active mode configuration key (update).
	 * 
	 * @param string $strKey
	 * @param mixed $value
	 * @return boolean: true if correctly set, false else
	 */
	public function setActiveModeValue($strKey, $value);
}