<?php
/**
 * Description :
 * This class allows to define default selector class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\select\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\framework\app_mode\select\api\SelectorInterface;

use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;
use liberty_code\framework\app_mode\select\library\ConstSelector;
use liberty_code\framework\app_mode\select\exception\ActiveKeyNotFoundException;
use liberty_code\framework\app_mode\select\exception\ModeCollectionInvalidFormatException;



class DefaultSelector extends FixBean implements SelectorInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ModeCollectionInterface $objModeCollection = null
     * @param string $strActiveKey = null
     */
    public function __construct(ModeCollectionInterface $objModeCollection = null, $strActiveKey = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init mode collection if required
        if(!is_null($objModeCollection))
        {
            $this->setModeCollection($objModeCollection);
        }

		// Init properties if required
        if(!is_null($strActiveKey))
        {
            $this->setActiveKey($strActiveKey);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstSelector::DATA_KEY_DEFAULT_MODE_COLLECTION))
        {
            $this->__beanTabData[ConstSelector::DATA_KEY_DEFAULT_MODE_COLLECTION] = null;
        }

		if(!$this->beanExists(ConstSelector::DATA_KEY_DEFAULT_ACTIVE_KEY))
        {
            $this->__beanTabData[ConstSelector::DATA_KEY_DEFAULT_ACTIVE_KEY] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstSelector::DATA_KEY_DEFAULT_MODE_COLLECTION,
            ConstSelector::DATA_KEY_DEFAULT_ACTIVE_KEY
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSelector::DATA_KEY_DEFAULT_MODE_COLLECTION:
                    ModeCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstSelector::DATA_KEY_DEFAULT_ACTIVE_KEY:
                    $objModeCollection = $this->getObjModeCollection();
                    if(
                        is_null($objModeCollection) ||
                        (!$objModeCollection->checkExists($value))
                    )
                    {
                        throw new ActiveKeyNotFoundException($value);
                    }
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************
	
	/**
     * @inheritdoc
     */
    public function getObjModeCollection()
    {
        // Return result
        return $this->beanGet(ConstSelector::DATA_KEY_DEFAULT_MODE_COLLECTION);
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getStrActiveKey()
	{
        // Return result
        return $this->beanGet(ConstSelector::DATA_KEY_DEFAULT_ACTIVE_KEY);
    }
	
	
	
	/**
	 * @inheritdoc
     * @throw ActiveKeyNotFoundException
	 */
	public function getObjActiveMode()
	{
        // Init var
        $objModeCollection = $this->getObjModeCollection();
        $strActiveKey = $this->getStrActiveKey();

        // Get active mode, if possible
        $result = null;
        if(!is_null($objModeCollection))
        {
            $result = $objModeCollection->getObjMode($strActiveKey);
        }

        // Check active mode found
        if(is_null($result))
        {
            throw new ActiveKeyNotFoundException($strActiveKey);
        }

        // Return result
        return $result;
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getActiveModeValue($strKey)
	{
		// Return result
        return $this->getObjActiveMode()->getObjConfig()->getValue($strKey);
	}
	
	
	
	
	
    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
    public function setModeCollection(ModeCollectionInterface $objModeCollection)
	{
		$this->beanSet(ConstSelector::DATA_KEY_DEFAULT_MODE_COLLECTION, $objModeCollection);
	}



    /**
     * @inheritdoc
     */
    public function setActiveKey($strKey, $boolReset = true)
    {
        $this->beanSet(ConstSelector::DATA_KEY_DEFAULT_ACTIVE_KEY, $strKey);
		
		// Reset configuration if required
		if(is_bool($boolReset) && $boolReset)
		{
			$this->getObjActiveMode()->getObjConfig()->resetValue();
		}
    }


	
	/**
	 * @inheritdoc
	 */
	public function setActiveModeValue($strKey, $value)
	{
		// Return result
		return $this->getObjActiveMode()->getObjConfig()->setValue($strKey, $value);
	}
	
	
	
}