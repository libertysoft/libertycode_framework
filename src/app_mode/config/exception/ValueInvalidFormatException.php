<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\config\exception;

use liberty_code\framework\app_mode\config\library\ConstConfig;



class ValueInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param string $strKey
	 * @param mixed $value
     */
	public function __construct($strKey, $value)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_VALUE_INVALID_FORMAT, $strKey, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified value has valid format for specified key.
	 * 
	 * @param string $strKey
     * @param mixed $value
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($strKey, $value)
    {
		// Init var
		$result = is_string($strKey) && (trim($strKey) != ''); // Check key is valid string
		
		// Check value is valid
		if($result)
		{
			switch($strKey)
			{
				case ConstConfig::TAB_CONFIG_KEY_CACHE:
				case ConstConfig::TAB_CONFIG_KEY_DEBUG:
				case ConstConfig::TAB_CONFIG_KEY_LOG_INFO:
				case ConstConfig::TAB_CONFIG_KEY_LOG_WARNING:
				case ConstConfig::TAB_CONFIG_KEY_LOG_ERROR:
				case ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION:
					$result = is_bool($value);
					break;
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($strKey, $value);
		}
		
		// Return result
		return $result;
    }
	
	
	
}