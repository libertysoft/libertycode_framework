<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\config\exception;

use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\app_mode\config\exception\KeyInvalidFormatException;
use liberty_code\framework\app_mode\config\exception\ValueInvalidFormatException;



class DataSrcInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $dataSrc
	 * @param mixed $errorMsg = null
     */
	public function __construct($dataSrc, $errorMsg = null)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(
				(!is_null($errorMsg)) &&
				is_string($errorMsg) && (trim($errorMsg) != '')
			) ?
				' ' . trim($errorMsg) :
				'';
		$this->message = sprintf
		(
			ConstConfig::EXCEPT_MSG_DATA_SRC_INVALID_FORMAT,
			mb_strimwidth(strval($dataSrc), 0, 50, "..."),
			$strErrorMsg
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified data source has valid format.
	 * 
     * @param mixed $dataSrc
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($dataSrc)
    {
		// Init var
		$result = is_array($dataSrc);
		$errorMsg = null;
		
		// Run all data if required
		if($result)
		{
			// Get info and check count data valid
			$tabKey = array_keys($dataSrc);
			$tabConfigKey = ConstConfig::getTabConfigKey();
			$result = (count($tabKey) == count($tabConfigKey));
			
			// Run all data if required
			if($result)
			{
				for($intCpt = 0; $result && ($intCpt < count($tabKey)); $intCpt++)
				{
					// Get data
					$strKey = $tabKey[$intCpt];
					$value = $dataSrc[$strKey];
					
					// Check data
					try
					{
						KeyInvalidFormatException::setCheck($strKey);
						ValueInvalidFormatException::setCheck($strKey, $value);
					}
					catch(\Exception $e)
					{
						$result = false;
						$errorMsg = $e->getMessage();
					}
				}
			}
		}
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($dataSrc) ? serialize($dataSrc) : $dataSrc), $errorMsg);
		}
		
		// Return result
		return $result;
    }
	
	
	
}