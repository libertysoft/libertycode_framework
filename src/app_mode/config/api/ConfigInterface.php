<?php
/**
 * Description :
 * This class allows to describe behavior of configuration class.
 * Configuration contain all parameters about mode.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\config\api;



interface ConfigInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if value exists,
     * from specified configuration key.
     *
     * @param mixed $strKey
     * @return boolean
     */
    public function checkValueExists($strKey);





	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get index array of string configuration keys.
	 * 
	 * @return array
	 */
	public function getTabKey();



    /**
     * Get associative array of configuration values.
     * Format: key => value.
     *
     * @return array
     */
    public function getTabValue();



	/**
	 * Get configuration value,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|mixed
	 */
	public function getValue($strKey);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set specified configuration value,
     * for specified key (update).
	 * 
	 * @param string $strKey
	 * @param mixed $value
	 * @return boolean: true if correctly set, false else
	 */
	public function setValue($strKey, $value);



    /**
     * Set associative array of configuration values.
     * Format: key => value.
     *
     * @param array $tabValue
     * @return boolean: true if all values correctly set, false else
     */
    public function setTabValue($tabValue);



	/**
	 * Reset same value,
     * for specified index array of configuration keys.
	 * If index array of keys empty, reset all keys.
	 * 
	 * @param array $tabKey = array()
     * @return boolean: true if all values correctly reset, false else
	 */
	public function resetValue(array $tabKey = array());
}