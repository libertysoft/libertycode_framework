<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************
	
	// Configuration
    const TAB_CONFIG_KEY_CACHE = 'cache';
	const TAB_CONFIG_KEY_DEBUG = 'debug';
	const TAB_CONFIG_KEY_LOG_INFO = 'log.info';
	const TAB_CONFIG_KEY_LOG_WARNING = 'log.warning';
	const TAB_CONFIG_KEY_LOG_ERROR = 'log.error';
	const TAB_CONFIG_KEY_LOG_EXCEPTION = 'log.exception';
	
	const TAB_CONFIG_DEFAULT_VALUE_CACHE = true;
    const TAB_CONFIG_DEFAULT_VALUE_DEBUG = true;
    const TAB_CONFIG_DEFAULT_VALUE_LOG_INFO = true;
    const TAB_CONFIG_DEFAULT_VALUE_LOG_WARNING = true;
    const TAB_CONFIG_DEFAULT_VALUE_LOG_ERROR = true;
    const TAB_CONFIG_DEFAULT_VALUE_LOG_EXCEPTION = true;
	
	
	
    // Exception message constants
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT = 'Following data source "%1$s" invalid!%2$s';
	const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a string in valid scope.';
	const EXCEPT_MSG_VALUE_INVALID_FORMAT = 'Following value "%2$s" invalid for key "%1$s"! The value must follow standard format for key "%1$s".';
	
	
	
	
	
	// ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration keys array
     *
     * @return array
     */
    static public function getTabConfigKey()
    {
        // Init var
        $result = array(
			self::TAB_CONFIG_KEY_CACHE,
            self::TAB_CONFIG_KEY_DEBUG,
            self::TAB_CONFIG_KEY_LOG_INFO,
            self::TAB_CONFIG_KEY_LOG_WARNING,
			self::TAB_CONFIG_KEY_LOG_ERROR,
			self::TAB_CONFIG_KEY_LOG_EXCEPTION
        );

        // Return result
        return $result;
    }
	
	
	
}