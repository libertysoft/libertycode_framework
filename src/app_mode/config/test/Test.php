<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\app_mode\config\model\DefaultConfig;



// Init var
$objConfig = new DefaultConfig();



// Test properties
echo('Test properties: <br />');

try{
	$objConfig->setDataSrc('test');
} catch (Exception $e) {
	echo($e->getMessage());echo('<br />');
}

try{
	$tabConfig = array(
		ConstConfig::TAB_CONFIG_KEY_CACHE => true,
		ConstConfig::TAB_CONFIG_KEY_DEBUG => false,
		ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => true,
		ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => true
	);
	$objConfig->setDataSrc($tabConfig);
} catch (Exception $e) {
	echo($e->getMessage());echo('<br />');
}

$tabConfig = array(
	ConstConfig::TAB_CONFIG_KEY_CACHE => true,
	ConstConfig::TAB_CONFIG_KEY_DEBUG => false,
	ConstConfig::TAB_CONFIG_KEY_LOG_INFO => false,
	ConstConfig::TAB_CONFIG_KEY_LOG_WARNING => false,
	ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => true,
	ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => true
);
$objConfig->setDataSrc($tabConfig);

echo('<br /><br /><br />');



// Test array key get
echo('Test array key get: <br />');

foreach($objConfig->getTabKey() as $strKey)
{
	echo('Test get "' . $strKey . '": <br />');
	echo('Value: <pre>');var_dump($objConfig->getValue($strKey));echo('</pre>');
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test set
echo('Test set: <br />');

$tabConfig = array(
	ConstConfig::TAB_CONFIG_KEY_CACHE => true,
	ConstConfig::TAB_CONFIG_KEY_DEBUG => false,
	ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => false,
	ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => false
);
$objConfig = new DefaultConfig($tabConfig);

$tabKey = array(
	ConstConfig::TAB_CONFIG_KEY_CACHE => false, // Ok
	ConstConfig::TAB_CONFIG_KEY_LOG_INFO => 'Test value', // Ko: bad value format
	ConstConfig::TAB_CONFIG_KEY_LOG_INFO => true, // Ok
	'Test key' => true, // Ko: key out of scope
	ConstConfig::TAB_CONFIG_KEY_LOG_WARNING => false, // Ok
	'Test key 2' => 'Test value' // Ko: key out of scope
);

foreach($tabKey as $strKey => $value)
{
	echo('Test set "' . $strKey . '": <br />');
	try{
		echo('Set: <pre>');var_dump($objConfig->setValue($strKey, $value));echo('</pre>');
	} catch (Exception $e) {
		echo($e->getMessage());echo('<br />');
	}
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get
echo('Test get: <br />');

$tabKey = array(
	ConstConfig::TAB_CONFIG_KEY_CACHE, // Found
	ConstConfig::TAB_CONFIG_KEY_DEBUG, // Found
	ConstConfig::TAB_CONFIG_KEY_LOG_INFO, // Found
	ConstConfig::TAB_CONFIG_KEY_LOG_WARNING, // Found
	ConstConfig::TAB_CONFIG_KEY_LOG_ERROR, // Found
	ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION, // Found
	'Test key', // Not found: key out of scope
	'test', // Not found: key out of scope
	3 // Not found: bad key format
);

foreach($tabKey as $strKey)
{
	echo('Test get "' . $strKey . '": <br />');
	try{
		echo('Get: <pre>');var_dump($objConfig->getValue($strKey));echo('</pre>');
	} catch (Exception $e) {
		echo($e->getMessage());echo('<br />');
	}
	
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


