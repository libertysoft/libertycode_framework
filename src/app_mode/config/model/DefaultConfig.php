<?php
/**
 * Description :
 * This class allows to define default configuration class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\config\model;

use liberty_code\data\data\table\model\TableData;
use liberty_code\framework\app_mode\config\api\ConfigInterface;

use liberty_code\data\data\library\ConstData;
use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\app_mode\config\exception\DataSrcInvalidFormatException;
use liberty_code\framework\app_mode\config\exception\KeyInvalidFormatException;
use liberty_code\framework\app_mode\config\exception\ValueInvalidFormatException;



class DefaultConfig extends TableData implements ConfigInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * Constructor
	 * 
	 * @param array $tabValue = array()
     */
	public function __construct($tabValue = array())
	{
		// Call parent constructor
		parent::__construct();
		
		// Init value
        $this->setTabValue($tabValue);
	}
	
	
	
	
	
	// Methods initialize
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		// Init bean data
		if(!$this->beanExists(ConstData::DATA_KEY_DEFAULT_DATA_SRC))
		{
			$tabConfig = array(
				ConstConfig::TAB_CONFIG_KEY_CACHE => ConstConfig::TAB_CONFIG_DEFAULT_VALUE_CACHE,
				ConstConfig::TAB_CONFIG_KEY_DEBUG => ConstConfig::TAB_CONFIG_DEFAULT_VALUE_DEBUG,
				ConstConfig::TAB_CONFIG_KEY_LOG_INFO => ConstConfig::TAB_CONFIG_DEFAULT_VALUE_LOG_INFO,
				ConstConfig::TAB_CONFIG_KEY_LOG_WARNING => ConstConfig::TAB_CONFIG_DEFAULT_VALUE_LOG_WARNING,
				ConstConfig::TAB_CONFIG_KEY_LOG_ERROR => ConstConfig::TAB_CONFIG_DEFAULT_VALUE_LOG_ERROR,
				ConstConfig::TAB_CONFIG_KEY_LOG_EXCEPTION => ConstConfig::TAB_CONFIG_DEFAULT_VALUE_LOG_EXCEPTION
			);
			$this->beanAdd(ConstData::DATA_KEY_DEFAULT_DATA_SRC, $tabConfig);
		}
		
		// Call parent method
        parent::beanHydrateDefault();
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstData::DATA_KEY_DEFAULT_DATA_SRC:
					DataSrcInvalidFormatException::setCheck($value);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}

	
	
	/**
	 * @inheritdoc
	 * @param string $strKey (=> $strPath)
	 */
	public function checkValidPath($strKey, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			KeyInvalidFormatException::setCheck($strKey);
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}

	
	
	/**
	 * @inheritdoc
	 * @param string $strKey (=> $strPath)
	 */
	public function checkValidValue($strKey, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			ValueInvalidFormatException::setCheck($strKey, $value);
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}


	
	/**
	 * @inheritdoc
	 * @param string $strKey (=> $strPath)
	 */
	public function checkValidRemove($strKey, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkValueExists($key)
    {
        // return result
        return parent::checkValueExists($key);
    }





    // Methods getters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function getTabKey()
	{
		// return result
		return array_keys($this->getDataSrc());
	}



    /**
     * @inheritdoc
     */
    public function getTabValue()
    {
        // return result
        return $this->getDataSrc();
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getValue($strKey)
	{
		// Return parent result
		return parent::getValue($strKey);
	}
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setValue($strKey, $value)
	{
		// Return parent result
		return parent::setValue($strKey, $value);
	}



    /**
     * @inheritdoc
     */
    public function setTabValue($tabValue)
    {
        // Init var
        $result = true;

        // Run all values
        foreach($tabValue as $strKey => $value)
        {
            // Set value
            $result = $this->setValue($strKey, $value) && $result;
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function resetValue(array $tabKey = array())
	{
		// Init var
        $result = true;
        $tabKey = ((count($tabKey) == 0) ? $this->getTabKey() : $tabKey);
		
		// Run all keys
		foreach($tabKey as $strKey)
		{
			// Reset same value (Allows indirectly to call event on set action)
            $result = $this->setValue($strKey, $this->getValue($strKey)) && $result;
		}

        // Return result
        return $result;
	}
	
	
	
}