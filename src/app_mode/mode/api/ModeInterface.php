<?php
/**
 * Description :
 * This class allows to describe behavior of mode class.
 * Mode is a group of parameters.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\api;

use liberty_code\framework\app_mode\config\api\ConfigInterface;
use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;



interface ModeInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get mode collection object.
	 *
	 * @return null|ModeCollectionInterface
	 */
	public function getObjModeCollection();
	
	
	
	/**
	 * Get string key (considered as mode id).
	 *
	 * @return string
	 */
	public function getStrKey();
	
	
	
    /**
     * Get configuration object.
     *
     * @return ConfigInterface
     */
    public function getObjConfig();
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set mode collection object.
	 * 
	 * @param ModeCollectionInterface $objModeCollection = null
	 */
	public function setModeCollection(ModeCollectionInterface $objModeCollection = null);



    /**
     * Set string key (considered as mode id).
     *
     * @param string $strKey
     */
    public function setKey($strKey);



    /**
     * Set configuration object.
     *
     * @param ConfigInterface $objConfig
     */
    public function setConfig(ConfigInterface $objConfig);
}