<?php
/**
 * Description :
 * This class allows to describe behavior of modes collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\api;

use liberty_code\framework\app_mode\mode\api\ModeInterface;



interface ModeCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************
	
	/**
	 * Check if specified mode key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get index array of string mode keys.
	 * 
	 * @return array
	 */
	public function getTabKey();
	
	
	
	/**
	 * Get mode from specified key.
	 * 
	 * @param string $strKey
	 * @return null|ModeInterface
	 */
	public function getObjMode($strKey);

	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set mode and return its key.
	 * 
	 * @param ModeInterface $objMode
	 * @return string
     */
	public function setMode(ModeInterface $objMode);



    /**
     * Set list of modes (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|ModeCollectionInterface $tabMode
     * @return array
     */
    public function setTabMode($tabMode);



    /**
     * Remove mode and return its instance.
     *
     * @param string $strKey
     * @return ModeInterface
     */
    public function removeMode($strKey);



    /**
     * Remove all modes.
     */
    public function removeModeAll();
}