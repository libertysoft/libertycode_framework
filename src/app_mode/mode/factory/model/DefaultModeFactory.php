<?php
/**
 * Description :
 * This class allows to define default mode factory class.
 * Default mode factory allows to provide and hydrate default mode instance.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\framework\app_mode\mode\factory\api\ModeFactoryInterface;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\app_mode\config\model\DefaultConfig;
use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;
use liberty_code\framework\app_mode\mode\model\DefaultMode;
use liberty_code\framework\app_mode\mode\exception\ModeCollectionInvalidFormatException;
use liberty_code\framework\app_mode\mode\factory\library\ConstModeFactory;



/**
 * @method null|ModeCollectionInterface getObjModeCollection() Get mode collection object.
 * @method void setObjModeCollection(null|ModeCollectionInterface $objModeCollection) Set mode collection object.
 */
class DefaultModeFactory extends DefaultFactory implements ModeFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ModeCollectionInterface $objModeCollection = null
     */
    public function __construct(
        ModeCollectionInterface $objModeCollection = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init mode collection
        $this->setObjModeCollection($objModeCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstModeFactory::DATA_KEY_DEFAULT_MODE_COLLECTION))
        {
            $this->__beanTabData[ConstModeFactory::DATA_KEY_DEFAULT_MODE_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstModeFactory::DATA_KEY_DEFAULT_MODE_COLLECTION
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstModeFactory::DATA_KEY_DEFAULT_MODE_COLLECTION:
                    ModeCollectionInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjMode($strKey, array $tabValue)
    {
        // Init var
        $result = $this->getObjInstance(DefaultMode::class);

        // Init mode, if required
        if(is_null($result))
        {
            $objConfig = $this->getObjInstance(DefaultConfig::class);
            $objConfig = (is_null($objConfig) ? new DefaultConfig() : $objConfig);
            $result = new DefaultMode($strKey, $objConfig);
        }

        // Hydrate mode
        $objModeCollection = $this->getObjModeCollection();
        if(!is_null($objModeCollection))
        {
            $result->setModeCollection($objModeCollection);
        }

        $result->getObjConfig()->setTabValue($tabValue);

        // Return result
        return $result;
    }



}