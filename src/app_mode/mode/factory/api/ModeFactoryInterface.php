<?php
/**
 * Description :
 * This class allows to describe behavior of mode factory class.
 * Mode factory allows to provide new mode instance.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\factory\api;

use liberty_code\framework\app_mode\config\api\ConfigInterface;
use liberty_code\framework\app_mode\mode\api\ModeInterface;



interface ModeFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get new object instance mode,
     * hydrated from specified key and array of value.
     * Array of value format: @see ConfigInterface::setTabValue()
     *
     * @param string $strKey
     * @param array $tabValue
     * @return ModeInterface
     */
    public function getObjMode(
        $strKey,
        array $tabValue
    );
}