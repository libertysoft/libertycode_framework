<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\library;



class ConstMode
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_MODE_COLLECTION = 'objModeCollection';
    const DATA_KEY_DEFAULT_KEY = 'strKey';
    const DATA_KEY_DEFAULT_CONFIG = 'objConfig';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_MODE_COLLECTION_INVALID_FORMAT = 'Following mode collection "%1$s" invalid! It must be a mode collection object.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a valid string, not empty.';
	const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following configuration "%1$s" invalid! It must be a configuration object.';
	const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a valid string, not empty, in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Following value "%1$s" invalid! The value must be a mode object in collection.';
}