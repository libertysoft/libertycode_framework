<?php
/**
 * Description :
 * This class allows to define default mode class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\framework\app_mode\mode\api\ModeInterface;

use liberty_code\framework\app_mode\config\api\ConfigInterface;
use liberty_code\framework\app_mode\mode\library\ConstMode;
use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;
use liberty_code\framework\app_mode\mode\exception\KeyInvalidFormatException;
use liberty_code\framework\app_mode\mode\exception\ConfigInvalidFormatException;
use liberty_code\framework\app_mode\mode\exception\ModeCollectionInvalidFormatException;



class DefaultMode extends FixBean implements ModeInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
	 * @param string $strKey
	 * @param ConfigInterface $objConfig
     * @param ModeCollectionInterface $objModeCollection = null
     */
    public function __construct($strKey, ConfigInterface $objConfig, ModeCollectionInterface $objModeCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init mode collection if required
        if(!is_null($objModeCollection))
        {
            $this->setModeCollection($objModeCollection);
        }

		// Init properties
		$this->setKey($strKey);
		$this->setConfig($objConfig);
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstMode::DATA_KEY_DEFAULT_MODE_COLLECTION))
        {
            $this->__beanTabData[ConstMode::DATA_KEY_DEFAULT_MODE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstMode::DATA_KEY_DEFAULT_KEY))
        {
            $this->__beanTabData[ConstMode::DATA_KEY_DEFAULT_KEY] = '';
        }

        if(!$this->beanExists(ConstMode::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstMode::DATA_KEY_DEFAULT_CONFIG] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMode::DATA_KEY_DEFAULT_MODE_COLLECTION,
            ConstMode::DATA_KEY_DEFAULT_KEY,
			ConstMode::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMode::DATA_KEY_DEFAULT_MODE_COLLECTION:
                    ModeCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstMode::DATA_KEY_DEFAULT_KEY:
                    KeyInvalidFormatException::setCheck($value);
                    break;
					
				case ConstMode::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************
	
	/**
     * @inheritdoc
     */
    public function getObjModeCollection()
    {
        // Return result
        return $this->beanGet(ConstMode::DATA_KEY_DEFAULT_MODE_COLLECTION);
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getStrKey()
	{
        // Return result
        return $this->beanGet(ConstMode::DATA_KEY_DEFAULT_KEY);
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getObjConfig()
	{
        // Return result
        return $this->beanGet(ConstMode::DATA_KEY_DEFAULT_CONFIG);
    }
	
	

	
	
    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setModeCollection(ModeCollectionInterface $objModeCollection = null)
	{
		$this->beanSet(ConstMode::DATA_KEY_DEFAULT_MODE_COLLECTION, $objModeCollection);
	}



    /**
     * @inheritdoc
     */
    public function setKey($strKey)
    {
        $this->beanSet(ConstMode::DATA_KEY_DEFAULT_KEY, $strKey);
    }



    /**
     * @inheritdoc
     */
    public function setConfig(ConfigInterface $objConfig)
    {
        $this->beanSet(ConstMode::DATA_KEY_DEFAULT_CONFIG, $objConfig);
    }



}