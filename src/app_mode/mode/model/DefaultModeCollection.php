<?php
/**
 * Description :
 * This class allows to define default mode collection class.
 * key: mode key => value: mode object.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\framework\app_mode\mode\api\ModeInterface;
use liberty_code\framework\app_mode\mode\exception\CollectionKeyInvalidFormatException;
use liberty_code\framework\app_mode\mode\exception\CollectionValueInvalidFormatException;



class DefaultModeCollection extends DefaultBean implements ModeCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			// Check value argument
			CollectionValueInvalidFormatException::setCheck($value);

			// Check key argument
			/** @var ModeInterface $value */
			if(
				(!is_string($key)) ||
				($key != $value->getStrKey())
			)
			{
				throw new CollectionKeyInvalidFormatException($key);
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





	// Methods check
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkExists($strKey)
	{
		// Return result
        return (!is_null($this->getObjMode($strKey)));
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function getTabKey()
	{
		// return result
		return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getObjMode($strKey)
	{
		// Init var
		$result = null;
		
		// Try to get module object if found
		try
		{
			if($this->beanDataExists($strKey))
			{
				$result = $this->beanGetData($strKey);
			}
		}
		catch(\Exception $e)
		{
		}
		
		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setMode(ModeInterface $objMode)
	{
		// Init var
		$strKey = $objMode->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objMode);
		
		// return result
		return $strKey;
	}
	
	
	
	/**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

		// Register mode collection on mode object
		/** @var ModeInterface $val */
		$val->setModeCollection($this);
    }



    /**
     * @inheritdoc
     */
    public function setTabMode($tabMode)
    {
        // Init var
        $result = array();

        // Case index array of modes
        if(is_array($tabMode))
        {
            // Run all modes and for each, try to set
            foreach($tabMode as $mode)
            {
                $strKey = $this->setMode($mode);
                $result[] = $strKey;
            }
        }
        // Case collection of modes
        else if($tabMode instanceof ModeCollectionInterface)
        {
            // Run all routes and for each, try to set
            foreach($tabMode->getTabKey() as $strKey)
            {
                $objMode = $tabMode->getObjMode($strKey);
                $strKey = $this->setMode($objMode);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeMode($strKey)
    {
        // Init var
        $result = $this->getObjMode($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeModeAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeMode($strKey);
        }
    }



}