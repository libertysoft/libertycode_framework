<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\app_mode\mode\exception;

use liberty_code\framework\app_mode\mode\library\ConstMode;
use liberty_code\framework\app_mode\mode\api\ModeCollectionInterface;



class ModeCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $modeCollection
     */
	public function __construct($modeCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMode::EXCEPT_MSG_MODE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($modeCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified mode collection has valid format.
	 * 
     * @param mixed $modeCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($modeCollection)
    {
		// Init var
		$result = (
			(is_null($modeCollection)) ||
			($modeCollection instanceof ModeCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($modeCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}