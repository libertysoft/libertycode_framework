<?php
/**
 * Description :
 * This class allows to define execution throwable warning handler class.
 * Execution throwable warning handler allows to handle throwable warnings,
 * during application execution.
 *
 * Execution throwable warning handler uses the following specified configuration:
 * [
 *     Throwable warning handler configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\error\warning\handler\throwable\model;

use liberty_code\error\warning\handler\throwable\model\ThrowableWarnHandler;

use liberty_code\request_flow\response\api\ResponseInterface;



abstract class ExecThrowableWarnHandler extends ThrowableWarnHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get response object,
     * from specified index array of warnings.
     *
     * @param mixed[] $tabWarning
     * @param ResponseInterface $objExecutionResponse
     * @param boolean $boolExecutionSuccess
     * @return ResponseInterface
     */
    abstract protected function getObjResponse(
        array $tabWarning,
        ResponseInterface $objExecutionResponse,
        $boolExecutionSuccess
    );





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * @return ResponseInterface
     */
    public function execute(array $tabWarning, $resultExecution, $boolExecutionSuccess)
    {
        // Init var
        $result = (
            ($resultExecution instanceof ResponseInterface) ?
                $this->getObjResponse($tabWarning, $resultExecution, $boolExecutionSuccess) :
                null
        );

        // Return response
        return $result;
    }



}