<?php
/**
 * Description :
 * This class allows to define fixed system throwable handler class.
 * Fixed system throwable handler allows to handle throwable errors,
 * during application system running,
 * from a specified fixed configuration.
 *
 * Fixed system throwable handler uses the following specified configuration:
 * [
 *     System throwable handler configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\error\handler\throwable\fix\model;

use liberty_code\framework\error\handler\throwable\model\SysThrowableHandler;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\error\handler\library\ConstHandler;
use liberty_code\error\handler\throwable\fix\exception\ConfigInvalidFormatException;



abstract class FixSysThrowableHandler extends SysThrowableHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(RegisterInterface $objWarnRegister)
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($objWarnRegister, $tabConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHandler::DATA_KEY_DEFAULT_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}