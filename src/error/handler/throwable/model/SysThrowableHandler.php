<?php
/**
 * Description :
 * This class allows to define system throwable handler class.
 * System throwable handler allows to handle throwable errors,
 * during application system running.
 *
 * System throwable handler uses the following specified configuration:
 * [
 *     Throwable handler configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\error\handler\throwable\model;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\framework\error\handler\throwable\model\ExecThrowableHandler;



abstract class SysThrowableHandler extends ExecThrowableHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * @var RegisterInterface
     */
    protected $objWarnRegister;
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RegisterInterface $objWarnRegister
     * @param array $tabConfig = null
     */
    public function __construct(
        RegisterInterface $objWarnRegister,
        array $tabConfig = null
    )
    {
        // Init properties
        $this->objWarnRegister = $objWarnRegister;

        // Call parent constructor
        parent::__construct($tabConfig);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get warning register object.
     *
     * @return RegisterInterface
     */
    public function getObjWarnRegister()
    {
        // Return result
        return $this->objWarnRegister;
    }



    /**
     * Get response object formatted with warnings,
     * from specified response.
     * Overwrite it to set specific feature.
     *
     * @param ResponseInterface $objResponse
     * @return ResponseInterface
     */
    protected function getObjWarnResponse(ResponseInterface $objResponse)
    {
        // Return result
        return $objResponse;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute($error)
    {
        // Send response
        $objResponse = $this->getObjResponse($error);
        $objResponse = $this->getObjWarnResponse($objResponse);
        $objResponse->send();
    }



}