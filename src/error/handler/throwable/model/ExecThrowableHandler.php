<?php
/**
 * Description :
 * This class allows to define execution throwable handler class.
 * Execution throwable handler allows to handle throwable errors,
 * during application execution.
 *
 * Execution throwable handler uses the following specified configuration:
 * [
 *     Throwable handler configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\error\handler\throwable\model;

use liberty_code\error\handler\throwable\model\ThrowableHandler;

use liberty_code\request_flow\response\api\ResponseInterface;



abstract class ExecThrowableHandler extends ThrowableHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get response object,
     * from specified error.
     *
     * @param mixed $error
     * @return ResponseInterface
     */
    abstract protected function getObjResponse($error);





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * @return ResponseInterface
     */
    public function execute($error)
    {
        // Return response
        return $this->getObjResponse($error);
    }



}