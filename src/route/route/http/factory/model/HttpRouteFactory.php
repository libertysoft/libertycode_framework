<?php
/**
 * Description :
 * This class allows to define HTTP route factory class.
 * HTTP route factory allows to provide and hydrate HTTP route instance, used for framework applications.
 *
 * HTTP route factory uses the following specified configuration, to get and hydrate route:
 * [
 *     HTTP route factory configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\route\route\http\factory\model;

use liberty_code\http\route\factory\model\HttpRouteFactory as BaseHttpRouteFactory;

use liberty_code\framework\route\route\http\pattern\model\HttpPatternRoute;
use liberty_code\framework\route\route\http\param\model\HttpParamRoute;
use liberty_code\framework\route\route\http\fix\model\HttpFixRoute;
use liberty_code\framework\route\route\http\separator\model\HttpSeparatorRoute;
use liberty_code\http\route\factory\library\ConstHttpRouteFactory;



class HttpRouteFactory extends BaseHttpRouteFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrRouteClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_PATTERN:
                $result = HttpPatternRoute::class;
                break;

            case null:
            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_PARAM:
                $result = HttpParamRoute::class;
                break;

            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_FIX:
                $result = HttpFixRoute::class;
                break;

            case ConstHttpRouteFactory::CONFIG_TYPE_HTTP_SEPARATOR:
                $result = HttpSeparatorRoute::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}