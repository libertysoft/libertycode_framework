<?php
/**
 * Description :
 * This class allows to define HTTP pattern route.
 * HTTP pattern route is HTTP pattern route, used for framework applications.
 *
 * HTTP pattern route uses the following specified configuration:
 * [
 *     HTTP pattern route configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\route\route\http\pattern\model;

use liberty_code\http\route\pattern\model\HttpPatternRoute as BaseHttpPatternRoute;

use liberty_code\framework\request_flow\request\http\library\ToolBoxHttpRequest;



class HttpPatternRoute extends BaseHttpPatternRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = parent::getStrHandleSource($tabCallArg, $tabStrCallElm);
        $result = ((!is_null($result)) ? ToolBoxHttpRequest::getStrFormatRoute($result) : $result);

        // Return result
        return $result;
    }



}