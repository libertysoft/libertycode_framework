<?php
/**
 * Description :
 * This class allows to define HTTP separator route.
 * HTTP separator route is HTTP separator route, used for framework applications.
 *
 * HTTP separator route uses the following specified configuration:
 * [
 *     HTTP separator route configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\route\route\http\separator\model;

use liberty_code\http\route\separator\model\HttpSeparatorRoute as BaseHttpSeparatorRoute;

use liberty_code\framework\request_flow\request\http\library\ToolBoxHttpRequest;



class HttpSeparatorRoute extends BaseHttpSeparatorRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = parent::getStrHandleSource($tabCallArg, $tabStrCallElm);
        $result = ((!is_null($result)) ? ToolBoxHttpRequest::getStrFormatRoute($result) : $result);

        // Return result
        return $result;
    }



}