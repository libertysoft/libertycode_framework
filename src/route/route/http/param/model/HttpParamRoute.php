<?php
/**
 * Description :
 * This class allows to define HTTP param route.
 * HTTP param route is HTTP param route, used for framework applications.
 *
 * HTTP param route uses the following specified configuration:
 * [
 *     HTTP param route configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\route\route\http\param\model;

use liberty_code\http\route\param\model\HttpParamRoute as BaseHttpParamRoute;

use liberty_code\framework\request_flow\request\http\library\ToolBoxHttpRequest;



class HttpParamRoute extends BaseHttpParamRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = parent::getStrHandleSource($tabCallArg, $tabStrCallElm);
        $result = ((!is_null($result)) ? ToolBoxHttpRequest::getStrFormatRoute($result) : $result);

        // Return result
        return $result;
    }



}