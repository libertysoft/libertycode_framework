<?php
/**
 * Description :
 * This class allows to define HTTP fix route.
 * HTTP fix route is HTTP fix route, used for framework applications.
 *
 * HTTP fix route uses the following specified configuration:
 * [
 *     HTTP fix route configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\route\route\http\fix\model;

use liberty_code\http\route\fix\model\HttpFixRoute as BaseHttpParamRoute;

use liberty_code\framework\request_flow\request\http\library\ToolBoxHttpRequest;



class HttpFixRoute extends BaseHttpParamRoute
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrHandleSource(
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = parent::getStrHandleSource($tabCallArg, $tabStrCallElm);
        $result = ((!is_null($result)) ? ToolBoxHttpRequest::getStrFormatRoute($result) : $result);

        // Return result
        return $result;
    }



}