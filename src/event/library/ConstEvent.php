<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\event\library;



class ConstEvent
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Event configuration
    const EVENT_NAME_APP_BEFORE_MODULE_BOOT = 'app_before_module_boot';
    const EVENT_NAME_APP_AFTER_MODULE_BOOT = 'app_after_module_boot';
	const EVENT_NAME_APP_BEFORE_FRONT_CONTROLLER_EXECUTION = 'app_before_front_controller_execution';
    const EVENT_NAME_APP_AFTER_FRONT_CONTROLLER_EXECUTION = 'app_after_front_controller_execution';
    const EVENT_NAME_APP_BEFORE_RESPONSE_SEND = 'app_before_response_send';
    const EVENT_NAME_APP_AFTER_RESPONSE_SEND = 'app_after_response_send';
    const EVENT_NAME_REQUEST_FLOW_BEFORE_ROUTE_EXECUTION = 'request_flow_before_route_execution';
    const EVENT_NAME_REQUEST_FLOW_AFTER_ROUTE_EXECUTION = 'request_flow_after_route_execution';
}