<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\library;



class ConstApp
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_TAB_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER = 'objConfigParserBuilder';
	const DATA_KEY_DEFAULT_APP_MODE = 'objAppMode';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';
	const DATA_KEY_DEFAULT_MODULE_COLLECTION = 'objModuleCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'objConfig';
	const DATA_KEY_DEFAULT_PROVIDER = 'objProvider';
	const DATA_KEY_DEFAULT_REGISTER = 'objRegister';
    const DATA_KEY_DEFAULT_OBSERVER = 'objObserver';
	const DATA_KEY_DEFAULT_FRONT_CONTROLLER = 'objFrontController';
    const DATA_KEY_DEFAULT_EXEC_TRYER = 'objExecTryer';
    const DATA_KEY_DEFAULT_SYS_TRYER = 'objSysTryer';



    // Configuration
    const TAB_CONFIG_KEY_ROOT_DIR_PATH = 'root_dir_path';
    const TAB_CONFIG_KEY_RESPONSE_REQUIRE = 'response_require';



    // Exception message constants
    const EXCEPT_MSG_TAB_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default application configuration standard.';
    const EXCEPT_MSG_CONFIG_PARSER_BUILDER_INVALID_FORMAT = 'Following configuration parser builder "%1$s" invalid! It must be a parser builder object.';
	const EXCEPT_MSG_APP_MODE_INVALID_FORMAT = 'Following application mode selector "%1$s" invalid! It must be a selector object.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';
	const EXCEPT_MSG_MODULE_COLLECTION_INVALID_FORMAT = 'Following module collection "%1$s" invalid! It must be a module collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following configuration "%1$s" invalid! It must be a configuration object.';
	const EXCEPT_MSG_PROVIDER_INVALID_FORMAT = 'Following DI provider "%1$s" invalid! It must be a provider object.';
	const EXCEPT_MSG_REGISTER_INVALID_FORMAT = 'Following register "%1$s" invalid! It must be a register object.';
    const EXCEPT_MSG_OBSERVER_INVALID_FORMAT = 'Following observer "%1$s" invalid! It must be an observer object.';
	const EXCEPT_MSG_FRONT_CONTROLLER_INVALID_FORMAT = 'Following front controller "%1$s" invalid! It must be a front controller object.';
    const EXCEPT_MSG_TRYER_INVALID_FORMAT = 'Following tryer "%1$s" invalid! It must be a tryer object.';
    const EXCEPT_MSG_FRONT_CONTROLLER_NOT_FOUND = 'No front controller found!';
	const EXCEPT_MSG_REPONSE_NOT_FOUND = 'No response found!';
}