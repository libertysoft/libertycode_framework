<?php
/**
 * Description :
 * This class allows to define default application class.
 * Can be consider as standard application.
 *
 * Default application uses the following specified configuration:
 * [
 *     root_dir_path(required): "string application root directory path",
 *
 *     response_require(optional: got true if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\framework\application\api\AppInterface;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\request_flow\front\api\FrontControllerInterface;
use liberty_code\error\tryer\api\TryerInterface;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code\framework\module\api\ModuleCollectionInterface;
use liberty_code\framework\event\library\ConstEvent;
use liberty_code\framework\application\library\ConstApp;
use liberty_code\framework\application\exception\TabConfigInvalidFormatException;
use liberty_code\framework\application\exception\ConfigParserBuilderInvalidFormatException;
use liberty_code\framework\application\exception\AppModeInvalidFormatException;
use liberty_code\framework\application\exception\CacheRepoInvalidFormatException;
use liberty_code\framework\application\exception\ModuleCollectionInvalidFormatException;
use liberty_code\framework\application\exception\ConfigInvalidFormatException;
use liberty_code\framework\application\exception\ProviderInvalidFormatException;
use liberty_code\framework\application\exception\RegisterInvalidFormatException;
use liberty_code\framework\application\exception\ObserverInvalidFormatException;
use liberty_code\framework\application\exception\FrontControllerInvalidFormatException;
use liberty_code\framework\application\exception\TryerInvalidFormatException;
use liberty_code\framework\application\exception\FrontControllerNotFoundException;
use liberty_code\framework\application\exception\ResponseNotFoundException;



/**
 * @method array getTabConfig() Get config array.
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
class DefaultApp extends FixBean implements AppInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig
	 * @param ParserBuilderInterface $objConfigParserBuilder = null
	 * @param SelectorInterface $objAppMode = null
     * @param RepositoryInterface $objCacheRepo = null
	 * @param ModuleCollectionInterface $objModuleCollection = null
     * @param ConfigInterface $objConfig = null
	 * @param ProviderInterface $objProvider = null
     * @param RegisterInterface $objRegister = null
     * @param ObserverInterface $objObserver = null
	 * @param FrontControllerInterface $objFrontController = null
     * @param TryerInterface $objExecTryer = null
     * @param TryerInterface $objSysTryer = null
     */
    public function __construct(
        array $tabConfig,
        ParserBuilderInterface $objConfigParserBuilder = null,
		SelectorInterface $objAppMode = null,
        RepositoryInterface $objCacheRepo = null,
		ModuleCollectionInterface $objModuleCollection = null,
        ConfigInterface $objConfig = null,
		ProviderInterface $objProvider = null,
        RegisterInterface $objRegister = null,
        ObserverInterface $objObserver = null,
		FrontControllerInterface $objFrontController = null,
        TryerInterface $objExecTryer = null,
        TryerInterface $objSysTryer = null
	)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration
        $this->setTabConfig($tabConfig);

		// Init configuration parser builder, if required
		if(!is_null($objConfigParserBuilder))
        {
			$this->setConfigParserBuilder($objConfigParserBuilder);
		}
		
		// Init application mode selector, if required
        if(!is_null($objAppMode))
        {
            $this->setAppMode($objAppMode);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setCacheRepo($objCacheRepo);
        }

		// Init module collection, if required
        if(!is_null($objModuleCollection))
        {
            $this->setModuleCollection($objModuleCollection);
        }

        // Init configuration, if required
        if(!is_null($objConfig))
        {
            $this->setConfig($objConfig);
        }

        // Init DI provider, if required
        if(!is_null($objProvider))
        {
            $this->setProvider($objProvider);
        }

        // Init register, if required
        if(!is_null($objRegister))
        {
            $this->setRegister($objRegister);
        }

        // Init event observer, if required
        if(!is_null($objObserver))
        {
            $this->setObserver($objObserver);
        }
		
		// Init front controller, if required
        if(!is_null($objFrontController))
        {
            $this->setFrontController($objFrontController);
        }

        // Init execution tryer, if required
        if(!is_null($objExecTryer))
        {
            $this->setExecTryer($objExecTryer);
        }

        // Init system tryer, if required
        if(!is_null($objSysTryer))
        {
            $this->setSysTryer($objSysTryer);
        }
    }

	
	
	
	
	// Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_TAB_CONFIG))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_TAB_CONFIG] = array(
                ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH => __DIR__ . '/../libertycode_framework'
            );
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER] = null;
        }

		if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_APP_MODE))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_APP_MODE] = null;
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }

		if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_MODULE_COLLECTION))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_MODULE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_CONFIG] = null;
        }

		if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_PROVIDER] = null;
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_REGISTER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_REGISTER] = null;
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_OBSERVER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_OBSERVER] = null;
        }

		if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_FRONT_CONTROLLER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_FRONT_CONTROLLER] = null;
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_EXEC_TRYER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_EXEC_TRYER] = null;
        }

        if(!$this->beanExists(ConstApp::DATA_KEY_DEFAULT_SYS_TRYER))
        {
            $this->__beanTabData[ConstApp::DATA_KEY_DEFAULT_SYS_TRYER] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstApp::DATA_KEY_DEFAULT_TAB_CONFIG,
            ConstApp::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER,
			ConstApp::DATA_KEY_DEFAULT_APP_MODE,
            ConstApp::DATA_KEY_DEFAULT_CACHE_REPO,
            ConstApp::DATA_KEY_DEFAULT_MODULE_COLLECTION,
            ConstApp::DATA_KEY_DEFAULT_CONFIG,
            ConstApp::DATA_KEY_DEFAULT_PROVIDER,
            ConstApp::DATA_KEY_DEFAULT_REGISTER,
            ConstApp::DATA_KEY_DEFAULT_OBSERVER,
            ConstApp::DATA_KEY_DEFAULT_FRONT_CONTROLLER,
            ConstApp::DATA_KEY_DEFAULT_EXEC_TRYER,
            ConstApp::DATA_KEY_DEFAULT_SYS_TRYER
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstApp::DATA_KEY_DEFAULT_TAB_CONFIG:
                    TabConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER:
                    ConfigParserBuilderInvalidFormatException::setCheck($value);
                    break;

				case ConstApp::DATA_KEY_DEFAULT_APP_MODE:
                    AppModeInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_MODULE_COLLECTION:
                    ModuleCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_REGISTER:
                    RegisterInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_OBSERVER:
                    ObserverInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_FRONT_CONTROLLER:
                    FrontControllerInvalidFormatException::setCheck($value);
                    break;

                case ConstApp::DATA_KEY_DEFAULT_EXEC_TRYER:
                case ConstApp::DATA_KEY_DEFAULT_SYS_TRYER:
                    TryerInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check response required.
     *
     * @return boolean
     */
    public function checkResponseRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }
	




    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrRootDirPath()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH];

        // Return result
        return $result;
    }



	/**
     * @inheritdoc
     */
    public function getObjConfigParserBuilder()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER);
    }
	
	
	
	/**
     * @inheritdoc
     */
    public function getObjAppMode()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_APP_MODE);
    }



    /**
     * @inheritdoc
     */
    public function getObjCacheRepo()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_CACHE_REPO);
    }


	
	/**
     * @inheritdoc
     */
    public function getObjModuleCollection()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_MODULE_COLLECTION);
    }



    /**
     * @inheritdoc
     */
    public function getObjConfig()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_CONFIG);
    }
	
	
	
	/**
     * @inheritdoc
     */
    public function getObjProvider()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_PROVIDER);
    }



    /**
     * @inheritdoc
     */
    public function getObjRegister()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_REGISTER);
    }



    /**
     * @inheritdoc
     */
    public function getObjObserver()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_OBSERVER);
    }



	/**
     * @inheritdoc
     */
    public function getObjFrontController()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_FRONT_CONTROLLER);
    }



    /**
     * @inheritdoc
     */
    public function getObjExecTryer()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_EXEC_TRYER);
    }



    /**
     * @inheritdoc
     */
    public function getObjSysTryer()
    {
        // Return result
        return $this->beanGet(ConstApp::DATA_KEY_DEFAULT_SYS_TRYER);
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setRootDirPath($strRootDirPath)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set root directory path
        $tabConfig[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH] = $strRootDirPath;

        // Set configuration
        $this->setTabConfig($tabConfig);
    }



    /**
     * Set response required option.
     *
     * @param boolean $boolResponseRequire
     */
    public function setResponseRequired($boolResponseRequire)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set root directory path
        $tabConfig[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE] = $boolResponseRequire;

        // Set configuration
        $this->setTabConfig($tabConfig);
    }



    /**
     * @inheritdoc
     */
    public function setConfigParserBuilder(ParserBuilderInterface $objParserBuilder = null)
    {
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_CONFIG_PARSER_BUILDER, $objParserBuilder);
    }
	
	
	
	/**
	 * @inheritdoc
	 */
	public function setAppMode(SelectorInterface $objSelector = null)
	{
		$this->beanSet(ConstApp::DATA_KEY_DEFAULT_APP_MODE, $objSelector);
	}



    /**
     * @inheritdoc
     */
    public function setCacheRepo(RepositoryInterface $objRepository = null)
    {
        // Return result
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_CACHE_REPO, $objRepository);
    }



	/**
	 * @inheritdoc
	 */
	public function setModuleCollection(ModuleCollectionInterface $objModuleCollection = null)
	{
		$this->beanSet(ConstApp::DATA_KEY_DEFAULT_MODULE_COLLECTION, $objModuleCollection);
	}



    /**
     * @inheritdoc
     */
    public function setConfig(ConfigInterface $objConfig = null)
    {
        // Return result
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_CONFIG, $objConfig);
    }



	/**
	 * @inheritdoc
	 */
	public function setProvider(ProviderInterface $objProvider = null)
	{
		$this->beanSet(ConstApp::DATA_KEY_DEFAULT_PROVIDER, $objProvider);
	}



    /**
     * @inheritdoc
     */
    public function setRegister(RegisterInterface $objRegister = null)
    {
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_REGISTER, $objRegister);
    }



    /**
     * @inheritdoc
     */
    public function setObserver(ObserverInterface $objObserver = null)
    {
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_OBSERVER, $objObserver);
    }



	/**
	 * @inheritdoc
	 */
	public function setFrontController(FrontControllerInterface $objFrontController = null)
	{
		$this->beanSet(ConstApp::DATA_KEY_DEFAULT_FRONT_CONTROLLER, $objFrontController);
	}



    /**
     * @inheritdoc
     */
    public function setExecTryer(TryerInterface $objTryer = null)
    {
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_EXEC_TRYER, $objTryer);
    }



    /**
     * @inheritdoc
     */
    public function setSysTryer(TryerInterface $objTryer = null)
    {
        $this->beanSet(ConstApp::DATA_KEY_DEFAULT_SYS_TRYER, $objTryer);
    }




	
	// Methods execute
	// ******************************************************************************

    /**
     * Execute specified event key if exists.
     *
     * @param string $strKey
     * @param array $tabCallArg = array()
     * @param array $tabStrCallElm = array()
     * @return null|mixed
     */
    public function dispatch(
        $strKey,
        array $tabCallArg = array(),
        array $tabStrCallElm = array()
    )
    {
        // Init var
        $result = null;
        $objObserver = $this->getObjObserver();

        // Check event observer found
        if(!is_null($objObserver))
        {
            // Execute
            $result = $objObserver->dispatch($strKey, $tabCallArg, $tabStrCallElm);
        }

        // Return result
        return $result;
    }



    /**
     * Execute module boot.
     */
    protected function executeModuleBoot()
    {
        // Init var
        $objModuleCollection = $this->getObjModuleCollection();

        // Boot module, if required
        if(!is_null($objModuleCollection))
        {
            $eventResult = $this->dispatch(ConstEvent::EVENT_NAME_APP_BEFORE_MODULE_BOOT);
            $objModuleCollection->boot();
            $this->dispatch(ConstEvent::EVENT_NAME_APP_AFTER_MODULE_BOOT, array($eventResult));
        }
    }



    /**
     * Execute front controller.
     *
     * @return null|ResponseInterface
     * @throws FrontControllerNotFoundException
     * @throws ResponseNotFoundException
     */
    protected function executeFrontController()
    {
        // Init var
        $objFrontController = $this->getObjFrontController();

        // Check front controller found
        if(is_null($objFrontController))
        {
            throw new FrontControllerNotFoundException();
        }

        // Get result (response with event before and after)
        $eventResult = $this->dispatch(ConstEvent::EVENT_NAME_APP_BEFORE_FRONT_CONTROLLER_EXECUTION);
        $result = $objFrontController->execute();
        $this->dispatch(ConstEvent::EVENT_NAME_APP_AFTER_FRONT_CONTROLLER_EXECUTION, array($result, $eventResult));

        // Check result (response found), if required
        if(is_null($result) && $this->checkResponseRequired())
        {
            throw new ResponseNotFoundException();
        }

        return $result;
    }



    /**
     * Execute application to resolve request
     * and get response, if required.
     *
     * @param boolean &$boolExecutionSuccess = true
     * @return null|ResponseInterface
     */
    protected function execute(&$boolExecutionSuccess = true)
    {
        // Init execution function
        $execute = function(){
            // Boot module
            $this->executeModuleBoot();

            // Execute front controller and get potential response
            $result = $this->executeFrontController();

            // Return result
            return $result;
        };

        // Init var
        $boolExecutionSuccess = true;
        $objExecTryer = $this->getObjExecTryer();
        $result = (
            is_null($objExecTryer) ?
                $execute() :
                // Try execution, if required
                $objExecTryer->execute($execute, $boolExecutionSuccess)
        );

        return $result;
    }



	/**
	 * @inheritdoc
	 * @throws ResponseNotFoundException
	 */
	public function run()
	{
	    // Init run function
	    $run = function() {
            // Init var
            $boolExecutionSuccess = true;

            // Execute and get response, if required
            $objResponse = $this->execute($boolExecutionSuccess);

            // Send response, if required (with event before and after)
            if(!is_null($objResponse))
            {
                $eventResult = $this->dispatch(ConstEvent::EVENT_NAME_APP_BEFORE_RESPONSE_SEND, array($objResponse, $boolExecutionSuccess));
                $objResponse->send();
                $this->dispatch(ConstEvent::EVENT_NAME_APP_AFTER_RESPONSE_SEND, array($objResponse, $boolExecutionSuccess, $eventResult));
            }
            else if($this->checkResponseRequired())
            {
                throw new ResponseNotFoundException();
            }
        };

	    // Init var
        $objSysTryer = $this->getObjSysTryer();

        // Run
        if(is_null($objSysTryer))
        {
            $run();
        }
        else
        {
            $objSysTryer->execute($run);
        }
	}
	
	
	
}