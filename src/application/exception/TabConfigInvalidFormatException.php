<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\exception;

use liberty_code\framework\application\library\ConstApp;



class TabConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstApp::EXCEPT_MSG_TAB_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache source required option
            isset($config[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH]) &&
            is_string($config[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH]) &&
            (trim($config[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH]) != '') &&
            file_exists($config[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH]) &&
            is_dir($config[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH]) &&

            // Check valid response required option
            (
                (!isset($config[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE]) ||
                    is_int($config[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE]) ||
                    (
                        is_string($config[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE]) &&
                        ctype_digit($config[ConstApp::TAB_CONFIG_KEY_RESPONSE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}