<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\exception;

use liberty_code\framework\application\library\ConstApp;
use liberty_code\framework\app_mode\select\api\SelectorInterface;



class AppModeInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $selector
     */
	public function __construct($selector)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstApp::EXCEPT_MSG_APP_MODE_INVALID_FORMAT,
            mb_strimwidth(strval($selector), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified selector has valid format
	 * 
     * @param mixed $selector
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($selector)
    {
		// Init var
		$result = (
			(is_null($selector)) ||
			($selector instanceof SelectorInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($selector);
		}
		
		// Return result
		return $result;
    }
	
	
	
}