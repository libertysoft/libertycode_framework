<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\exception;

use liberty_code\framework\application\library\ConstApp;
use liberty_code\request_flow\front\api\FrontControllerInterface;



class FrontControllerInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $frontController
     */
	public function __construct($frontController)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstApp::EXCEPT_MSG_FRONT_CONTROLLER_INVALID_FORMAT,
            mb_strimwidth(strval($frontController), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified front controller has valid format
	 * 
     * @param mixed $frontController
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($frontController)
    {
		// Init var
		$result = (
			(is_null($frontController)) ||
			($frontController instanceof FrontControllerInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($frontController);
		}
		
		// Return result
		return $result;
    }
	
	
	
}