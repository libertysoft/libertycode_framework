<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\exception;

use liberty_code\framework\application\library\ConstApp;
use liberty_code\framework\module\api\ModuleCollectionInterface;



class ModuleCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $moduleCollection
     */
	public function __construct($moduleCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstApp::EXCEPT_MSG_MODULE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($moduleCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified module collection has valid format
	 * 
     * @param mixed $moduleCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($moduleCollection)
    {
		// Init var
		$result = (
			(is_null($moduleCollection)) ||
			($moduleCollection instanceof ModuleCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($moduleCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}