<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\exception;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\framework\application\library\ConstApp;



class ConfigParserBuilderInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $parserBuilder
     */
	public function __construct($parserBuilder)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstApp::EXCEPT_MSG_CONFIG_PARSER_BUILDER_INVALID_FORMAT,
            mb_strimwidth(strval($parserBuilder), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified parser builder has valid format
	 * 
     * @param mixed $parserBuilder
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($parserBuilder)
    {
		// Init var
		$result = (
			(is_null($parserBuilder)) ||
			($parserBuilder instanceof ParserBuilderInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($parserBuilder);
		}
		
		// Return result
		return $result;
    }
	
	
	
}