<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\exception;

use liberty_code\framework\application\library\ConstApp;
use liberty_code\error\tryer\api\TryerInterface;



class TryerInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $tryer
     */
	public function __construct($tryer)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstApp::EXCEPT_MSG_TRYER_INVALID_FORMAT,
            mb_strimwidth(strval($tryer), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified tryer has valid format.
	 * 
     * @param mixed $tryer
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($tryer)
    {
		// Init var
		$result = (
			(is_null($tryer)) ||
			($tryer instanceof TryerInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($tryer);
		}
		
		// Return result
		return $result;
    }
	
	
	
}