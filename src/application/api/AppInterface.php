<?php
/**
 * Description :
 * This class allows to describe behavior of application class.
 * Application allows to run project application.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\application\api;

use liberty_code\parser\build\api\BuilderInterface as ParserBuilderInterface;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\event\observer\api\ObserverInterface;
use liberty_code\request_flow\front\api\FrontControllerInterface;
use liberty_code\error\tryer\api\TryerInterface;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code\framework\module\api\ModuleCollectionInterface;



interface AppInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get application root directory full path.
     *
     * @return string
     */
    public function getStrRootDirPath();



	/**
	 * Get parser builder object, for configuration.
	 *
	 * @return null|ParserBuilderInterface
	 */
	public function getObjConfigParserBuilder();



	/**
	 * Get cache repository object.
	 *
	 * @return null|RepositoryInterface
	 */
	public function getObjCacheRepo();



    /**
     * Get application mode selector object.
     *
     * @return null|SelectorInterface
     */
    public function getObjAppMode();



	/**
	 * Get module collection object.
	 *
	 * @return null|ModuleCollectionInterface
	 */
	public function getObjModuleCollection();



    /**
     * Get configuration object.
     *
     * @return null|ConfigInterface
     */
    public function getObjConfig();



	/**
	 * Get DI provider object.
	 *
	 * @return null|ProviderInterface
	 */
	public function getObjProvider();



	/**
	 * Get register object.
	 *
	 * @return null|RegisterInterface
	 */
	public function getObjRegister();



    /**
     * Get event observer object.
     *
     * @return null|ObserverInterface
     */
    public function getObjObserver();



	/**
	 * Get front controller object.
	 *
	 * @return null|FrontControllerInterface
	 */
	public function getObjFrontController();



    /**
     * Get execution tryer object.
     * Tryer used to try application execution.
     *
     * @return null|TryerInterface
     */
    public function getObjExecTryer();



    /**
     * Get system tryer object.
     * Tryer used to try application system running.
     *
     * @return null|TryerInterface
     */
    public function getObjSysTryer();
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set application root directory full path.
     *
     * @param string $strRootDirPath
     */
    public function setRootDirPath($strRootDirPath);



    /**
     * Set parser builder object, for configuration.
     *
     * @param ParserBuilderInterface $objParserBuilder = null
     */
    public function setConfigParserBuilder(ParserBuilderInterface $objParserBuilder = null);



	/**
	 * Set application mode selector object.
	 *
	 * @param SelectorInterface $objSelector = null
	 */
	public function setAppMode(SelectorInterface $objSelector = null);



    /**
     * Set cache repository object.
     *
     * @param RepositoryInterface $objRepository = null
     */
    public function setCacheRepo(RepositoryInterface $objRepository = null);



	/**
	 * Set module collection object.
	 *
	 * @param ModuleCollectionInterface $objModuleCollection = null
	 */
	public function setModuleCollection(ModuleCollectionInterface $objModuleCollection = null);



    /**
     * Set configuration object.
     *
     * @param ConfigInterface $objConfig = null
     */
    public function setConfig(ConfigInterface $objConfig = null);


	
	/**
	 * Set DI provider object.
	 *
	 * @param ProviderInterface $objProvider = null
	 */
	public function setProvider(ProviderInterface $objProvider = null);



	/**
	 * Set register object.
	 *
	 * @param RegisterInterface $objRegister = null
	 */
	public function setRegister(RegisterInterface $objRegister = null);



    /**
     * Set event observer object.
     *
     * @param ObserverInterface $objObserver = null
     */
    public function setObserver(ObserverInterface $objObserver = null);



	/**
	 * Set front controller object.
	 *
	 * @param FrontControllerInterface $objFrontController = null
	 */
	public function setFrontController(FrontControllerInterface $objFrontController = null);



    /**
     * Set execution tryer object.
     *
     * @param TryerInterface $objTryer = null
     */
    public function setExecTryer(TryerInterface $objTryer = null);



    /**
     * Set system tryer object.
     *
     * @param TryerInterface $objTryer = null
     */
    public function setSysTryer(TryerInterface $objTryer = null);
	




	// Methods execute
	// ******************************************************************************

	/**
	 * Run application to resolve request.
	 */
	public function run();
}