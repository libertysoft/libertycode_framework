<?php
/**
 * Description :
 * This class allows to define default bootstrap class.
 * Can be consider is base of all bootstrap type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\bootstrap\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\framework\bootstrap\api\BootstrapInterface;

use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\bootstrap\exception\ModuleNotFoundException;



abstract class DefaultBootstrap extends DefaultBean implements BootstrapInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	/**
     * @var AppInterface
     */
	protected $objApp;
	
	
	
	/**
     * @var string
     */
	protected $strModuleKey;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @param AppInterface $objApp
     * @param string $strModuleKey
     */
	public function __construct(AppInterface $objApp, $strModuleKey)
	{
        // Init properties
        $this->objApp = $objApp;
        $this->strModuleKey = $strModuleKey;

		// Call parent constructor
		parent::__construct();
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get application object.
     *
     * @return AppInterface
     */
    public function getObjApp()
    {
        // Return result
        return $this->objApp;
    }



    /**
     * Get module object.
     *
     * @return ModuleInterface
     * @throws ModuleNotFoundException
     */
    public function getObjModule()
    {
        // Init var
        $objModuleCollection = $this->objApp->getObjModuleCollection();
        $result = (
            (!is_null($objModuleCollection)) ?
                $objModuleCollection->getObjModule($this->strModuleKey) :
                null
        );

        // Check module found
        if(is_null($result))
        {
            throw new ModuleNotFoundException();
        }

        // Return result
        return $result;
    }
	
	
	
}