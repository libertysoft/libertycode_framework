<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\bootstrap\exception;

use liberty_code\framework\bootstrap\library\ConstBootstrap;



class ModuleNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct()
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstBootstrap::EXCEPT_MSG_MODULE_NOT_FOUND;
	}
}