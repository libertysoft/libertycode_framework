<?php
/**
 * Description :
 * This class allows to describe behavior of bootstrap class.
 * Bootstrap is item, containing all information to boot module.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\bootstrap\api;

use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\application\api\AppInterface;



interface BootstrapInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get application object.
     *
     * @return AppInterface
     */
    public function getObjApp();



    /**
     * Get module object.
     *
     * @return ModuleInterface
     */
    public function getObjModule();
}