<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\bootstrap\library;



class ConstBootstrap
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
	const EXCEPT_MSG_MODULE_NOT_FOUND = 'No module found!';
}