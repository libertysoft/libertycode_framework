<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\framework\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Path separator configuration
	const CONFIG_PATH_SEPARATOR = '/';
}