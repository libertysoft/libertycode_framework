<?php
/**
 * Description :
 * This include allows to boot file system (directory creation, ...).
 * It can be boot before application root directory path boot, if application root directory path provided.
 * Else it must be boot after application root directory path boot (include in application boot).
 *
 * @var string $strAppRootDirPath = null
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

use liberty_code\framework\framework\library\path\library\ToolBoxPath;
use liberty_code\framework\framework\library\ConstFramework;



// Create directories, if required
$tabPath = array(
    ConstFramework::CONF_PATH_DIR_CONFIG,
    ConstFramework::CONF_PATH_DIR_VAR,
    ConstFramework::CONF_PATH_DIR_VAR_FRAMEWORK,
    ConstFramework::CONF_PATH_DIR_VAR_FRAMEWORK_CACHE
);

foreach($tabPath as $strPath)
{
    // Try to get directory full path
    try{
        $strPath = ToolBoxPath::getStrPathFull($strPath);
    } catch (Exception $e) {
        if(
            isset($strAppRootDirPath) &&
            is_string($strAppRootDirPath)
        )
        {
            $strPath = ($strAppRootDirPath . $strPath);
        }
        else
        {
            throw $e;
        }
    }

    // Create directory, if required
    if(!is_dir($strPath))
    {
        mkdir($strPath);
    }
}