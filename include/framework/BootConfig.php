<?php
/**
 * Description :
 * This include allows to configure libraries for application.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

use liberty_code\autoload\config\model\DefaultConfig as AutoloadDefaultConfig;
use liberty_code\di\config\model\DefaultConfig as DiDefaultConfig;
use liberty_code\route\config\model\DefaultConfig as RouteDefaultConfig;
use liberty_code\request_flow\config\model\DefaultConfig as RequestFlowDefaultConfig;
use liberty_code\controller\config\model\DefaultConfig as ControllerDefaultConfig;
use liberty_code\http\config\model\DefaultConfig as HttpDefaultConfig;
use liberty_code\command\config\model\DefaultConfig as CommandDefaultConfig;



// Init var
/** @var AutoloadDefaultConfig $objAutoloadConfig */
$objAutoloadConfig = AutoloadDefaultConfig::instanceGetDefault();
/** @var DiDefaultConfig $objDiConfig */
$objDiConfig = DiDefaultConfig::instanceGetDefault();
/** @var RouteDefaultConfig $objRouteConfig */
$objRouteConfig = RouteDefaultConfig::instanceGetDefault();
/** @var RequestFlowDefaultConfig $objRequestFlowConfig */
$objRequestFlowConfig = RequestFlowDefaultConfig::instanceGetDefault();
/** @var ControllerDefaultConfig $objControllerConfig */
$objControllerConfig = ControllerDefaultConfig::instanceGetDefault();
/** @var HttpDefaultConfig $objHttpConfig */
$objHttpConfig = HttpDefaultConfig::instanceGetDefault();
/** @var CommandDefaultConfig $objCommandConfig */
$objCommandConfig = CommandDefaultConfig::instanceGetDefault();



// Configuration: auto-loading
// $objAutoloadConfig->setBoolPatternDecorationRequire(bool $boolPatternDecorationRequire); // Default configuration
// $objAutoloadConfig->setStrPatternDecoration(string $strPatternDecoration); // Default configuration



// Configuration: dependence injection
// $objDiConfig->setBoolAutoConfig(true); // Already configured in application building
// $objDiConfig->setIntMaxLimitRecursion(integer $intLimitRecursion); // Default configuration



// Configuration: routing
// $objRouteConfig->setBoolPatternDecorationRequire(bool $boolPatternDecorationRequire); // Default configuration
// $objRouteConfig->setStrPatternDecoration(string $strPatternDecoration); // Default configuration
// $objRouteConfig->setStrParamElementStart(string $strParamElementStart); // Default configuration
// $objRouteConfig->setStrParamElementEnd(string $strParamElementEnd); // Default configuration
// $objRouteConfig->setStrParamArgumentStart(string $strParamArgumentStart); // Default configuration
// $objRouteConfig->setStrParamArgumentEnd(string $strParamArgumentEnd); // Default configuration



// Configuration: event
// $objEventConfig->setBoolRootClassPathRequire(bool $boolRootClassPathRequire); // Default configuration
// $objEventConfig->setStrRootClassPath(string $strRootClassPath); // Default configuration
// $objEventConfig->setBoolRootFilePathRequire(bool $boolRootFilePathRequire); // Already configured in application building
// $objEventConfig->setStrRootFilePath(string $strRootFilePath); // Already configured in application building



// Configuration: request flow
// $objRequestFlowConfig->setStrDefaultResponseContent(string $strContent); // Default configuration



// Configuration: controller
// $objControllerConfig->setTabDefaultMethodAllowed(array $tabMethod); // Default configuration
// $objControllerConfig->setTabDefaultMethodDenied(array $tabMethod); // Default configuration



// Configuration: HTTP
// $objHttpConfig->setStrHttpRequestSourceLink(string $strLink); // Default configuration
// $objHttpConfig->setIntHttpResponseStatusCode(integer $intStatusCode); // Default configuration
// $objHttpConfig->setStrHttpResponseStatusMsg(string $strStatusMsg); // Default configuration
// $objHttpConfig->setTabHttpResponseHeader(array $tabHeader); // Default configuration
// $objHttpConfig->setTabHttpResponseHeaderAdd(array $tabHeader); // Default configuration
// $objHttpConfig->setIntHttpRedirectResponseStatusCode(integer $intStatusCode); // Default configuration
// $objHttpConfig->setStrHttpRedirectResponseUrl(string $strUrl); // Default configuration
// $objHttpConfig->setStrHttpRouteMethod(string $strMethod); // Default configuration



// Configuration: command line
// $objCommandConfig->setTabCmdRequestOptRegexp(array $tabRegexp); // Default configuration
// $objCommandConfig->setTabCmdRequestOptValueRegexp(array $tabRegexp); // Default configuration