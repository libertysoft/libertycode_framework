<?php
/**
 * Description :
 * This include allows to boot command line application.
 *
 * @var array $tabAppConfig
 * Format: @see DefaultApp
 *
 * @var array $tabAppConfigParserType = array(
 *     ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_PHP,
 *     ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP => '#^\<\?php\s*([^\z]*)(\s\?\>)?\s*\z#',
 *     ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN => '<?php ' . PHP_EOL . '%1$s'
 * )
 *
 * @var string $strActiveAppModeKey = null
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

use liberty_code\parser\parser\string_table\library\ConstStrTableParser;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\parser\build\library\ConstBuilder;
use liberty_code\framework\application\library\ConstApp;
use liberty_code\framework\application\model\DefaultApp;
use liberty_code\framework\framework\app_mode\build\library\ToolBoxBuilder as ToolBoxAppModeBuilder;
use liberty_code\framework\framework\module\build\library\ToolBoxBuilder as ToolBoxModuleBuilder;
use liberty_code\framework\framework\config\build\library\ToolBoxBuilder as ToolBoxConfigBuilder;
use liberty_code\framework\framework\di\build\library\ToolBoxBuilder as ToolBoxDiBuilder;
use liberty_code\framework\framework\register\build\library\ToolBoxBuilder as ToolBoxRegisterBuilder;
use liberty_code\framework\framework\event\build\library\ToolBoxBuilder as ToolBoxEventBuilder;
use liberty_code\framework\framework\route\build\library\ToolBoxBuilder as ToolBoxRouteBuilder;
use liberty_code\framework\framework\application\factory\library\ToolBoxAppFactory;
use liberty_code\framework\framework\application\build\library\ToolBoxBuilder as ToolBoxAppBuilder;



// Init var
$strAppRootDirPath = $tabAppConfig[ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH];
$tabAppConfigParserType = (
    isset($tabAppConfigParserType) ?
        $tabAppConfigParserType :
        array(
            ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_PHP,
            ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
            ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN => '<?php ' . PHP_EOL . '%1$s',
            ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE => true,
            ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE => true
        )
);
$strActiveAppModeKey = (isset($strActiveAppModeKey) ? $strActiveAppModeKey : null);

// Init file system
require(__DIR__ . '/BootFileSystem.php');

// Get application
$objApp = ToolBoxAppFactory::getObjCommandDefaultApp($tabAppConfig, $tabAppConfigParserType);

// Hydrate application
ToolBoxAppBuilder::hydrateCommandApp($objApp);
ToolBoxAppModeBuilder::hydrateApp($objApp, $strActiveAppModeKey);
ToolBoxModuleBuilder::hydrateApp($objApp);
ToolBoxConfigBuilder::hydrateApp($objApp);
ToolBoxDiBuilder::hydrateApp($objApp);
ToolBoxRegisterBuilder::hydrateApp($objApp);
ToolBoxEventBuilder::hydrateApp($objApp);
ToolBoxRouteBuilder::hydrateCommandApp($objApp);

// Init autoload
require(__DIR__ . '/BootAutoload.php');


