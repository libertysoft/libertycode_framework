<?php
/**
 * Description :
 * This include allows to boot autoload.
 * It must be boot after application boot (include in application boot).
 * 
 * @var string $strAppRootDirPath
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

use liberty_code\framework\framework\autoload\factory\library\ToolBoxLoaderFactory;
use liberty_code\framework\framework\autoload\build\library\ToolBoxBuilder;



// Get auto-loader
$objLoader = ToolBoxLoaderFactory::getObjLoader($strAppRootDirPath);

// Hydrate auto-loader
ToolBoxBuilder::hydrateLoader($objLoader);

// Register autoload
$objLoader->registerSet();


