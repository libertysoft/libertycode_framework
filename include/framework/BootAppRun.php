<?php
/**
 * Description :
 * This include allows to run application.
 * It must be boot after application boot.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

use liberty_code\framework\framework\application\library\ToolBoxApp;



// Run application
$objApp = ToolBoxApp::getObjApp();
$objApp->run();