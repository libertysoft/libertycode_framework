LibertyCode_Framework
=====================



Version "1.0.0"
---------------

- Create repository

- Set application mode

- Set module

- Set request

- Set application

- Set controller

- Set utility

- Set builder

---



Version "1.0.1"
---------------

- Update application mode

    - Set mode factory
    - Update builder: use mode factory

- Update module

    - Set module factory
    - Update builder: use module factory

- Set bootstrap

- Set route

- Set error

- Set framework

- Remove controller

- Remove utility

    - Move feature on framework

- Remove builder

    - Move feature on framework

---


